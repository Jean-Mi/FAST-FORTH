    ; -*- coding: utf-8 -*-
;
; ----------------------------------------------------------------------
;forthMSP430FR_EXTD_ASM.asm
; ----------------------------------------------------------------------

; PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, rDOVAR,rDOCON,rDODOES, rDOCOL, R3, SR,RSP, PC
; PUSHM order : R15,R14,R13,R12,R11,R10, R9, R8,  R7   ,  R6  ,  R5   ,   R4  , R3, R2, R1, R0

; example : PUSHM #6,IP pushes IP,S,T,W,X,Y registers to return stack
;
; POPM  order :  PC,RSP, SR, R3, rDOCOL,rDODOES,rDOCON,rDOVAR,  Y,  X,  W,  T,  S, IP,TOS,PSP
; POPM  order :  R0, R1, R2, R3,   R4  ,  R5   ,  R6  ,  R7  , R8, R9,R10,R11,R12,R13,R14,R15

; example : POPM #6,IP   pop Y,X,W,T,S,IP registers from return stack

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER : search argument "xxxx" or "x.xxxx"
; ----------------------------------------------------------------------

; common code for 3 successive Searches: ARG, ARG+Offset, ARG-offset
; frequently ARG is a number, so let's go first to search a number :
SearchARGn  PUSH &TOIN              ;4                  push TOIN for iterative SearchARGn
; Part I: search numeric ARG        ;
SRCHARGNUM  mLO2HI                  ; -- sep            sep =  ','|'('|' '
            .word WORDD,QNUMBER     ; -- n|addr flag    SR(Z)=1 if not a number
            .word QFBRAN,SRCHARGCHAR;
            .word ARGFOUND          ;
; ----------------------------------;
; part II: search symbolic ARG (FORTH NAME) if numeric ARG not found
; TOS = ARG = address of VARIABLE, CONSTANT, User address for a MARKER name, CFA for all others.
SRCHARGCHAR .word FIND              ; -- addr flag      search definition
            mHI2LO                  ;
            MOV @PSP+,TOS           ; -- n|addr
            JNZ ARGCHARNEXT         ; -- n              if found
ARGNOTFOUND MOV @RSP+,&TOIN         ; -- addr           restore TOIN for next search
            MOV @RSP+,PC            ;32                 SR(Z)=1 if ARG not found
; ----------------------------------;
ARGCHARNEXT MOV @TOS+,S             ; -- PFA            S=DOxxx
QDOVAR      SUB #1287h,S            ;                   if CFA is DOVAR ?
            JZ ARGFOUND             ; -- ADR            yes, PFA = ADR of VARIABLE
QDOCON      ADD #1,S                ;                   is CFA is DOVAR-1 = DOCON ?
            JNZ QMARKER             ; -- PFA
            MOV @TOS,TOS            ; -- CTE            yes, TOS = constant
            JMP ARGFOUND            ;
QMARKER     CMP #MARKER_DOES,0(TOS) ;                   search if PFA = [MARKER_DOES]
            JNZ OTHERARG            ; -- PFA
            ADD #2+ENV_LEN+2,TOS    ; -- UPFA+2         skip DODOES + ENV_LEN + 2  
OTHERARG    SUB #2,TOS              ; -- UPFA|CFA|CTE|ADR
    .IFDEF LARGE_CODE               ;
            BIT #UF2,SR             ;                   set by LARGE_CODE|LARGE_DATA instructions
            JZ ARGFOUND             ; -- $llll          ARG format for type I|II = "xxxx"
            SUB #2,PSP              ;
            MOV TOS,0(PSP)          ;
            MOV #0,TOS              ; -- $llll $0000    ARG format for type V|VI = "xxxxx"
    .ENDIF                          ;
ARGFOUND    ADD #2,RSP              ;                   remove TOIN
            MOV @RSP+,PC            ;24                 SR(Z)=0 if ARG found
; ----------------------------------;

SearchIndex                         ;
; Search index of "xxxx(REG),"      ; <== CompIdxSrchRn <== PARAM1IDX
; Search index of ",xxxx(REG)"      ; <== CompIdxSrchRn <== PARAM2IDX
; Search index of "xxxx(REG),"      ; <== CALLA, MOVA
; Search index of ",xxxx(REG)"      ; <== MOVA
            SUB #1,&TOIN            ;               move >IN back one (unskip first_char)
            MOV #'(',TOS            ; addr -- "("   as WORD separator to find xxxx of "xxxx(REG),"
SearchARG                           ; sep -- n|d    or abort" not found"
; Search ARG of "#xxxx,"            ; <== PARAM1SHARP   sep = ',' 
; Search ARG of "&xxxx,"            ; <== PARAMXAMP     sep = ','
; Search ARG of ",&xxxx"            ; <== PARAMXAMP <== PARAM2AMP   sep = ' '
            MOV TOS,W               ;
            PUSHM #4,IP             ; -- sep        PUSHM IP,S,T,W as IP_RET,OPCODE,OPCODEADR,sep
            CALL #SearchARGn        ;               first search argument without offset
    .IFDEF LARGE_CODE               ;
            JNZ SrchNext            ;
    .ELSE                           ;
            JNZ SrchEnd             ; -- ARG        if ARG found "xxxx" or "xxxxx"
    .ENDIF                          ;
; ----------------------------------;
SearchArgPl MOV #'+',TOS            ; -- '+'
            CALL #SearchARGn        ;               2th search argument with '+' as separator
            JNZ ArgPlusOfst         ; -- ARG        if ARG of ARG+offset found
SearchArgMi MOV #'-',TOS            ; -- '-'
    .IFDEF LARGE_CODE               ;
            BIS #UF3,SR             ;               for LARGE_CODE|LARGE_DATA instructions with negative offset
    .ENDIF                          ;
            CALL #SearchARGn        ;               3th search argument with '-' as separator
            SUB #1,&TOIN            ;               to handle offset with its minus sign
ArgPlusOfst PUSH TOS                ; -- ARGlo [ARGhi]       R-- IP_RET,OPCODE,OPCODEADR,sep,ARG
            MOV 2(RSP),TOS          ; -- ARGlo sep        reload offset sep
SrchOfst    mLO2HI                  ;               search offset "xxxxx"
            .word WORDD,QNUMBER     ; -- Ofst|c-addr flag
            .word QFBRAN,FNOTFOUND  ; -- c-addr     no return, see INTERPRET
            mHI2LO                  ; -- Ofst       -- OfstLo OfstHi
    .IFDEF LARGE_CODE               ;
            BIT #UF2,SR             ;
            JZ AddOffset            ;               if no LARGE_CODE|LARGE_DATA instructions
AddDblOfst  BIC #0FFF0h,TOS         ; -- OfstHi     troncate Ofst 20 bits            
            ADD @PSP+,0(PSP)        ; -- ofstHi     add OfstLo to ARGlo
            ADDC TOS,0(RSP)         ;               addc OfstHi to ARGhi
            BIT #UF3,SR             ;               - offset double number flag
            JZ AddOffset            ;
            BIC #UF3,SR             ;               if -offset flag, clear it
            ADD #1,0(RSP)           ;                   add #1 to ARGhi
    .ENDIF                          ;
AddOffset   ADD @RSP+,TOS           ; -- Arg+Ofst   -- ArgLo+OfstLo ArgHi+OfstHi
    .IFDEF LARGE_CODE               ;
SrchNext    BIT #UF2,SR             ;
            JZ SrchEnd              ;
            AND #000Fh,TOS          ; -- ArgLo ArgHi    troncate Arg 20 bits            
    .ENDIF                          ;
SrchEnd     POPM #4,IP              ;               POPM W,T,S,IP     common return for SearchARG and SearchRn
            MOV @RSP+,PC            ;66             with SR(Z) valid for SearchRn (not used for SearchArg)

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER : search REG
; ----------------------------------------------------------------------
; compute index of "xxxx(REG),"     ; <== PARAM1IDX, sep=','
; compute index of ",xxxx(REG)"     ; <== PARAM2IDX, sep=' '
CompIdxSrchRn                       ; addr -- Rn|addr
            CALL #SearchIndex       ; -- xxxx       aborted if not found
            MOV &DP,X               ;
            MOV TOS,0(X)            ; -- xxxx       compile ARG xxxx
            ADD #2,&DP              ;
            MOV #')',TOS            ; -- ")"        prepare separator to search REG of "xxxx(REG)"
; search REG of "xxxx(REG),"        ;
; search REG of ",xxxx(REG)"        ;
; search REG of "@REG,"   sep = ',' ; <== PARAM1AT
SkipRSrchRn ADD #1,&TOIN            ;               skip 'R' in input buffer
; search REG of "@REG+,"  sep = '+' ; <== PARAM1ATPL
; search REG of "REG,"    sep = ',' ; <== PARAM1REG
; search REG of ",REG"    sep = ' ' ; <== PARAM2REG
SearchRn    MOV &TOIN,W             ;
            PUSHM #4,IP             ;               PUSHM IP,S,T,W as IP_RET,OPCODE,OPCODEADR,TOIN
            mLO2HI                  ;               search xx of Rxx
            .word WORDD,QNUMBER     ;
            .word QFBRAN,REGNOTFOUND; -- xxxx       SR(Z)=1 if Not a Number
            mHI2LO                  ; -- Rn         number is found
    .IFDEF LARGE_CODE               ; -- Rlo Rhi
            BIT #UF2,SR             ;
            JZ $+2+2                ;               skip one word
            MOV @PSP+,TOS           ; -- Rn
    .ENDIF                          ;
            CMP #16,TOS             ; -- Rn
            JNC SrchEnd             ; -- Rn         SR(Z)=0 if Rn found,
            JC  REGNUM_ERR          ;               abort if Rn out of bounds
REGNOTFOUND mHI2LO                  ; -- addr       SR(Z)=1, (used in case of @REG not found),
            MOV @RSP,&TOIN          ; -- addr       restore TOIN, ready for next SearchRn
            JMP SrchEnd             ; -- addr       SR(Z)=1 ==> not a register

; ----------------------------------------------------------------------
;       MOV(.B) SR,dst   is coded as follow : MOV(.B) R2,dst            ; 1 cycle,  one word    AS=00   (register mode)
;       MOV(.B) #0,dst   is coded as follow : MOV(.B) R3,dst            ; 1 cycle,  one word    AS=00   (register mode)
;       MOV(.B) #1,dst   is coded as follow : MOV(.B) (R3),dst          ; 1 cycle,  one word    AS=01   ( x(reg)  mode)
;       MOV(.B) #4,dst   is coded as follow : MOV(.B) @R2,dst           ; 1 cycle,  one word    AS=10   ( @reg    mode)
;       MOV(.B) #2,dst   is coded as follow : MOV(.B) @R3,dst           ; 1 cycle,  one word    AS=10   ( @reg    mode)
;       MOV(.B) #8,dst   is coded as follow : MOV(.B) @R2+,dst          ; 1 cycle,  one word    AS=11   ( @reg+   mode)
;       MOV(.B) #-1,dst  is coded as follow : MOV(.B) @R3+,dst          ; 1 cycle,  one word    AS=11   ( @reg+   mode)
; ----------------------------------------------------------------------
;       MOV(.B) &EDE,dst is coded as follow : MOV(.B) EDE(R2),dst       ; 3 cycles, two words   AS=01   ( x(reg)  mode)
;       MOV(.B) #xxxx,dst is coded as follow: MOV(.B) @PC+,dst          ; 2 cycles, two words   AS=11   ( @reg+   mode)
; ----------------------------------------------------------------------

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER : INTERPRET FIRST OPERAND
; ----------------------------------------------------------------------
; PARAM1     separator --           ; parse input buffer until separator and compute first operand of opcode
                                    ;               sep is "," for src TYPE II and " " for dst (TYPE II).
PARAM1      JNZ QPARAM1SHARP        ; -- A sep      A=@code_instruct, regs: S=0, T=@compil, SR(Z)=1 if prefix = 'R'
PARAM1REG   CALL #SearchRn          ;               case of "REG,"
            JNZ SWAPREG             ; -- A 000R     REG of "REG," found, S=OPCODE=0
; ----------------------------------;
QPARAM1SHARP CMP.B #'#',W           ; -- A sep      W=first char
            JNE QPARAM1AMP          ;
;PARAM1SHARP                        ;               immediate {#-1 #0 #1 #2 #4 #8} are processed by the preprocessor GEMA
            CALL #SearchARG         ; -- A xxxx     abort if not found
;; ==================================;               comment below if immediate {#-1 #0 #1 #2 #4 #8} are processed in FastForthREGtoTI.pat
;            MOV #0300h,S            ;               OPCODE = 0300h : MOV #0,dst is coded MOV R3,dst
;            CMP #0,TOS              ; -- A xxxx     #0 ?
;            JZ PARAMENDOF           ;
;            MOV #0310h,S            ;               OPCODE = 0310h : MOV #1,dst is coded MOV 0(R3),dst
;            CMP #1,TOS              ; -- A xxxx     #1 ?
;            JZ PARAMENDOF           ;
;            MOV #0320h,S            ;               OPCODE = 0320h : MOV #2,dst is coded MOV @R3,dst
;            CMP #2,TOS              ; -- A xxxx     #2 ?
;            JZ PARAMENDOF           ;
;            MOV #0330h,S            ;               OPCODE = 0330h : MOV #-1,dst is coded MOV @R3+,dst
;            CMP #-1,TOS             ; -- A xxxx     #-1 ?
;            JZ PARAMENDOF           ;
;            MOV #0220h,S            ;               OPCODE = 0220h : MOV #4,dst is coded MOV @R2,dst
;            CMP #4,TOS              ; -- A xxxx     #4 ?
;            JZ PARAMENDOF           ;
;            MOV #0230h,S            ;               OPCODE = 0230h : MOV #8,dst is coded MOV @R2+,dst
;            CMP #8,TOS              ; -- A xxxx     #8 ?
;            JZ PARAMENDOF           ;
;; ==================================;
            MOV #0030h,S            ; -- A xxxx     for all other cases : MOV @PC+,dst
; endcase of "&xxxx,"               ;               <== PARAM1AMP
; endcase of ",&xxxx"               ;               <== PARAMXAMP <== PARAM2AMP
StoreArg    MOV &DP,X               ;
            ADD #2,&DP              ;               cell allot for arg
            MOV TOS,0(X)            ;               compile arg
            JMP PARAMENDOF          ;
; ----------------------------------;
QPARAM1AMP  CMP.B #'&',W            ; -- A sep
            JNE QPARAM1AT           ;
; case of "&xxxx,"                  ;               search for "&xxxx,"
PARAM1AMP   MOV #0210h,S            ;               set code type : xxxx(R2) with AS=0b01 ==> x210h
; case of "&xxxx,"|",&xxxx"         ;               <== PARAM2AMP
PARAMXAMP   CALL #SearchARG         ;
            JMP StoreArg            ; -- A          then ret
; ----------------------------------;
QPARAM1AT   CMP.B #'@',W            ; -- A sep
            JNE PARAM1IDX           ;
; case of "@REG,"|"@REG+,"          ;
PARAM1AT    MOV #0020h,S            ; -- A sep      init OPCODE with indirect code type : AS=0b10
            CALL #SkipRSrchRn       ;               Z = not found
            JNZ SWAPREG             ; -- A Rn       REG of "@REG," found
; case of "@REG+,"                  ; -- A addr     search REG of "@REG+"
PARAM1ATPL  MOV #'+',TOS            ; -- A sep
            CALL #SearchRn          ;
            JNZ PARAM1ATPLX         ; -- A Rn       REG found
; ----------------------------------;               REG not found
; case of "xxxx(REG),"              ; -- A sep      OPCODE I
; case of "xxxx(REG)"               ; -- A sep      OPCODE II
PARAM1IDX   CALL #CompIdxSrchRn     ; -- A 000R     compile index xxxx and search REG of "(REG)", abort if xxxx not found
; ==================================;              uncomment below if immediate {#-1 #0 #1 #2 #4 #8} are processed in FastForthREGtoTI.pat
            CMP #3,TOS              ;               0(R3) is used to do constant generator CG2 : #1
            JNZ PARAM1ATPLX         ;               if not
            SUB #2,&DP              ; -- A 0003     if (REG) = (R3), remove arg xxxx 
; ==================================;
; case of "@REG+,"|"xxxx(REG),"     ;               <== PARAM1ATPL OPCODE I
; case of "@REG+"|"xxxx(REG)"       ;               <== PARAM1ATPL OPCODE II
PARAM1ATPLX BIS #0010h,S            ;               AS=0b01 for indexing address, AS=0b11 for @REG+
            MOV #4000h,W            ;2              4000h = first OPCODE type I
            CMP S,W                 ;1              with OPCODE II @REG or xxxx(REG) don't skip CR !
            ADDC #0,&TOIN           ;1              with OPCODE I, @REG+, or xxxx(REG), skip "," ready for the second operand search
; endcase of "@REG,"                ; -- A 000R     <== PARAM1AT
; endcase of "REG,"                 ; -- A 000R     <== PARAM1REG
SWAPREG     SWPB TOS                ; -- A 0R00     swap bytes because it's not a dst REG typeI (not a 2 ops inst.)
; endcase of ",REG"                 ; -- A 0R0D     <== PARAM2REG (dst REG typeI)
; endcase of ",xxxx(REG)"           ; -- A 0R0D     <== PARAM2IDX (dst REG typeI)
OPCODEPLREG ADD TOS,S               ; -- A 0R00|0R0D
; endcase of all                    ;               <== PARAM1SHARP PARAM1AMP PARAM2AMP
PARAMENDOF  MOV @PSP+,TOS           ; -- A
            MOV @IP+,PC             ; -- A          S=OPCODE,T=OPCODEADR
; ----------------------------------;

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER : INTERPRET 2th OPERAND
; ----------------------------------------------------------------------
PARAM2      JNZ     QPARAM2AMP      ; -- A sep      A=@code_instruct, regs: S=code_operand1, T=@compil, SR(Z)=1 if prefix = 'R'
PARAM2REG   CALL    #SearchRn       ; -- A sep      case of ",REG"
            JNZ     OPCODEPLREG     ; -- A 000D     REG of ",REG" found
; ----------------------------------;
QPARAM2AMP  CMP.B   #'&',W          ;
            JNZ     PARAM2IDX       ;               '&' not found
; case of ",&xxxx"                  ;
PARAM2AMP   BIS     #0082h,S        ;               change OPCODE : AD=1, dst = R2
            JMP     PARAMXAMP       ; -- A ' '
; ----------------------------------;
; case of ",xxxx(REG)               ; -- A sep
PARAM2IDX   BIS     #0080h,S        ;               set AD=1
            CALL    #CompIdxSrchRn  ;               compile index xxxx and search REG of ",xxxx(REG)", abort if xxxx not found
            JNZ     OPCODEPLREG     ; -- A 000D     if REG found
            MOV     #NOTFOUND,PC    ;               does ABORT" ?"
; ----------------------------------;

; ----------------------------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER: reset OPCODE in S reg, set OPCODE addr in T reg,
; move Prefix in W reg, skip prefix in input buffer. Flag SR(Z)=1 if prefix = 'R'.
; ----------------------------------------------------------------------------------------
    .IFDEF LARGE_CODE               ;
InitSkipPrfxDble                    ;
            BIS #UF2,SR             ;                   flag used to process ARG of large code|data instructions
    .ENDIF                          ;                   cleared with UF2DROPEXIT (CALLA,MOVA) or UPDATE_XW (opcode V,VI)
InitSkipPrfx                        ;
            MOV #0,S                ;                   reset OPCODE_complement
            MOV &DP,T               ;                   HERE --> OPCODEADR
            ADD #2,&DP              ;                   cell allot for opcode|ext.opcode
; SkipPrfx                          ;                   skip all occurring char 'BL', plus one prefix
SkipPrfx    MOV #20h,W              ;                   W=BL
            MOV &SOURCE_ORG,X       ;   
            ADD &TOIN,X             ;
SKIPLOOP    CMP.B @X+,W             ;                   W=BL  does character match?
            JZ SKIPLOOP             ;   
            MOV.B -1(X),W           ;                   W=prefix
            SUB &SOURCE_ORG,X       ;   
            MOV X,&TOIN             ;                   >IN points after prefix
            CMP.B #'R',W            ;                   preset SR(Z)=1 if prefix = 'R'
            MOV @IP+,PC             ;

; ; ----------------------------------------------------------------------------------------------
; ; DTCforthMSP430FR5xxx ASSEMBLER: OPCODE TYPE 0 : zero operand; done with pre-processor GEMA :-)
; ; ----------------------------------------------------------------------------------------------
;             asmword "RETI"
;             mCOLON
;             .word   lit,1300h,COMMA,EXIT

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER: OPCODES TYPE I : double operand
; ----------------------------------------------------------------------
;               OPCODE(FEDC)
; OPCODE(code)     = 0bxxxx             opcode
;                   OPCODE(BA98)
;                      = 0bxxxx         src_register,
;                       OPCODE(7)       AD (dst addr type)
;                          = 0b0        ,register
;                          = 0b1        ,x(Rn),&adr
;                        OPCODE(6)      size
; OPCODE(B)                 = 0b0       word
;                           = 0b1       byte
;                         OPCODE(54)    AS (src addr type)
; OPCODE(AS)                 = 0b00     register,
;                            = 0b01     x(Rn),&adr,
;                            = 0b10     @Rn,
;                            = 0b11     @Rn+,
;                           OPCODE(3210)
; OPCODE(dst)                  = 0bxxxx ,dst_register
; ----------------------------------------------------------------------

; TYPE1DOES                         ; -- A      A = @code_instruct      
TYPE1DOES   .word   lit,','         ; -- A sep
            .word   InitSkipPrfx    ;           A=@code_instruct, regs: S=0, T=@compil, SR(Z)=1 if prefix = 'R'
            .word   PARAM1          ; -- A      S=OPCODE_complement,T=OPCODEADR
            .word   lit,' '         ; -- A sep 
            .word   SkipPrfx        ;           SR(Z)=1 if prefix = 'R'
            .word   PARAM2          ; -- A      S=OPCODE_complement,T=OPCODEADR
            mHI2LO                  ;
MAKEOPCODE  MOV     @RSP+,IP        ;
            BIS     @TOS,S          ; -- A      code_instruct + OPCODE_complement
            MOV     S,0(T)          ;                       store complete opcode to OPCODEADR
            JMP     PARAMENDOF      ; -- A      then NEXT

            asmword "MOV"
            CALL rDODOES
            .word   TYPE1DOES,4000h
            asmword "MOV.B"
            CALL rDODOES
            .word   TYPE1DOES,4040h
            asmword "ADD"
            CALL rDODOES
            .word   TYPE1DOES,5000h
            asmword "ADD.B"
            CALL rDODOES
            .word   TYPE1DOES,5040h
            asmword "ADDC"
            CALL rDODOES
            .word   TYPE1DOES,6000h
            asmword "ADDC.B"
            CALL rDODOES
            .word   TYPE1DOES,6040h
            asmword "SUBC"
            CALL rDODOES
            .word   TYPE1DOES,7000h
            asmword "SUBC.B"
            CALL rDODOES
            .word   TYPE1DOES,7040h
            asmword "SUB"
            CALL rDODOES
            .word   TYPE1DOES,8000h
            asmword "SUB.B"
            CALL rDODOES
            .word   TYPE1DOES,8040h
            asmword "CMP"
            CALL rDODOES
            .word   TYPE1DOES,9000h
            asmword "CMP.B"
            CALL rDODOES
            .word   TYPE1DOES,9040h
;            asmword "DADD"
;            CALL rDODOES
;            .word   TYPE1DOES,0A000h
;            asmword "DADD.B"
;            CALL rDODOES
;            .word   TYPE1DOES,0A040h
            asmword "BIT"
            CALL rDODOES
            .word   TYPE1DOES,0B000h
            asmword "BIT.B"
            CALL rDODOES
            .word   TYPE1DOES,0B040h
            asmword "BIC"
            CALL rDODOES
            .word   TYPE1DOES,0C000h
            asmword "BIC.B"
            CALL rDODOES
            .word   TYPE1DOES,0C040h
            asmword "BIS"
            CALL rDODOES
            .word   TYPE1DOES,0D000h
            asmword "BIS.B"
            CALL rDODOES
            .word   TYPE1DOES,0D040h
            asmword "XOR"
            CALL rDODOES
            .word   TYPE1DOES,0E000h
            asmword "XOR.B"
            CALL rDODOES
            .word   TYPE1DOES,0E040h
            asmword "AND"
            CALL rDODOES
            .word   TYPE1DOES,0F000h
            asmword "AND.B"
            CALL rDODOES
            .word   TYPE1DOES,0F040h

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER, OPCODES TYPE II : single operand
; ----------------------------------------------------------------------
;               OPCODE(FEDCBA987)
; OPCODE(code)     = 0bxxxxxxxxx
;                        OPCODE(6)      size
; OPCODE(B)                 = 0b0       word
;                           = 0b1       byte
;                         OPCODE(54)    (dst addr type)
; OPCODE(AS)                 = 0b00     register
;                            = 0b01     x(Rn),&adr
;                            = 0b10     @Rn
;                            = 0b11     @Rn+
;                           OPCODE(3210)
; OPCODE(dst)                  = 0bxxxx dst register
; ----------------------------------------------------------------------

TYPE2DOES                           ; -- A      A = @code_instruct 
            .word   LIT,' '         ; -- A sep
            .word   InitSkipPrfx    ;           init S=0, T=@compil, DP=DP+2 then skip prefix, SR(Z)=1 if prefix = 'R'
            .word   PARAM1          ; -- A      
            mHI2LO                  ;
            MOV     S,W             ;
            AND     #0070h,S        ;                   keep B/W & AS infos in OPCODE_complement
            SWPB    W               ;                   (REG org --> REG dst)
            AND     #000Fh,W        ;                   keep REG
BIS_ASMTYPE BIS     W,S             ; -- A add it in OPCODE_complement
            JMP     MAKEOPCODE      ; --                then EXIT

            asmword "RRC"           ; Rotate Right through Carry ( word)
            CALL rDODOES
            .word   TYPE2DOES,1000h
            asmword "RRC.B"         ; Rotate Right through Carry ( byte)
            CALL rDODOES
            .word   TYPE2DOES,1040h
            asmword "SWPB"          ; Swap bytes
            CALL rDODOES
            .word   TYPE2DOES,1080h
            asmword "RRA"
            CALL rDODOES
            .word   TYPE2DOES,1100h
            asmword "RRA.B"
            CALL rDODOES
            .word   TYPE2DOES,1140h
            asmword "SXT"
            CALL rDODOES
            .word   TYPE2DOES,1180h
            asmword "PUSH"
            CALL rDODOES
            .word   TYPE2DOES,1200h
            asmword "PUSH.B"
            CALL rDODOES
            .word   TYPE2DOES,1240h
            asmword "CALL"
            CALL rDODOES
            .word   TYPE2DOES,1280h

; ----------------------------------------------------------------------
; errors output
; ----------------------------------------------------------------------

MUL_REG_ERR ADD     #1,W            ; <== PUSHM|POPM|RRAM|RRUM|RRCM|RLAM error
BRANCH_ERR  MOV     W,TOS           ; <== ASM_branch error
REGNUM_ERR                          ; <== REG number error
            mLO2HI                  ; -- n      n = value out of bounds
            .word   DOT,XSQUOTE
            .byte 13,"out of bounds"
            .word   QABORT_YES

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER, CONDITIONAL BRANCHS
; ----------------------------------------------------------------------
;                       ASSEMBLER       FORTH         OPCODE(FEDC)
; OPCODE(code) for TYPE JNE,JNZ         0<>, <>     = 0x20xx + (offset AND 3FF) ; branch if Z = 0
; OPCODE(code) for TYPE JEQ,JZ          0=, =       = 0x24xx + (offset AND 3FF) ; branch if Z = 1
; OPCODE(code) for TYPE JNC,JLO         U<          = 0x28xx + (offset AND 3FF) ; branch if C = 0
; OPCODE(code) for TYPE JC,JHS          U>=         = 0x2Cxx + (offset AND 3FF) ; branch if C = 1
; OPCODE(code) for TYPE JN              0<          = 0x30xx + (offset AND 3FF) ; branch if N = 1
; OPCODE(code) for TYPE JGE             >=          = 0x34xx + (offset AND 3FF) ; branch if (N xor V) = 0
; OPCODE(code) for TYPE JL              <           = 0x38xx + (offset AND 3FF) ; branch if (N xor V) = 1
; OPCODE(code) for TYPE JMP                         = 0x3Cxx + (offset AND 3FF)

            asmword "S>="           ; if >= assertion (opposite of jump if < )
            CALL rDOCON
            .word   3800h
            asmword "S<"            ; if < assertion
            CALL rDOCON
            .word   3400h
            asmword "0>="           ; if 0>= assertion  ; use only with IF UNTIL WHILE !
            CALL rDOCON
            .word   3000h
            asmword "0<"            ; jump if 0<        ; use only with ?GOTO !
            CALL rDOCON
            .word   3000h
            asmword "U<"            ; if U< assertion
            CALL rDOCON
            .word   2C00h
            asmword "U>="           ; if U>= assertion
            CALL rDOCON
            .word   2800h
            asmword "0<>"           ; if <>0 assertion
            CALL rDOCON
            .word   2400h
            asmword "0="            ; if =0 assertion
            CALL rDOCON
            .word   2000h

;ASM IF      OPCODE -- @OPCODE1
            asmword "IF"
ASM_IF      MOV     &DP,W
            MOV     TOS,0(W)        ; compile incomplete opcode
            ADD     #2,&DP
            MOV     W,TOS
            MOV     @IP+,PC

;ASM THEN     @OPCODE --        resolve forward branch
            asmword "THEN"
ASM_THEN    MOV     &DP,W           ; -- @OPCODE    W=dst
            MOV     TOS,Y           ;               Y=@OPCODE
ASM_THEN1   MOV     @PSP+,TOS       ; --
            MOV     Y,X             ;
            ADD     #2,X            ; --        Y=@OPCODE   W=dst   X=src+2
            SUB     X,W             ; --        Y=@OPCODE   W=dst-src+2=displacement (bytes)
            RRA     W               ; --        Y=@OPCODE   W=displacement (words)
            CMP     #512,W          ;
            JC      BRANCH_ERR      ;           (JHS) unsigned branch if words > 511
            BIS     W,0(Y)          ; --        [@OPCODE]=OPCODE completed
            MOV     @IP+,PC

; ELSE      @OPCODE1 -- @OPCODE2    branch for IF..ELSE
            asmword "ELSE"
ASM_ELSE    MOV     &DP,W           ; --        W=HERE
            MOV     #3C00h,0(W)     ;           compile unconditionnal branch
            ADD     #2,&DP          ; --        DP+2
            SUB     #2,PSP          ;
            MOV     W,0(PSP)        ; -- @OPCODE2 @OPCODE1
            JMP     ASM_THEN        ; -- @OPCODE2

; BEGIN     -- BEGINadr             initialize backward branch
            asmword "BEGIN"
HERE        SUB #2,PSP
            MOV TOS,0(PSP)
            MOV &DP,TOS
            MOV @IP+,PC

; UNTIL     @BEGIN OPCODE --   resolve conditional backward branch
            asmword "UNTIL"         ;  -- @BEGIN OPCODE
ASM_UNTIL   MOV     @PSP+,W         ;  -- OPCODE                        W=@BEGIN
ASM_UNTIL1  MOV     TOS,Y           ;               Y=OPCODE            W=@BEGIN
ASM_UNTIL2  MOV     @PSP+,TOS       ;  --
            MOV     &DP,X           ;  --           Y=OPCODE    X=HERE  W=dst
            SUB     #2,W            ;  --           Y=OPCODE    X=HERE  W=dst-2
            SUB     X,W             ;  --           Y=OPCODE    X=src   W=src-dst-2=displacement (bytes)
            RRA     W               ;  --           Y=OPCODE    X=HERE  W=displacement (words)
            CMP     #-512,W         ;
            JL      BRANCH_ERR      ;               signed branch if displ. < -512 words
            AND     #3FFh,W         ;  --           Y=OPCODE   X=HERE  W=troncated negative displacement (words)
            BIS     W,Y             ;  --           Y=OPCODE (completed)
            MOV     Y,0(X)          ;
            ADD     #2,&DP          ;
            MOV     @IP+,PC         ;

; AGAIN     @BEGIN --      uncond'l backward branch
;   unconditional backward branch
            asmword "AGAIN"
ASM_AGAIN   MOV TOS,W               ;               W=@BEGIN
            MOV #3C00h,Y            ;               Y = asmcode JMP
            JMP ASM_UNTIL2          ;

; WHILE     @BEGIN OPCODE -- @WHILE @BEGIN
            asmword "WHILE"
ASM_WHILE   mCOLON                  ; -- @BEGIN OPCODE
            .word   ASM_IF,SWAP,EXIT

; REPEAT    @WHILE @BEGIN --     resolve WHILE loop
            asmword "REPEAT"
ASM_REPEAT  mCOLON                  ; -- @WHILE @BEGIN
            .word   ASM_AGAIN,ASM_THEN,EXIT

; ------------------------------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER : branch up to 3 backward labels and up to 3 forward labels
; ------------------------------------------------------------------------------------------
; used for non canonical branchs, as BASIC language: "goto line x"
; labels BWx and FWx must be set at the beginning of line (>IN < 8).
; FWx can resolve only one previous GOTO|?GOTO FWx.
; BWx can resolve any subsequent GOTO|?GOTO BWx.

;BACKWDOES   mHI2LO                  ; -- A BODY     A = JMP_OPCODE
;            MOV @RSP+,IP            ; -- A BODY
;            MOV @TOS,TOS            ; -- A ASMBWx   address
;            MOV TOS,Y               ; -- A BODY     Y = ASMBWx addr
;            MOV @PSP+,TOS           ; -- A
;            MOV @Y,W                ;               W = ASMBWx value
;            CMP #8,&TOIN            ;               are we colon 8 or more ?
;BACKWUSE    JC ASM_UNTIL1           ; -- A          yes, W = @BEGIN
;BACKWSET    MOV &DP,0(Y)            ;               no, set LABEL = DP
;            MOV @IP+,PC

BACKWDOES   mHI2LO                  ;
            MOV @RSP+,IP            ;
            MOV TOS,Y               ; -- BODY       Y = ASMBWx addr
            MOV @PSP+,TOS           ; -- 
            MOV @Y,W                ;               W = ASMBWx value
            CMP #8,&TOIN            ;               are we colon 8 or more ?
BACKWUSE    JC ASM_UNTIL1           ; -- A          yes, W = @BEGIN
BACKWSET    MOV &DP,0(Y)            ;               no, set LABEL = DP
            MOV @IP+,PC             ;

; backward label 1
            asmword "BW1"
            CALL rDODOES            ; CFA
            .word BACKWDOES         ; PFA
            .word 0                 ; BODY
; backward label 2
            asmword "BW2"
            CALL rDODOES
            .word BACKWDOES
            .word 0                 ; BODY
; backward label 3
            asmword "BW3"
            CALL rDODOES
            .word BACKWDOES
            .word 0                 ; BODY

FORWDOES    mHI2LO
            MOV @RSP+,IP            ; -- BODY       = ASMFWx adr
            MOV &DP,W               ;
            MOV @TOS,Y              ; -- BODY       Y =ASMFWx value
            MOV #0,0(TOS)           ;               V3.9: clear @OPCODE of FWx to avoid jmp resolution without label
            CMP #8,&TOIN            ;               are we colon 8 or more ?
FORWUSE     JNC ASM_THEN1           ;               no: resolve ASMFWx with W=DP, Y=@OPCODE
FORWSET     MOV @PSP+,0(W)          ;               yes compile opcode (without displacement)
            ADD #2,&DP              ;                   increment DP
            MOV W,0(TOS)            ;                   store @OPCODE to ASMFWx adr
            MOV @PSP+,TOS           ; --
            MOV @IP+,PC                 ;

; forward label 1
            asmword "FW1"
            CALL rDODOES            ; CFA
            .word FORWDOES          ; PFA
            .word 0                 ; BODY
; forward label 2
            asmword "FW2"
            CALL rDODOES
            .word FORWDOES
            .word 0                 ; BODY
; forward label 3
            asmword "FW3"
            CALL rDODOES
            .word FORWDOES
            .word 0                 ; BODY

;ASM    GOTO <label>                   --       unconditionnal branch to label
            asmword "GOTO"
            SUB #2,PSP              ;
            MOV TOS,0(PSP)          ;
            MOV #3C00h,TOS          ;  -- JMP_OPCODE
GOTONEXT    mCOLON                  ;
            .word   TICK            ;  -- OPCODE CFA<label>
            .word   EXECUTE,EXIT    ;

;ASM    <cond> ?GOTO <label>    OPCODE --       conditionnal branch to label
            asmword "?GOTO"
INVJMP      CMP #3000h,TOS          ; invert code jump process
            JZ GOTONEXT             ; case of JN, do nothing
            XOR #0400h,TOS          ; case of: JNZ<-->JZ  JNC<-->JC  JL<-->JGE
            BIT #1000h,TOS          ; 3xxxh case ?
            JZ  GOTONEXT            ; no
            XOR #0800h,TOS          ; complementary action for JL<-->JGE
            JMP GOTONEXT

; --------------------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER, OPCODES TYPE III : PUSHM|POPM|RLAM|RRAM|RRUM|RRCM
; --------------------------------------------------------------------------------
; PUSHM, syntax:    PUSHM #n,REG  with 0 < n < 17
; POPM syntax:       POPM #n,REG  with 0 < n < 17


; PUSHM order : PSP,TOS, IP,  S,  T,  W,  X,  Y, rEXIT,rDOVAR,rDOCON, rDODOES, R3, SR,RSP, PC
; PUSHM order : R15,R14,R13,R12,R11,R10, R9, R8,  R7  ,  R6  ,  R5  ,   R4   , R3, R2, R1, R0

; example : PUSHM #6,IP pushes IP,S,T,W,X,Y registers to return stack
;
; POPM  order :  PC,RSP, SR, R3, rDODOES,rDOCON,rDOVAR,rEXIT,  Y,  X,  W,  T,  S, IP,TOS,PSP
; POPM  order :  R0, R1, R2, R3,   R4   ,  R5  ,  R6  ,  R7 , R8, R9,R10,R11,R12,R13,R14,R15

; example : POPM #6,IP   pulls Y,X,W,T,S,IP registers from return stack

; RxxM syntax: RxxM #n,REG  with 0 < n < 5

TYPE3DOES                           ; -- @code_instruct
            .word   LIT,','         ; -- @code_instruct ','
            .word   SkipPrfx        ;
            .word   WORDD,QNUMBER   ;
            .word   QFBRAN,FNOTFOUND;                       see INTERPRET
            .word   LIT,' '         ; -- @code_instruct n sep
            .word   InitSkipPrfx    ;
            .word   PARAM2          ; -- @code_instruct n   S=OPCODE_complement = 0x000R
            mHI2LO                  ;
            MOV     TOS,W           ; -- @code_instruct n         W = n
            MOV     @PSP+,TOS       ; -- @code_instruct
            SUB     #1,W            ;                       W = n floored to 0
            JN      MUL_REG_ERR     ;
            MOV     @TOS,X          ;                       X=OPCODE
            RLAM    #4,X            ;                       OPCODE bit 1000h --> C
            JNC     RxxMINSTRU      ;                       if bit 1000h = 0
PxxxINSTRU  MOV     S,Y             ;                       S=REG, Y=REG to test
            RLAM    #3,X            ;                       OPCODE bit 0200h --> C
            JNC     PUSHMINSTRU     ;                       W=n-1 Y=REG
POPMINSTRU  SUB     W,S             ;                       to make POPM opcode, compute first REG to POP; TI is complicated....
PUSHMINSTRU SUB     W,Y             ;                       Y=REG-(n-1)
            CMP     #16,Y           ;
            JC      MUL_REG_ERR     ;                       JC=JHS    (U>=)
            RLAM    #4,W            ;                       W = n << 4
            JMP     BIS_ASMTYPE     ;
RxxMINSTRU  CMP     #4,W            ;
            JC      MUL_REG_ERR     ;                       JC=JHS    (U>=)
            SWPB    W               ;                       W = n << 8
            RLAM    #2,W            ;                       W = N << 10
            JMP     BIS_ASMTYPE     ; -- @code_instruct

            asmword "RRCM"
            CALL rDODOES
            .word   TYPE3DOES,0050h
            asmword "RRAM"
            CALL rDODOES
            .word   TYPE3DOES,0150h
            asmword "RLAM"
            CALL rDODOES
            .word   TYPE3DOES,0250h
            asmword "RRUM"
            CALL rDODOES
            .word   TYPE3DOES,0350h
            asmword "PUSHM"
            CALL rDODOES
            .word   TYPE3DOES,1500h
            asmword "POPM"
            CALL rDODOES
            .word   TYPE3DOES,1700h

    .IFDEF LARGE_CODE
            asmword "RRCM.A"
            CALL rDODOES
            .word   TYPE3DOES,0040h
            asmword "RRAM.A"
            CALL rDODOES
            .word   TYPE3DOES,0140h
            asmword "RLAM.A"
            CALL rDODOES
            .word   TYPE3DOES,0240h
            asmword "RRUM.A"
            CALL rDODOES
            .word   TYPE3DOES,0340h
            asmword "PUSHM.A"
            CALL rDODOES
            .word   TYPE3DOES,1400h
            asmword "POPM.A"
            CALL rDODOES
            .word   TYPE3DOES,1600h

; --------------------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER:  OPCODE TYPE III bis, one operand without extended word: CALLA
; --------------------------------------------------------------------------------
            asmword "CALLA"
            mCOLON                  ;
            .word   LIT,' '         ; -- sep
            .word  InitSkipPrfxDble ; -- sep    set ASM extended instructions flag to process double numbers
            mHI2LO                  ;           init S=0, T=@compil, DP=DP+2, UF2=1, skip prefix, SR(Z)=1 if prefix = 'R'
CALLA0      MOV #134h,S             ;           134h<<4 = 1340h = opcode for CALLA Rn
            JNZ CALLA1              ; -- sep    if prefix <> 'R'
CALLA01     CALL #SearchRn          ; -- Rn
CALLA02     RLAM #4,S               ;           (opcode>>4)<<4 = opcode
            BIS TOS,S               ;           update opcode with Rn|$x
            MOV S,0(T)              ;           store opcode
;-----------------------------------;
UF2DROPEXIT BIC #UF2,SR             ;           clear double number process flag
            MOV #DROPEXIT,PC        ;
;-----------------------------------;
CALLA1      ADD #2,S                ; -- sep    136h<<4 = opcode for CALLA @REG
            CMP.B #'@',W            ;           Search @REG
            JNZ CALLA2              ;
CALLA11     CALL #SkipRSrchRn       ; -- Rn
            JNZ  CALLA02            ;           if REG found, update opcode
;-----------------------------------;
            ADD #1,S                ;           137h<<4 = opcode for CALLA @REG+
            MOV #'+',TOS            ; -- sep
            JMP CALLA01             ;
;-----------------------------------;
CALLA2      ADD #2,&DP              ; -- sep    make room for $llll of #$hllll|&$hllll|$llll(REG)
;-----------------------------------;
            CMP.B #'#',W            ;
            JNZ CALLA3              ;
            MOV #13Bh,S             ;           13Bh<<4 = opcode for CALLA #$xxxxx
CALLA21     CALL #SearchARG         ; -- Lo Hi
            MOV @PSP+,2(T)          ; -- Hi     store $llll of #$hllll|&$hllll
            JMP CALLA02             ;           update opcode with $h and store opcode
;-----------------------------------;
CALLA3      CMP.B #'&',W            ; -- sep
            JNZ CALLA4              ;
            ADD #2,S                ;           138h<<4 = opcode for CALLA &$xxxxx
            JMP CALLA21             ;
;-----------------------------------;
CALLA4      SUB #1,S                ;           135h<<4 = opcode for CALLA $xxxx(REG)
CALLA41     CALL #SearchIndex       ; -- Lo Hi
            MOV @PSP+,2(T)          ; -- Hi     store $llll of $llll(REG)
            MOV #')',TOS            ; -- sep
            JMP CALLA11             ;           search Rn and update opcode
;-----------------------------------;

; --------------------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER, OPCODES IV, 2 operands without extended word: MOVA|ADDA|CMPA|SUBA
; --------------------------------------------------------------------------------
; MOVA {Rsrc #$hllll &$hllll @Rsrc @Rsrc+ $llll(Rsrc)} , Rdst
; MOVA Rsrc , {Rdst &$hllll $llll(Rdst)}
; ADDA {Rsrc #$hllll} , Rdst
; CMPA {Rsrc #$hllll} , Rdst
; SUBA {Rsrc #$hllll} , Rdst

; first operand process MCAS1       ; -- A          A=@code_instruct S=0, T=@compil, DP=DP+2, SR(UF2)=1, SR(Z)=1 if prefix = 'R'
MCAS1       MOV @PSP+,S             ; -- sep
            MOV @S,S                ;               S=code_instruct
;-----------------------------------; -- sep
MCAS10      JNZ MCAS11              ; -- sep if prefix <> 'R'
MCAS101     CALL #SearchRn          ; -- Rn
MCAS102     RLAM #4,TOS             ;
            RLAM #4,TOS             ; -- 8<<Rn (src)
MCAS103     BIS S,TOS               ; -- opcode     update opcode with src|dst
            MOV TOS,0(T)            ;               save opcode
            MOV T,TOS               ; -- A
            MOV @IP+,PC             ; -- A
;-----------------------------------;
MCAS11      CMP.B #'#',W            ; -- sep        X=addr
            JNE MOVA12              ;
            BIC #40h,S              ;               set #opcode_compl to do #imm,Rdst
MCAS111     ADD #2,&DP              ;               make room for low #$xxxxx|&$xxxxx
            CALL #SearchARG         ; --  Lo Hi
            MOV @PSP+,2(T)          ; --  Hi        store $xxxx of #$xxxxx|&$xxxxx
            JMP MCAS102             ;
;-----------------------------------;
MOVA12      CMP #00C0h,S            ;               MOVA default opcode (MOVA Rsrc,Rdst) ?
            JZ MOVA121              ;                   if yes
MOVA120     MOV PSP,TOS             ;
            MOV #0,0(TOS)           ;
            MOV #NOTFOUND,PC        ;                   else does ABORT" ?"
;-----------------------------------;
MOVA121     MOV #0020h,S            ;               opcode for MOVA &$hllll,Rdst
            CMP.B #'&',W            ; --  sep       case of MOVA &$xxxxx
            JZ MCAS111              ;
;-----------------------------------;
MOVA13      MOV #0,S                ;               set MOVA @REG,Rdst opcode
            CMP.B #'@',W            ; --  sep
            JNZ MOVA14              ;
            CALL #SkipRSrchRn       ; --  Rsrc
            JNZ MCAS102             ;               if @REG found
            MOV #0010h,S            ;               set @REG+,Rdst opcode
            MOV #'+',TOS            ; --  '+'
MOVA131     CALL #SearchRn          ; --  Rsrc      case of MOVA @REG,|@REG+,|$x.xxxx(REG),
MOVA132     ADD #1,&TOIN            ;               skip "," ready for the second operand search
            JMP MCAS102             ;
;-----------------------------------;
MOVA14      MOV #0030h,S            ; --  sep       set llll(REG),Rdst opcode
            ADD #2,&DP              ;               make room for first $xxxx of $xxxx(REG),
            CALL #SearchIndex       ; --  $llll $000h
            MOV @PSP+,2(T)          ; --  0         store $llll as 2th word
            MOV #')',TOS            ; --  ')'
            CALL #SkipRSrchRn       ; --  Rn
            JMP MOVA132             ;

; 2th operand process MCAS2
;-----------------------------------; -- A sep
MCAS2       MOV @PSP+,T             ; -- sep        T=@code_instruct
            MOV @T,S                ;               S=code_instruct
;-----------------------------------; -- sep
MCAS20      JNZ MOVA21              ; -- sep        if prefix <> 'R'
MCAS201     CALL #SearchRn          ; -- Rn
            JMP MCAS103             ;
;-----------------------------------;
MOVA21      CMP.B #0C0h,S           ;               MOVA default opcode(7-0) (MOVA Rsrc,Rdst) ?
            JNZ MOVA120             ;               does ABORT" ?" if not
;-----------------------------------;
            ADD #2,&DP              ;               make room for $llll
            CMP.B #'&',W            ;
            JNZ MOVA22              ;
            SUB #060h,S             ;               set MOVA Rsrc,&$hllll opcode = 60h
            CALL #SearchARG         ; -- Lo Hi
            MOV @PSP+,2(T)          ; -- Hi         store $llll as 2th word
            JMP MCAS103             ; -- Hi         update opcode with dst $h and write opcode
;-----------------------------------;
MOVA22      SUB #050h,S             ;               set MOVA Rsrc,$llll(REG) opcode = 70h
            CALL #SearchIndex       ; -- $llll $0000
            MOV @PSP+,2(T)          ; -- $0000      write $llll of ,$llll(REG) as 2th word
            MOV #')',TOS            ; -- ")"        as WORD separator to find REG of "$llll(REG),"
            CALL #SkipRSrchRn       ; -- Rn
            JMP MCAS103             ;
;-----------------------------------;

TYPE4DOES   .word   lit,','         ; -- A=@code_instruct ","   char separator for PARAM1
            .word  InitSkipPrfxDble ;                           SR(Z)=1 if prefix = 'R'
            .word   MCAS1           ; -- A=@code_instruct
            .word   LIT,' '         ; -- A=@code_instruct " "   char separator for PARAM2
            .word   SkipPrfx        ;                           SR(Z)=1 if prefix = 'R'
            .word   MCAS2           ; -- A=@code_instruct
            .word   UF2DROPEXIT

            asmword "MOVA"
            CALL rDODOES
            .word   TYPE4DOES,00C0h ; opcode for MOVA Rsrc,Rdst
            asmword "CMPA"
            CALL rDODOES
            .word   TYPE4DOES,00D0h ; opcode for CMPA Rsrc,Rdst
            asmword "ADDA"
            CALL rDODOES
            .word   TYPE4DOES,00E0h ; opcode for ADDA Rsrc,Rdst
            asmword "SUBA"
            CALL rDODOES
            .word   TYPE4DOES,00F0h ; opcode for SUBA Rsrc,Rdst

    .ENDIF ; LARGE_CODE
    .IFDEF LARGE_DATA      ;
; --------------------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER, OPCODES V double operand with extended word
; --------------------------------------------------------------------------------

; PRMX1 is used for OPCODES type V / VI (double operand / single operand) with extended word
; A = @ext_word, B = >IN, S=0, T=@compil, SR(UF2)=1, SR(Z)=1 if prefix = 'R'
;-----------------------------------; -- A B            
PRMX10      JNZ PRMX11              ; -- A B ','        if prefix <> 'R'
PRMX101     CALL #SearchRn          ; -- A B Rsrc       Rn of REG; call SearchRn  only to update >IN
PRMX102     MOV @PSP+,TOS           ; -- A B
            MOV @IP+,PC             ; -- A B
;-----------------------------------;
PRMX11      CMP.B #'#',W            ; -- A B ','
            JNZ PRMX12              ;
PRMX111     CALL #SearchARG         ; -- A B Lo Hi      search $xxxxx of #xxxxx,
PRMX112     ADD #2,PSP              ; -- A B Hi         pop unused low word
PRMX114     RLAM #3,TOS             ;
            RLAM #4,TOS             ; -- A B 7<<Hi
PRMX115     BIS TOS,S               ; -- A B Ext_Word   set S with index|immediate|indirect
            JMP PRMX102             ;
;-----------------------------------;
PRMX12      CMP.B #'&',W            ; -- A B ','
            JZ PRMX111              ;                   to update ext_word with indirect
;-----------------------------------;
PRMX13      CMP.B #'@',W            ; -- A B ','
            JNZ PRMX14              ;
PRMX131     CALL #SkipRSrchRn       ; -- A B Rsrc       Rsrc of @REG,
            JNZ PRMX102             ;                   if Rsrc found, nothing to update
;-----------------------------------;
            MOV #'+',TOS            ; -- A B '+'
PRMX133     CALL #SearchRn          ; -- A B Rsrc       Rn of @REG+,
PRMX134     CMP &SOURCE_LEN,&TOIN   ; -- A B $000h<<7|Src   case of TYPE VI one operand (without ',')
            JZ PRMX102              ;                       don't skip CR !
            ADD #1,&TOIN            ;                   skip ',' ready to search 2th operand
            JMP PRMX102             ;
;-----------------------------------;
PRMX14      CALL #SearchIndex       ; -- A B $llll $hhhh
            ADD #2,PSP              ; -- A B $hhhh
            AND #0Fh,TOS            ;                   
            RLAM #4,TOS             ;
            RLAM #3,TOS             ; -- A B $000h<<7
PRMX141     BIS TOS,S               ;                   update S with index|immediate|indirect
            MOV #')',TOS            ; -- A ')'
            CALL #SkipRSrchRn       ; -- A Rn
            JMP PRMX134             ; -- A B $000h<<7
;-----------------------------------;

; PRMX2 is used for OPCODES type V: double operand with extended word
;-----------------------------------;
PRMX20      JZ  PRMX101             ; -- A B sep    if prefix = 'R'
;-----------------------------------;
PRMX21      CMP.B #'&',W            ;
            JNZ PRMX22              ;
PRMX211     CALL #SearchARG         ; -- A B Lo Hi
PRMX213     ADD #2,PSP              ; -- A B hi     pop low word
            JMP PRMX115             ;               update Extended word with dst_Hi
;-----------------------------------;
PRMX22      CALL #SearchIndex       ; -- A B $llll $hhhh
            ADD #2,PSP              ; -- A B Shhhh
            AND #0Fh,TOS            ; -- A B $000h                
            JMP PRMX141             ; -- A B $000h
;-----------------------------------;

;-----------------------------------;
; UPDATE_eXtendedWord               ; -- A B            A = @ext_word, B = >IN
;-----------------------------------;
UPDATE_XW   MOV TOS,&TOIN           ; -- A B            restore >IN at the start of instruction string
            MOV @PSP+,TOS           ; -- A              A = @ext_word
            BIS @TOS+,S             ; -- A+2            A+2 = @opcode, update S with default Extended_Word
            BIS &RPT_WORD,S         ;                   update S with RPT_WORD
            MOV S,0(T)              ;                   store S = completed ext_word at @ext_word
            MOV #0,&RPT_WORD        ;                   clear RPT_WORD
            BIC #UF2,SR             ;                   clear UF2 flag              
            MOV @IP+,PC             ; -- A+2            @opcode
;-----------------------------------;

; these instructions below are processed in two passes:
; pass 1: extended word process by TYPE6DOES with [@ext_word] value
; pass 2: instruction process by TYPE2DOES with [@opcode] value ( = [@ext_word+2] value)
TYPE5DOES                               ; -- A          A = @ext_word
            .word   LIT,TOIN,FETCH      ; -- A B        A = @ext_word, B = >IN
            .word   lit,','             ; -- A B ','    sep for PRMX1
            .word   InitSkipPrfxDble    ;
            .word   PRMX10              ; -- A B     
            .word   LIT,' '             ; -- A B ' '    sep for PRMX2
            .word   SkipPrfx,PRMX20     ; -- A B
            .word   UPDATE_XW           ; -- @opcode                    >IN is restored ready for 2th pass
            .word   BRAN,TYPE1DOES      ; -- @opcode                    2th pass: completes instruction with opcode = [@opcode]

            asmword "MOVX"
            CALL rDODOES
            .word   TYPE5DOES   ; [PFADOES] = TYPE5DOES
            .word   1840h       ; [@ext_word] = extension word default value + A/L bit
            .word   4000h       ; [@opcode]   = CODE_INSTRCT
            asmword "MOVX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,4040h
            asmword "MOVX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,4040h
            asmword "ADDX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,5000h
            asmword "ADDX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,5040h
            asmword "ADDX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,5040h
            asmword "ADDCX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,6000h
            asmword "ADDCX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,6040h
            asmword "ADDCX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,6040h
            asmword "SUBCX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,7000h
            asmword "SUBCX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,7040h
            asmword "SUBCX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,7040h
            asmword "SUBX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,8000h
            asmword "SUBX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,8040h
            asmword "SUBX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,8040h
            asmword "CMPX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,9000h
            asmword "CMPX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,9040h
            asmword "CMPX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,9040h
;            asmword "DADDX"
;            CALL rDODOES
;            .word   TYPE5DOES,1840h,0A000h
;            asmword "DADDX.A"
;            CALL rDODOES
;            .word   TYPE5DOES,1800h,0A040h
;            asmword "DADDX.B"
;            CALL rDODOES
;            .word   TYPE5DOES,1840h,0A040h
            asmword "BITX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0B000h
            asmword "BITX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,0B040h
            asmword "BITX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0B040h
            asmword "BICX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0C000h
            asmword "BICX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,0C040h
            asmword "BICX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0C040h
            asmword "BISX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0D000h
            asmword "BISX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,0D040h
            asmword "BISX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0D040h
            asmword "XORX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0E000h
            asmword "XORX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,0E040h
            asmword "XORX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0E040h
            asmword "ANDX"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0F000h
            asmword "ANDX.A"
            CALL rDODOES
            .word   TYPE5DOES,1800h,0F040h
            asmword "ANDX.B"
            CALL rDODOES
            .word   TYPE5DOES,1840h,0F040h

; --------------------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER, OPCODES VI extended single operand with take count of RPT
; --------------------------------------------------------------------------------

; these instructions below are processed in two passes:
; pass 1: extended word process by TYPE6DOES with [@ext_word] value
; pass 2: instruction process by TYPE2DOES with [opcode_adr ] value ( = [@ext_word+2] value)

TYPE6DOES                               ; -- @ext_word
            .word   LIT,TOIN,FETCH      ; -- A B        as -- @ext_word >IN
            .word   lit,','             ; -- A B ','    sep for PRMX1
            .word   InitSkipPrfxDble    ;
            .word   PRMX10              ; -- A B        enable RPT instruction + double number process
            .word   UPDATE_XW           ; -- @opcode    >IN is restored ready for 2th pass
            .word   BRAN,TYPE2DOES      ; -- @opcode    pass 2: completes instruction

            asmword "RRCX"              ;   ZC = 0      use carry
            CALL rDODOES
            .word   TYPE6DOES,1840h,1000h;
            asmword "RRCX.A"            ;   ZC = 0      use carry
            CALL rDODOES
            .word   TYPE6DOES,1800h,1040h
            asmword "RRCX.B"            ;   ZC = 0      use carry
            CALL rDODOES
            .word   TYPE6DOES,1840h,1040h
            asmword "RRUX"              ;   ZC = 1      carry = 0
            CALL rDODOES
            .word   TYPE6DOES,1940h,1000h
            asmword "RRUX.A"            ;   ZC = 1      carry = 0
            CALL rDODOES
            .word   TYPE6DOES,1900h,1040h
            asmword "RRUX.B"            ;   ZC = 1      carry = 0
            CALL rDODOES
            .word   TYPE6DOES,1940h,1040h
            asmword "SWPBX"
            CALL rDODOES
            .word   TYPE6DOES,1840h,1080h
            asmword "SWPBX.A"
            CALL rDODOES
            .word   TYPE6DOES,1800h,1080h
            asmword "RRAX"
            CALL rDODOES
            .word   TYPE6DOES,1840h,1100h
            asmword "RRAX.A"
            CALL rDODOES
            .word   TYPE6DOES,1800h,1140h
            asmword "RRAX.B"
            CALL rDODOES
            .word   TYPE6DOES,1840h,1140h
            asmword "SXTX"
            CALL rDODOES
            .word   TYPE6DOES,1840h,1180h
            asmword "SXTX.A"
            CALL rDODOES
            .word   TYPE6DOES,1800h,1180h
            asmword "PUSHX"
            CALL rDODOES
            .word   TYPE6DOES,1840h,1200h
            asmword "PUSHX.A"
            CALL rDODOES
            .word   TYPE6DOES,1800h,1240h
            asmword "PUSHX.B"
            CALL rDODOES
            .word   TYPE6DOES,1840h,1240h

; ----------------------------------------------------------------------
; DTCforthMSP430FR5xxx ASSEMBLER, RPT instruction defined before eXtended instructions
; used only with instructions type 6 : RRAX,RRCX,RRUX
; ----------------------------------------------------------------------
; RPT #1 is coded 0 in repetition count field (count n-1)
; please note that "RPT Rn" with [Rn]=0 has same effect as "RPT #1"

RPT_WORD    .word 0

            asmword "RPT"           ; RPT #n | RPT Rn     repeat (n|[Rn])+1 times modulo 16
            mCOLON                  ;
            .word   LIT,' '         ;
            .word   SkipPrfx        ; -- sep
            mHI2LO                  ;
            JNZ RPT1                ; -- sep        if prefix <> 'R'
            CALL #SearchRn          ; -- Rn
            BIS #80h,TOS            ; -- flag+Rn    set Rn flag
            JMP RPT2                ;
RPT1        CALL #SearchARG         ; -- ARG
            SUB #1,TOS              ; -- ARG-1
            AND #0Fh,TOS            ; -- $000x      four repeat bits mask
RPT2        MOV TOS,&RPT_WORD       ;
            MOV @PSP+,TOS           ;
            MOV @RSP+,IP            ;
            MOV @IP+,PC             ;
    .ENDIF ; LARGE_DATA
