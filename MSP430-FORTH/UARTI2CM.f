\ -*- coding: utf-8 -*-
\
\ TARGET SELECTION ( = the name of \INC\target.pat file)
\ MSP_EXP430FR5739  MSP_EXP430FR5969    MSP_EXP430FR5994    MSP_EXP430FR6989
\ MSP_EXP430FR4133 (can't use LED1 because wired on UART TX)
\ MSP_EXP430FR2433  CHIPSTICK_FR2433    MSP_EXP430FR2355
\ LP_MSP430FR2476
\
\ from scite editor : copy your target selection in parameter 1 (shift+F8):
\
\ OR
\
\ drag and drop this file onto SendSourceFileToTarget.bat
\ then select your TARGET when asked.
\
\
\ FastForth kernel compilation minimal options:
\ TERMINAL3WIRES | TERMINAL4WIRES
\
\ see symbolic values in \inc\launchpad.pat or/and in \inc\device.pat
\
\ ================================================================================
\ coupled to a PL2303HXD/GC cable, this driver enables a FastForth target to act as USB to I2C_Slave bridge,
\ thus, from TERATERM.exe you can take the entire control of up to 112 I2C_FastForth targets.
\ In addition, it simulates a full duplex communication while the I2C bus is only half duplex.
\ ================================================================================

\ ------------------------------------------------- 
; UARTI2CM.f  \I2C to UART bridge for I2C_FastForth\ -------------------------->+
\ -------------------------------------------------                             |
\                                                                               |
\ ------------------------------                                                |
\ see forthMSP430FR_TERM_I2C.asm                                                |
\ ------------------------------                                                |
\        |                                                                      |
\        |                                                                      |
\        |             GND------------------------------GND                     |
\        |             3V3-------------o---o------------3V3                     |
\        |                             |   |                                    | 
\        |                             1   1                                    | 
\        |                             k   k                Txy.z output        | 
\        v                             0   0                     to             v             GND---------------------------------------------GND 
\   I2C_FastForth                      |   |                  Px.y int       UARTI2CM              +-------------------------------------->+
\     (hardware         +<-------------|---o------------>+     jumper       (Software              |    +<----------------------------+    |
\     I2C Slave)        ^      +<------o----------+      ^     +--->+       I2C Master)            |    |    +------(option)---->+    |    |
\                       v      v                  ^      v     ^    |                              ^    v    ^                   v    ^    v
\ I2C_FastForth(s)     SDA    SCL  connected to: SCL    SDA    |    v   I2C_to_UART_bridge        TXD  RXD  RTS  connected to : CTS  TXD  RXD  UARTtoUSB <--> COMx <--> TERMINAL
\ ------------------   ----   ----               ----   ----             ----------------         ---  ---  ---                 ---  ---  ---  ---------      ----      --------
\ MSP_EXP430FR2355     P1.2   P1.3               P3.3   P3.2  P1.7 P1.6  MSP_EXP430FR2355 (24MHz) P4.3 P4.2 P2.0                               PL2303GC                    |      
\ MSP_EXP430FR5739     P1.6   P1.7               P4.1   P4.0  P1.1 P1.0  MSP_EXP430FR5739 (24MHz) P2.0 P2.1 P2.2                               PL2303HXD                   v
\ MSP_EXP430FR5969     P1.6   P1.7               P1.3   P1.2  P2.2 P3.4  MSP_EXP430FR5969 (16MHz) P2.0 P2.1 P4.1                               PL2303TA               TERATERM.EXE     
\ MSP_EXP430FR5994     P7.0   P7.1               P8.1   P8.2  P1.5 P1.4  MSP_EXP430FR5994 (16MHz) P2.0 P2.1 P4.2                               CP2102                      ^ 
\ MSP_EXP430FR6989     P1.6   P1.7               P1.5   P1.3  P3.6 P3.7  MSP_EXP430FR6989 (16MHz) P3.4 P3.5 P3.0                                                           |       
\ MSP_EXP430FR4133     P5.2   P5.3               P8.3   P8.2  P1.6 P1.7  MSP_EXP430FR4133 (16MHz) P1.0 P1.1 P2.3                                                           |       
\ MSP_EXP430FR2433     P1.2   P1.3               P3.1   P3.2  P1.2 P1.3  MSP_EXP430FR2433 (16MHz) P1.4 P1.5 P1.0                                                           |           
\ LP_MSP430FR2476      P4.4   P4.3               P3.3   P3.2  P1.2 P1.1  LP_MSP430FR2476  (16MHz) P1.4 P1.5 P6.1                                                           |                                                                     
\ MY_MSP430FR5738_2    P1.6   P1.7               P1.3   P1.2  P1.1 P1.0 MY_MSP430FR5738_2 (24MHz) P2.0 P2.1 P2.2                                                           |
\                                                -----------                    ^                                                                                          |
\ MSP_EXP430FR2355     P1.2   P1.3               P1.4   P1.2  P6.1 P4.4 FP_MAIN |         (24MHz) P4.3 P4.2 P2.0                                                           |      
\                                                -----------                    |                                                                                          |
\ FP_XLR               P1.4   P1.2               P3.3   P3.2  P1.7 P1.6  MSP_EXP430FR2355 (24MHz) P4.3 P4.2 P4.1                                                           |      
\                 GND  P1.3   P1.1  DPA+                                        ^                                                                                          |
\                                                                               |                                                                                          |
\                                                                              SW2                                                                                       ALT+B
\                                                                               ^                                                                                          ^
\                                                                               |                                                                                          |
\                                                                        to QUIT UARTI2CM                                                                           to QUIT UARTI2CM
\
\ =============================================================================================
\ don't forget to link 3V3 and GND on each side and to add 1k0 pullup resistors on SDA and SCL.
\ =============================================================================================
\ don't forget to set the jumper Txy.z <--> Px.y
\ =============================================================================================
\ don't forget to remove the jumpers SBWTCK & SBWTDIO from the unpowered launchpad if any
\ =============================================================================================
\
\ if you want to see what is happening on the I2C bus with an oscilloscope, pay attention to the capacitance of the probes, 
\ switch them from x1 to x10. 
\
\ ------------
\ how it works
\ ------------
\
\ 1- the I2C bus is Master to Slave oriented, the Slave does not decide anything.
\    The TERMINAL is connected to the I2C Master device, the FastForth target is the I2C Slave.
\    Once the Master to Slave link is made, we have to find a trick to reverse the roles, 
\    so that the Slave can take control of the data exchange.
\
\ 2- The I2C bus is half duplex. 
\    Another trick will be to simulate an I2C_Master TERMINAL in Full Duplex mode.
\
\ 3- without forgetting a visual effect to show the lost of I2C_Slave.
\
\ Solution:
\ To take control of the master, the slave emits one of 6 CTRL-Chars:
\   CTRL-Char $00 sent by ACCEPT (before falling asleep in BACKGRND),
\   CTRL-Char $01 sent by KEY to receive one char from TERMINAL,
\   CTRL-Char $03 sent by the user to logout I2C_MASTER,
\   CTRL-Char $04 sent by NOECHO to switch the UART in half-duplex mode,
\   CTRL-Char $05 sent by ECHO to switch the UART in full duplex mode,
\   CTRL-Char $FF sent by ABORT" to abort the file downloading if any,
\                                 followed by a reSTART MASTER RX to display the ABORT" message.
\   More, if the master receives a $FF as char (case of RSTSYSIV event on I2C_Slave side), 
\   it considers the link broken and performs ABORT, which forces a reSTART RX into a 500 ms loop with an appropriate visual effect.
\   All this guarantees a perfect hot swap of any I2C_slave.
\
\ Once the slave sends the CTRL_Char $00, it falls asleep, 
\ On its receipt, the master sends an UART RXON then falls down to sleep awaiting a UART RX interruption from TERMINAL.
\ As long as the TERMINAL is silent, the master and the slave remain in SLEEP mode,
\ SLEEP mode is LPM0 (else UART does not wake up) for the master (650 uA), LPM4 for the slave (1.5 uA).
\
\ interruptions
\ -------------
\ Since the slave can't wake up the master with a dedicated wired interrupt, 
\ the master must generate one cyclically to listen to the slave.
\ TICK_INT is used to generate a 500 ms interrupt, obviously taken into account only when the master goes to sleep.
\ It performs a (re)START I2C RX that enables the I2C link to be re-established following a RESET performed on I2C_Slave side.
\
\ This interruption also allows to exit the UARTI2CM program when user sends a software BREAK (Teraterm(Alt-B)).
\
\ To avoid locking, we have to ensure TERM_INT priority greater than TICK_INT. As MSP430FR2xxx don't have timer with lower priority than eUSCI,
\ we link the timer output pin with a contiguous pin with lower interrupt than TERM_INT to do this.
\
\
\ driver test : MCLK=24MHz, PL2303CG with shortened cable (20cm), WIFI off, all windows apps closed else Scite and TERATERM.
\ -----------                                                                                    .
\                                                                                               .         ┌────────────────────────────────┐
\     notebook                                  USB to I2C bridge                              +-- I2C -->| up to 112 I2C_FASTFORTH targets|
\ ┌───────────────┐          ╔════════════════════════════════════════════════════════════╗   /         ┌───────────────────────────────┐  |
\ |   WINDOWS 10  |          ║ PL2303GC/HXD/TA            this launchpad running UARTI2CM ║  +-- I2C -->|    MSP430FR4133 @ 1 MHz       |  |
\ |               |          ║───────────────┐           ┌────────────────────────────────║ /        ┌───────────────────────────────┐  |──┘
\ |               |          ║               |  3 wires  |    MSP430FR2355 @ 24MHz        ║/         |    MSP430FR5738 @ 24 MHz      |  |
\ |   TERATERM   -o--> USB --o--> USB2UART --o--> UART --o--> FASTFORTH  +  UARTI2CM    --o--> I2C --o-->     I2C_FASTFORTH          |──┘
\ |   terminal    |          ║               |   6 MBds  |     (software I2C MASTER)      ║          |     (hardware I2C SLAVE)      | 
\ |               |          ║───────────────┘           └────────────────────────────────║          └───────────────────────────────┘
\ |               |          ║               |<- l=20cm->|                                ║<-l=20cm->| 
\ └───────────────┘          ╚════════════════════════════════════════════════════════════╝              
\                                                                       └─┘
\ test results :                                                        SW2
\ ------------
\
\ Full duplex downloading (+ interpret + compile + execute) CORETEST.4TH to I2C Master target = 547ms/836kBds.
\ Full duplex downloading (+ interpret + compile + execute) CORETEST.4TH to I2C Slave target = 1031ms/443kBds.
\ the difference (422 ms) is the effective time of the I2C Half duplex exchange.
\ [(9 bits / char) + ( 2*START + 2*addr + 1 CTRL_Char + 1 STOP / line )] = [(45773 chars * 9 bits) + (1536 lines * 30 bits)] / 0.485 = 507 kHz
\ 
\ also connected to and tested with another I2C_FastForth target with MCLK = 1MHz
\
\ The I2C_Slave address is defined as 'MYSLAVEADR' in forthMSP430FR.asm source file for the I2C_Slave target.
\ You can use any pin for SDA and SCL, preferably in the interval Px0...Px3.  
\ don't forget to add 1.0k (3.3k max) pullup resistors on wires SDA and SCL.
\
\ the LEDs TX and RX work fine, comment/uncomment as you want.
\
\ Multi Master Mode works but is not tested in the real word.
\
\
\ ================================================================================
\ REGISTERS USAGE for embedded MSP430 ASSEMBLER  
\ ================================================================================
\ don't use R2, R3,
\ R4, R5, R6, R7 must be PUSHed/POPed before/after use
\ scratch registers S,T,W,X and Y are free,
\ in interrupt routines, IP is free,
\ Apply FORTH rules for TOS, PSP, RSP registers.
\
\ PUSHM order : PSP,TOS, IP, S , T , W , X , Y ,rDOVAR,rDOCON,rDODOES,rDOCOL, R3, SR,RSP, PC
\ PUSHM order : R15,R14,R13,R12,R11,R10, R9, R8,  R7  ,  R6  ,  R5   ,  R4  , R3, R2, R1, R0
\
\ example : PUSHM #6,IP pushes IP,S,T,W,X,Y registers to return stack, with IP first pushed
\
\ POPM  order :  PC,RSP, SR, R3, rDOCOL,rDODOES,rDOCON,rDOVAR,  Y,  X,  W,  T,  S, IP,TOS,PSP
\ POPM  order :  R0, R1, R2, R3,   R4  ,  R5   ,  R6  ,  R7  , R8, R9,R10,R11,R12,R13,R14,R15
\
\ example : POPM #6,IP   pop Y,X,W,T,S,IP registers from return stack, with IP last poped
\
\ ASSEMBLER conditionnal usage before IF UNTIL WHILE : S< S>= U< U>= 0= 0<> 0>=
\ ASSEMBLER conditionnal usage before          ?GOTO : S< S>= U< U>= 0= 0<> 0< 
\ ================================================================================

    [UNDEFINED] @ [IF]
\ https://forth-standard.org/standard/core/Fetch
\ @       a-addr -- x   fetch cell from memory
    CODE @
    MOV @TOS,TOS
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] - [IF]
\ https://forth-standard.org/standard/core/Minus
\ -      n1/u1 n2/u2 -- n3/u3     n3 = n1-n2
    CODE -
    SUB @PSP+,TOS   \ 2  -- n2-n1 ( = -n3)
    XOR #-1,TOS     \ 1
    ADD #1,TOS      \ 1  -- n3 = -(n2-n1) = n1-n2
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] AND [IF]
\ https://forth-standard.org/standard/core/AND
\ C AND    x1 x2 -- x3           logical AND
    CODE AND
    AND @PSP+,TOS
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] XOR [IF]
\ https://forth-standard.org/standard/core/XOR
\ C XOR    x1 x2 -- x3           logical XOR
    CODE XOR
    XOR @PSP+,TOS
    MOV @IP+,PC
    ENDCODE
    [THEN]

    : ?DOWNLOAD_ABORT
    'CR' EMIT   \ return to column 1
    VERSION @ 402 -
    ABORT" FastForth V4.2 please!"
    KERNEL_ADDON @
    %1000_0000_0000 AND   \ |XON-XOFF| |... <-- tested
    %1000_0000_0000 XOR   \ |        | |... <-- requested
    ABORT" <-- Ouch! I2C TERMINAL target..."
    RST_RET
    ;

    ?DOWNLOAD_ABORT

\ assembly names are defined in ..\inc\MSP430FRxxxx.pat
\
    MARKER _UART2I2CM_ \  asm_addr      symbolic name   content
    U2I_ENV_LEN ALLOT  \ ----------     -------------   -------
\                       _UART2I2CM_     PREV_STOP_U2I   )
\                       _UART2I2CM_+2   PREV_ABORT_U2I  )
\                       _UART2I2CM_+4   PREV_SOFT_U2I   )
\                       _UART2I2CM_+6   PREV_HARD_U2I   > previous values saved by START_U2I and restored by HARD_U2I
\                       _UART2I2CM_+8   PREV_BACK_U2I   )
\                       _UART2I2CM_+$0A PREV_TICK_VEC   )
\                       _UART2I2CM_+$0C PREV_TERM_VEC   )
\                       _UART2I2CM_+$0E I2C_SLV_ADR     <-- I2C_Slave_Addr<<1
\                       _UART2I2CM_+$10 COLLISION_DLY   <-- 20 us resolution delay after I2C collision
\                       _UART2I2CM_+$11 DUPLEX_MODE     <-- = 4 --> NOECHO, <> 4 --> ECHO
\
\
\ =========================================================================
\    HDNCODE LEDS MOV @IP+,PC ENDCODE \ comment/uncomment this line to remove/add LEDS option
\ =========================================================================
\
\ see symbolic values in ..\inc\launchpad.pat or/and in ..\inc\device.pat
\ note: HDNCODE definitions are HiDdeN and cannot be called from TERMINAL


\ =========================================================================
\ REGISTERS RESERVED USE
\ =========================================================================
\ S = PAD_ORG
\ T = 'CR'|'LF'|KEY_input
\ W = count of bits
\ X = byte
\ Y = NOECHO if Y = 4, else ECHO, Y = 2 --> Nack_on_addr, Y = 1 --> collision detection

\   --------------------------------\
    HDNCODE INIT_IOs                \ adds I/O's card init to default HARD_APP, set by any START_APP
\   --------------------------------\
\   init I2C_MASTER I/O             \
\   --------------------------------\
    BIC.B #M_BUS,&I2CM_REN          \ remove internal pull up resistors before next instruction which changes them to pull down resistors
    BIC.B #M_BUS,&I2CM_OUT          \ preset SDA + SCL output LOW
    [DEFINED] LEDS [IF]             \
    BIS.B #LED1,&LED1_DIR           \ set red led (I2C TX) pin as output
    BIS.B #LED2,&LED2_DIR           \ set green led (I2C RX) pin as output
    [THEN]                          \
\   --------------------------------\
    MOV &DEEP_HARD,PC               \ then RET to caller
    ENDCODE                         \
\   --------------------------------\

\   ================================\
    HDNCODE SOFT_U2I                \ new SOFT_APP subroutine,
\   ================================\
    CMP.B #SVSH_EVENT,TOS           \ SYSRSTIV = SVHSIFG
    0= ?GOTO FW1                    \
    CMP #8,TOS                      \
    U>= IF                          \   if human request (-1|-n) or RSTSYSIV event other than RST|NMI|POWER_ON
\       ----------------------------\
\       restore previous environment\       that restores previous hardware environment (FORTH interpreter)
\       ----------------------------\
        MOV #_UART2I2CM_,X          \       X = org = U2I_ENV >BODY
        MOV #RST_ORG,W              \       W = dst = first param to be restored
        MOV #5,T                    \       words count = 5 (STOP_APP ABORT_APP SOFT_APP HARD_APP BACK_APP)
        CALL #MVT_WRD_X2W           \       move 5 words from @X+ to RST_ORG(W)+
        MOV @X+,&TICK_VEC           \       restore previous (default) TICK_VEC value
        MOV @X+,&TERM_VEC           \       restore previous (default) TERM_VEC value
        MOV @RSP+,PC                \       --> WARM --> ... --> INTERPRET --> ... --> BACKGRND --> LPM0.
    THEN                            \
\   --------------------------------\
FW1 MOV.B &I2C_SLV_ADR,&PAD_ORG-2   \ I2C ROM address to I2C RAM address
    MOV @RSP+,PC                    \
    ENDCODE                         \ ABORT + QUIT initialize PSP and RSP stacks and set TOS=0
\   --------------------------------\

\   ================================\
    HDNCODE HARD_U2I                \ new HARD_APP subroutine,
\   ================================\ here, after PUC, all I/O's are those defined in FASTFORTH core 
    CALL &PRV_HARD_U2I              \ execute previous HARD_APP to init TERM_UC, activates I/O.
\   --------------------------------\ TOS = USERSYS{-1}|SYSRSTIV{2 4 6 $0E $xx} as POWER_ON RST NMI SVSH_threshold SYS_failures 
\   init 500MS_INT                  \ used to scan human request and also to slow down reSTART RX loop
\   --------------------------------\
    MOV #$0194,&TICK_CTL            \ start TICK_timer, Txy_CLK=ACLK=32768/4=8192Hz, up mode
    MOV #4096,&TICK_CCR0            \ time  0.5s
    MOV #$60,&TICK_CCTL2            \ output mode = set/reset
    MOV #4096,&TICK_CCR2            \ one cycle pulse to set TICK_IN IFG
    BIS.B #TICK_OUT,&TICK_OUT_DIR   \ TICK_OUT as output
    BIS.B #TICK_OUT,&TICK_OUT_SEL   \ TICK_OUT as Txy.z output
    BIS.B #TICK_IN,&TICK_IN_IE      \ enable TICK_IN interrupt
\   --------------------------------\
    MOV #ABORT,PC                   \ (bypass WARM) --> ABORT --> QUIT --> ACCEPT --> LPM0
    ENDCODE                         \ ABORT + QUIT initialize PSP and RSP stacks and set TOS=0
\   --------------------------------\

\   ================================\
    HDNCODE BACK_U2I                \ new BACK_APP subroutine, Y = 0 --> ECHO
\   ================================\
    ADD #2,RSP                      \ because no return to BACKGROUND.
\   --------------------------------\
    BEGIN                           \
        BIC.B #TICK_IN,&TICK_IN_IFG \ clear TICK_IN IFG, cleared on each ACCEPT request from I2C_Slave...
\       ----------------------------\
\       event messages              \
\       ----------------------------\
        CMP.B #I2CMM_COLLISION,TOS  \ from I2CMM_START
        0= IF                       \
\           ----------------------------\
\           I2C multi master collision  \
\           ----------------------------\
\            collision resolution        \
\            ----------------------------\
\            BEGIN                       \    
\                BIT.B #M_SCL,&I2CM_IN   \ 3 h           wait while SCL is streched low, if any
\            0<> UNTIL                   \ 2
\            BIC.B #M_BUS,&I2CM_IES      \               set M_BUS IFG for low_to_high transition
\            BEGIN                       \
\                BIC.B #M_BUS,&I2CM_IFG  \ 4             clear M_BUS IFG
\                MOV.B &COLLISION_DLY,Y  \ 3             load delay value 
\                BEGIN                   \               wait for n * I2C_Address cycles
\                    NOP                 \ 1
\                    SUB #1,Y            \ 1
\                0= UNTIL                \ 2               4~ loop
\                BIT.B #M_BUS,&I2CM_IFG  \ 4
\            0= UNTIL                    \ 2             out of loop if I2C_BUS is idle during delay
\            ----------------------------\
            MOV.B #'c',&TERM_TXBUF  \ to see 'c'ollision detection.
        THEN                        \
        CMP.B #I2CMM_NACK_ADR,TOS   \ from TICK_INT(I2CMM_START)
        0= IF                       \
            MOV.B #'.',&TERM_TXBUF  \ to see Nack_On_Addr.
        THEN                        \
\       ----------------------------\
        MOV #0,TOS                  \       clear REG events
        MOV #PAD_ORG,S              \
        MOV #'CR',T                 \ 2     T = 'CR' = penultimate char of line to be RXed by UART
        MOV.B &DUPLEX_MODE,Y        \ 3     Y U>= 4 ==> NOECHO else ECHO, for TERM_INT and TICK_INT use
        CALL #UART_RXON             \       enables TERMINAL to RX; use no registers
        BIS #LPM0+GIE,SR            \ 2     enter in LPM0 mode with GIE=1
\       ----------------------------\
\       all interrupts return here  \
\       ----------------------------\
\       user request for QUIT test  \
\       ----------------------------\
        BIT #8,&TERM_STATW          \ and clears it
    0= WHILE                        \ out of loop if BREAK sent by TERATERM(Alt+B)|I2C_Slave(Ctrl_char 3)
        BIT.B #SW2,&SW2_IN          \ SW2 pressed ?                               v
    0= UNTIL                        \ also out of loop if SW2 pressed             |
    THEN                            \ <-------------------------------------------+
    MOV.B #BREAK_EVENT,TOS          \ to do BOR
    MOV #SYS,PC                     \
    ENDCODE                         \
\   --------------------------------\

\   ----------------------------------------\
    HDNCODE I2CMM_START                     \       I2C_Master START and TX Address, version with collision detection & resolution
\   ----------------------------------------\     
\                                           \       out: W X are lost
\                                           \       if Z=0, TOS = I2CM_START EVENT
\   ----------------------------------------\     _
    BIS.B #M_SDA,&I2CM_DIR                  \ 4 h  v_   force SDA as output (low)
    MOV.B &I2CMM_ADR,X                      \ 3 h _     X = (Slave_Address<<1 + R/w bit)
    BIS.B #M_SCL,&I2CM_DIR                  \ 4 h  v_   force SCL as output (low)
\   ----------------------------------------\
\   I2C_Master Send I2C Addr                \
\   ----------------------------------------\
    MOV.B #8,W                              \ 1 l       count for 7 bits address + R/w bit
    BEGIN                                   \
        ADD.B X,X                           \ 1 l       shift one left
        U< IF                               \ 2 l       carry set ?
            BIS.B #M_SDA,&I2CM_DIR          \ 4 l   _   no  SDA as output ==> SDA low
            BIC.B #M_SCL,&I2CM_DIR          \ 4 l _^        release SCL (high)
            NOP3                            \ 3 h           for symmetry.
        ELSE                                \ 2 h       yes we can detect collision only when SDA is driven high 
            BIC.B #M_SDA,&I2CM_DIR          \ 4 l   _       SDA as input  ==> SDA high because pull up resistor
            BIC.B #M_SCL,&I2CM_DIR          \ 4 l _^        release SCL (high)
            BIT.B #M_SDA,&I2CM_IN           \ 3 h           get SDA input
            0= IF                           \ 2 h
\               ----------------------------\
\               collision detected          \               if SDA input is low, collision detected
\               ----------------------------\
               	MOV.B #I2CMM_COLLISION,TOS  \               set collision event
                BIC #Z,SR                   \               force Z=0 = bad issue
                MOV @RSP+,PC                \               ret to caller
            THEN                            \
        THEN                                \     _
        BIS.B #M_SCL,&I2CM_DIR              \ 4 h  v_   SCL as output : force SCL low
        SUB #1,W                            \ 1 l       bits count-1
    0= UNTIL                                \ 2 l       22 * 8 cycles
\   ----------------------------------------\
\   I2C_Master get Slave Ack/Nack on address\
\   ----------------------------------------\       _
    BIC.B #M_SDA,&I2CM_DIR                  \ 4 l _^_   after TX address we must release SDA to read Ack/Nack from Slave
    BIC.B #M_SCL,&I2CM_DIR                  \ 4 l _^    release SCL (high)
    BEGIN                                   \           we must wait I2C_Slave software
        BIT.B #M_SCL,&I2CM_IN               \ 3 h       by testing SCL release
    0<> UNTIL                               \ 2 h       because Slave can strech SCL low (may be occupied)
    BIT.B #M_SDA,&I2CM_IN                   \ 3 h _     get SDA state: flag Z = 0 if Nack
    BIS.B #M_SCL,&I2CM_DIR                  \ 4 h  v_   SCL as output : force SCL low
\   ----------------------------------------\
    0<> IF                                  \ 2 l
        MOV.B #I2CMM_NACK_ADR,TOS           \           if Nack on address
    THEN                                    \
    MOV @RSP+,PC                            \ 4 l       !!! here, slave have released SDA !!!
    ENDCODE                                 \           START done in 12 + (22*8) + 22 = 210 ~ (8,75us @ 24MHz)
\   ----------------------------------------\

\   --------------------------------\
    HDNCODE I2CMM_STOP              \           sends a STOP on I2C_BUS, clear I2CMM_ADR READ bit, SR(Z) is unchanged
\   --------------------------------\           here, SCL is low, SDA is high
BW2 
\   --------------------------------\           here, SCL is low, SDA is high
\ if you comment the next two lines, I2CMM_STOP performs a reSTART, I2C_BUS remains busy until another (re)START occurs.
\ Thus, I2C bus is never idle, what which may be interesting in some cases.
\   --------------------------------\     _     here, SCL is low, SDA is high
    BIS.B #M_SDA,&I2CM_DIR          \ 4 l  v_   SDA as output ==> SDA low
    NOP3                            \ 3 l    
\   --------------------------------\       _   here, SCL is low, SDA is undefined
    BIC.B #M_SCL,&I2CM_DIR          \ 4 l _^    release SCL (high)
    BIC.B #BIT0,&I2CMM_ADR          \ 4     _   clear READ bit if any (restore I2C_Address)
    BIC.B #M_SDA,&I2CM_DIR          \ 4 h _^    relase SDA (high) when SCL is high = STOP
    MOV @RSP+,PC                    \ 4         here, I2C bus is free
    ENDCODE                         \
\   --------------------------------\

\   ****************************************\
    HDNCODE TERM_INT                        \ UART RX interrupt starts on first char of each line sent by TERMINAL
\   ****************************************\ 
    ADD #2,RSP                              \ 1 remove unused SR_RET
\   ----------------------------------------\
\   get one line from UART TERMINAL to PAD  \ S = I2C_BUF_ORG, T = 'CR', W = char, Y = ECHO/NOECHO flag (see U2I_BACKGRND)
\   ----------------------------------------\
    MOV S,IP                                \
    BEGIN                                   \ input buffer begins at PAD-2, able to receive CIB_LEN = 84 chars, plus CR+LF !!!
        MOV.B &TERM_RXBUF,W                 \ 3
        CMP.B T,W                           \ 1 char = CR ? (if yes goto next REPEAT)
    0<> WHILE                               \ 2 while <> CR
        CMP #PAD_END,IP                     \ 2
        U< IF                               \ 2 discard chars out of (PAD-2 + CIB_LEN) bound
            MOV.B W,0(IP)                   \ 3 write char to input buffer, PAD-2 first
            ADD #1,IP                       \ 1 post incr
        THEN                                \
        CMP.B #4,Y                          \ 1 echo OFF ?
        0<> IF                              \ 2 if echo is ON
            BEGIN                           \
                BIT #TX,&TERM_IFG           \ TX buf empty ?
            0<> UNTIL                       \ 
            MOV.B W,&TERM_TXBUF             \ 3 return all characters to UART_TERMINAL except CR+LF which will be sent later by I2C_SLAVE
        THEN                                \
        BEGIN                               \ 
            BIT #RX,&TERM_IFG               \ 3 wait for next char received
        0<> UNTIL                           \ 2 
    REPEAT                                  \ 2 27 cycles loop ==> UART up to 8.8 Mbds @ 24MHz
    CALL #UART_RXOFF                        \ stops UART RX still char CR is received, the LF char is being transmitted.
    MOV.B W,0(IP)                           \ move CR in buffer
    ADD #1,IP                               \ 1 post incr
    BEGIN                                   \
        BIT #RX,&TERM_IFG                   \ char LF received ?
    0<> UNTIL                               \ 
\   ----------------------------------------\
BW1 \                                       \ <=== KEY input from TERMINAL, via I2C_MASTER
\   ----------------------------------------\
    MOV.B &TERM_RXBUF,T                     \           T = last char RXed by UART (LF|KEY_input), used by I2C_MASTER_TX as last byte to be TXed.         
    MOV.B T,0(IP)                           \
\   ========================================\ here I2C_Slave is sleeping in its ACCEPT routine
\   I2C_MASTER TX datas from PAD            \ now we transmit UART RX buffer (PAD) to I2C_Slave, S = LF|KEY = last char to transmit
\   ========================================\          
    CALL #I2CMM_START                       \ 4 l       flag Z = 0 if bad issue
    0<> ?GOTO FW2                           \           Nack_on_address or Master Collision,
\   ========================================\           let's see if slave want to talk ──┐
\   I2C MASTER TX datas                     \                                             |
\   ========================================\                                             |
    [DEFINED] LEDS [IF]                     \                                             |
    BIS.B #LED1,&LED1_OUT                   \           red led ON = I2C TX               |
    [THEN]                                  \                                             |
\   ----------------------------------------\                                             |
    MOV S,IP                                \ 1                                           |
    BEGIN                                   \                                             |
        MOV.B @IP,X                         \ 2 l       get first TX char                 |
\       ------------------------------------\                                             v
\       I2C_Master TX 8 bits of Data        \
\       ------------------------------------\
        MOV.B #8,W                          \ 1 l       count for 8 data bits
        BEGIN                               \
            ADD.B X,X                       \ 1 l       shift one left
            U>= IF                          \ 2 l       carry set ?
                BIC.B #M_SDA,&I2CM_DIR      \ 4 l       yes : SDA as input  ==> SDA high because pull up resistor
            ELSE                            \ 2 l
                BIS.B #M_SDA,&I2CM_DIR      \ 4 l       no: SDA as output ==> SDA low
                NOP2                        \ 2 l           for symmetry  
            THEN                            \   l   _
            BIC.B #M_SCL,&I2CM_DIR          \ 4 l _^    release SCL (high)
            CMP.B #1,W                      \ 1 h       here, eUSCI_Bx I2C_Slave streches SCL low until its RX_BUF is read,
            0= IF                           \ 2 h       that is not documented in any MSP430FRxxx family user's guide...
                BEGIN                       \     
                    BIT.B #M_SCL,&I2CM_IN   \ 3 h
                0<> UNTIL                   \ 2 h
            THEN                            \
\           --------------------------------\     _
            BIS.B #M_SCL,&I2CM_DIR          \ 4 h  v_   SCL as output : force SCL low
            SUB #1,W                        \ 1         bits count-1
        0= UNTIL                            \ 2  
\       ------------------------------------\
\       I2C_Master_TX get Slave Ack/Nack    \
\       ------------------------------------\
        BIC.B #M_SDA,&I2CM_DIR              \ 4     _   after TX byte we must release SDA to read Ack/Nack from Slave
        BIC.B #M_SCL,&I2CM_DIR              \ 4 l _^  h release SCL (high)
\        NOP3                                \           here, I2C_Slave doesn't strech SCL low, as suggested in TI's documentation...
        BIT.B #M_SDA,&I2CM_IN               \ 3 h _     get SDA state
        BIS.B #M_SCL,&I2CM_DIR              \ 4 h  v_ l SCL as output : force SCL low, to keep I2C_BUS until next START (RX|TX)
\   ----------------------------------------\
    0= WHILE \ Ack received from Slave      \ 2         out of loop if Nack on data (goto next THEN)
\   ----------------------------------------\
\   I2C_Master TX Data Loop                 \
\   ----------------------------------------\
        CMP.B @IP+,T                        \ 2         last char I2C TXed = last char UART RXed (LF|KEY) ?
\   ----------------------------------------\
    0= UNTIL  \ TXed char = last char       \ 2         loop back if <> 0
\   ----------------------------------------\
    THEN                                    \           <-- WHILE1 case of I2C_Slave Nack on Master_TX data
\   ========================================\
\   END OF I2C MASTER TX datas              \           !!! here, slave have released SDA !!!
\   ========================================\
    [DEFINED] LEDS [IF]                     \
    BIC.B #LED1,&LED1_OUT                   \    red led OFF = endof I2C TX
    [THEN]                                  \
\   ----------------------------------------\
    GOTO FW1                                \  l SCL is kept low  >─────┐
    ENDCODE                                 \                           |
\   ****************************************\                           |
\                                                                       |
\   wakes up every 1/2s to listen I2C Slave.                            |
\   ****************************************\                           |                 |
    HDNCODE TICK_INT                        \                           |                 |
\   ****************************************\                           |                 |
    ADD #2,RSP                              \ 1 remove unused SR_RET    |                 |
\   ----------------------------------------\                           |                 |    
FW1 \ single use forward label              \ <──────── does reSTART <──┘                 |
FW2 \ single use forward label              \ <──────── does reSTART <── TX Nack_on_Adr <─┘
\   ========================================\
\   I2C_MASTER RX                           \
\   ========================================\
    BIS.B #1,&I2CMM_ADR                     \ 3 l       (re)Start in RX mode
\   ----------------------------------------\
    BEGIN                                   \   l
\       ------------------------------------\
\       I2C MASTER RX (re)START             \
\       ------------------------------------\       _
        BIC.B #M_SCL,&I2CM_DIR              \ 4 l _^  h release SCL to enable (re)START RX
        CALL #I2CMM_START                   \   l       flag Z = 0 if Nack_On_Address
\       ------------------------------------\
        0<> IF                              \ 2 l       if collision/NackOnAdr event
            CMP.B #I2CMM_NACK_ADR,TOS       \
            0= ?GOTO BW2                    \           if NackOnAdr goto STOP then RET
            MOV @RSP+,PC                    \           if collision RET without STOP
        THEN                                \
\       ====================================\
\       I2C Master RX data                  \
\       ====================================\
        [DEFINED] LEDS [IF]                 \
        BIS.B #LED2,&LED2_OUT               \           green led ON = I2C RX
        [THEN]                              \
\       ------------------------------------\
        BEGIN                               \
            BEGIN                           \
                BIC.B #M_SDA,&I2CM_DIR      \ 4         after Ack and before RX next byte, we must release SDA
                MOV.B #8,W                  \ 2         count for 8 data bits
                BEGIN                       \       _      
                  BIC.B #M_SCL,&I2CM_DIR    \ 4 l _^  h release SCL (high)
                  BIT.B #M_SDA,&I2CM_IN     \ 3 h _     get SDA
                  BIS.B #M_SCL,&I2CM_DIR    \ 4 h  v_ l SCL as output : force SCL low   13~
                  ADDC.B X,X                \ 1         C <--- X(7) ... X(0) <--- SDA
                  SUB #1,W                  \ 1         count down of bits
                0= UNTIL                    \ 2  
\               ----------------------------\
\               case of RX data $FF         \           case of I2C_Slave BOR, ABORT request, I2C_BUS lost
\               ----------------------------\
                CMP.B #-1,X                 \ 1         received char $FF ? let's consider that the slave is lost...
            0<> WHILE                       \ 2  
\               ----------------------------\
                CMP.B #8,X                  \ 1         $08 = char BS
            U>= WHILE                       \ 2         ASCII char received, from char 'BS' up to char $FE.
\               ----------------------------\
\               I2C_Master_RX Send Ack      \           on char {$08...$FE}
\               ----------------------------\ 
                BIS.B #M_SDA,&I2CM_DIR      \ 4     _   set SDA low to do Ack
                BIC.B #M_SCL,&I2CM_DIR      \ 4 l _^  h release SCL (high)
                BEGIN                       \           we must wait I2C_Slave software (data processing)
                    BIT.B #M_SCL,&I2CM_IN   \ 3 h       by testing SCL released,
                0<> UNTIL                   \ 2 h _
                BIS.B #M_SCL,&I2CM_DIR      \ 4 h  v_ l SCL as output : force SCL low
\               ----------------------------\
\               I2C_Master echo to TERMINAL \
\               ----------------------------\
                CMP.B #4,Y                  \ 1         $04 = NOECHO request, others = ECHO request
                0<> IF                      \ 2
                    BEGIN                   \
                        BIT #TX,&TERM_IFG   \ 3         UART TX buffer empty ?
                    0<> UNTIL               \ 2         loop if no
                    MOV.B X,&TERM_TXBUF     \ 3         send RXed char to UART TERMINAL
                THEN                        \
            REPEAT                          \           loop back to RX next char{$08...$FE}
\               ----------------------------\
\               case of CTRL_Char {$00..$07}\           here Master holds SCL low
\               ----------------------------\           see forthMSP430FR_TERM_I2C.asm
                CMP.B #4,X                  \ 1         
                U>= IF                      \ 2
                   MOV.B X,Y                \           NOECHO = $04, ECHO = $05, set Y for actual line input
                   MOV.B X,&DUPLEX_MODE     \           save ECHO|NOECHO flag
                   BIS.B #M_SDA,&I2CM_DIR   \ 3         prepare SDA low = Ack for Ctrl_Chars {$04...$07}
                THEN                        \
            THEN                            \           false branch of CMP.B #-1,X 0<> WHILE 
\           --------------------------------\
\           case of CTRL_Char {$FF,$00..$07}\
\           --------------------------------\
\           Master_RX send Ack/Nack on data \           Ack for {$04...$07}, Nack for {$FF...$03}
\           --------------------------------\       _
            BIC.B #M_SCL,&I2CM_DIR          \ 3 l _^  h release SCL (high)
            BEGIN                           \           we must wait I2C_Slave software (data processing)
                BIT.B #M_SCL,&I2CM_IN       \ 3         by testing SCL released
            0<> UNTIL                       \ 2         (because Slave may strech SCL low)
            BIT.B #M_SDA,&I2CM_IN           \ 3   _     get SDA as Master Ack/Nack state
            BIS.B #M_SCL,&I2CM_DIR          \ 3 h  v_ l SCL as output : force SCL low
\           --------------------------------\    
        0<> UNTIL                           \ 2 l       if Ack, loop back to Master_RX data after {$04...$07}
\       ------------------------------------\   
\       Nack is sent by Master              \           case of CTRL-Chars {$FF...$03}, SDA is high, SCL is low 
\       ------------------------------------\           !!! here, slave have released SDA !!!
        CMP.B #4,X                          \
    U>= WHILE                               \           out of loop for CTRL_chars {$00,$01,$02,$03}
\       ------------------------------------\   
\       Nack on CTRL_Char $FF               \           only CTRL_Char $FF remains
\       ------------------------------------\
        CALL #SKIP_DOWNLOAD                 \           resume UART downloading source file, if any; Y=0.
    REPEAT                                  \           loop back to reSTART RX and process WARM|ABORT" messages or on I2C_Slave lost.
\   ----------------------------------------\
\   I2C_Master_RX Send STOP                 \   l       remainder: CTRL_Chars {$00,$01,$02,$03}
\   ----------------------------------------\ 
    CALL #I2CMM_STOP                        \    
\   ========================================\
\   END OF I2C MASTER RX datas              \ here I2C_bus is freed, CTRL_chars {$00,$01,$02,$03} remain to be processed.
\   ========================================\
    [DEFINED] LEDS [IF]                     \
    BIC.B #LED2,&LED2_OUT                   \ green led OFF = endof I2C RX
    [THEN]                                  \
\   ========================================\
    CMP.B #1,X                              \
\   ----------------------------------------\
\   case of ctrl_char = $01                 \ I2C_Slave request for KEY input
\   ----------------------------------------\
    0= IF                                   \
        CALL #UART_RXON                     \ enables TERMINAL to TX; use no registers
        BEGIN                               \ wait for a char
            BIT #RX,&TERM_IFG               \ received char ?
        0<> UNTIL                           \ 
        CALL #UART_RXOFF                    \ stops UART RX; use no registers
        MOV S,IP                            \
        GOTO BW1                            \ goto the end of UART RX line input
    THEN                                    \
\   ----------------------------------------\   
\   case of ctrl_char = $02|$03             \ $03 = I2C_Slave request to quit U2I driver
\   ----------------------------------------\
    U>= IF                                  \
        BIS #8,&TERM_STATW                  \ 3 set UART BREAK flag
    THEN                                    \                             
\   ----------------------------------------\
\   case of ctrl_char = $00                 \ case of request by I2C_Slave ACCEPT
\   ----------------------------------------\
    MOV @RSP+,PC                            \ ret to BACK_U2I, to wait line input from UART TERMINAL
    ENDCODE                                 \ 
\   ****************************************\
\
\
\ ==============================================================================
\ Driver UART to I2C to do a bridge USB to I2C_FastForth devices
\ ==============================================================================
\
\ I2C_address<<1  mini = $10, maxi = $EE (I2C-bus specification and user manual V6)
\ type on TERMINAL "$12 START_U2I" to link teraterm TERMINAL with FastForth I2C_Slave target at address $12

\   ================================\
    CODE I2C_TERM                   \ I2C_Addr<<1 --   
\   ================================\
    MOV TOS,&I2C_SLV_ADR            \   save I2C_address<<1
\   --------------------------------\
    MOV #INIT_IOs,&HARD_APP         \   adds I/O ADDON in HARD_APP
    MOV #RST_ORG,X                  \   org = RST_ORG in FRAM
    MOV #_UART2I2CM_,W              \   dst = >BODY of U2I_ENV definition
    MOV #5,T                        \   5 RST_xxx_APP values must to be saved as PREV_xxx_APP addr
    CALL #MVT_WRD_X2W               \   MOV T words from @X+ to 0(W)+
    MOV #SOFT_U2I,&SOFT_APP         \   set SOFT_U2I as new SOFT_APP
    MOV #HARD_U2I,&HARD_APP         \   set HARD_U2I as new HARD_APP
    MOV #BACK_U2I,&BACK_APP         \   set BACK_U2I as new BACK_APP
    MOV &TICK_VEC,&PRV_TICK_VEC     \   save previous TICK_VEC value
    MOV #TICK_INT,&TICK_VEC         \   set 500MS_INT as new TICK_VEC
    MOV &TERM_VEC,&PRV_TERM_VEC     \   save previous TERM_VEC value
    MOV #TERM_INT,&TERM_VEC         \   set TERM_INT as new TERM_VEC
    MOV #80,&COLLISION_DLY          \   2 (up to 6) * I2C-Address time = (6 * 160~ / 4) in low byte, and set ECHO in high byte
\   --------------------------------\
    LO2HI                           \   ASSEMBLER switch to FORTH without saving IP
    'CR' EMIT                       \   
    ." Connected to I2C_SLAVE at @" \   
    . 'BS' EMIT                     \   display number without space after
    ." , TERATERM(Alt-B) or "       \   
    ." I2C_MASTER(SW2) to quit"     \
    'LF' EMIT                       \
    SYS                             \   to do WARM --> HARD_U2I --> ... --> ACCEPT --> BACK_U2I --> LPM0...
    ;                               \
\   --------------------------------\

RST_SET ECHO    \ RST_SET defines the new bound of program memory protected against any (positive) SYS event.

$20 I2C_TERM    \ $20 is the wanted I2C_Slave_Address<<1
