\
\ ----------------------------------------
\ FORTH_I2CS_BSL.f
\ ----------------------------------------
\
\ TARGET SELECTION ( = the name of \INC\target.pat file without the extension)
\ MSP_EXP430FR5739  MSP_EXP430FR5969    MSP_EXP430FR5994    MSP_EXP430FR6989
\ MSP_EXP430FR4133  CHIPSTICK_FR2433    MSP_EXP430FR2433    MSP_EXP430FR2355
\ LP_MSP430FR2476
\
\ ----------------------------------------
\ loads in RAM or FRAM a bootstrap to load a binary.txt program.
\ ----------------------------------------
\ use PAD area
\
\ key recognized : '@' which defines a new ORG address for compilation,
\                  'CR' and 'LF' as end of line,
\                  'q' to quit.
\
\ ----------------------------------------
\ To do
\ ----------------------------------------
\ load forthMSP430FR.asm in Scite editor from directory ./
\ uncomment and copy your target (ex. MSP_EXP430FR2355)
\ Tools --> select Assemble 1:[target] 2:[extension]
\           paste your target in 1:[target] parameter
\           execute
\ if no error:
\ Tools --> select Prog 1:[target] 2:[extension] with FORTH_UART_BSL
\             or   Prog 1:[target] 2:[extension] with FORTH_I2CS_BSL
\           execute
\ it's done !
\
\ ------------------------------------------------------------------
\ first we download the set of definitions we need (from CORE_ANS.f)
\ ------------------------------------------------------------------

\ ----------------------------------------
\ we do some tests allowing the download
\ ----------------------------------------
    : ?ABORT_DOWNLOAD
    'CR' EMIT        \ return to column 1
    KERNEL_ADDON @
    %1000_0000_0000 AND   \ |XON-XOFF| |... <-- tested
    ABORT" Ouch! UART TERMINAL target..."
    VERSION @ 402 -
    ABORT" FastForth V4.2 please!"
    [DEFINED] NOBOOT [IF] NOBOOT [THEN] \ Perhaps SD BOOTSTRAP is active... 
    ;

    ?ABORT_DOWNLOAD \ all is ok ?

    [UNDEFINED] @ [IF]
\ https://forth-standard.org/standard/core/Fetch
\ @       a-addr -- x   fetch cell from memory
    CODE @
    MOV @TOS,TOS
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] ! [IF]
\ https://forth-standard.org/standard/core/Store
\ !        x a-addr --   store cell in memory
    CODE !
    MOV @PSP+,0(TOS)
    MOV @PSP+,TOS
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] - [IF]
\ https://forth-standard.org/standard/core/Minus
\ -      n1/u1 n2/u2 -- n3/u3     n3 = n1-n2
    CODE -
    SUB @PSP+,TOS   \ 2  -- n2-n1 ( = -n3)
    XOR #-1,TOS     \ 1
    ADD #1,TOS      \ 1  -- n3 = -(n2-n1) = n1-n2
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] AND [IF]
\ https://forth-standard.org/standard/core/AND
\ C AND    x1 x2 -- x3           logical AND
    CODE AND
    AND @PSP+,TOS
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] XOR
    [IF]
\ https://forth-standard.org/standard/core/XOR
\ C XOR    x1 x2 -- x3           logical XOR
    CODE XOR
    XOR @PSP+,TOS
    MOV @IP+,PC
    ENDCODE
    [THEN]

    [UNDEFINED] EXECUTE [IF]
\ https://forth-standard.org/standard/core/EXECUTE
\ EXECUTE   i*x xt -- j*x   execute Forth word at 'xt'
    CODE EXECUTE
    MOV #EXECUTE,PC
    ENDCODE
    [THEN]

\ ----------------------------------------
\ select where we install bootstrap
\ ----------------------------------------
    RAM_USR_PTR @ DP !                  \ compile at the end of RAM_USR section
\    $FE80 DP !                          \ compile at the end of MAIN section

\   ------------------------------------\
    CODENNM                             \   is the bootstrap
\   ------------------------------------\
BW1 MOV #PAD_ORG,X                      \   X = PAD_PTR
\   ------------------------------------\
    MOV.B #$00,&I2CS_TXBUF              \   send Ctrl_Char ACCEPT new line
    BEGIN                               \
        BIT #4,&I2CS_IFG                \       IFG(2) = WAKE_UP interrupt
    0<> UNTIL                           \   START interrupt flag is set
    BIC #4,&I2CS_IFG                    \   clear it
    BEGIN                               \   accept new line
\       --------------------------------\
        BEGIN                           \
            BIT #RX,&I2CS_IFG           \       wait RX buffer full
        0<> UNTIL                       \   
        MOV.B &I2CS_RXBUF,Y             \       read character into Y
        MOV.B Y,0(X)                    \           store char in PAD_PTR
        ADD #1,X                        \           incr PAD_PTR
        CMP.B #'LF',Y                   \       
    0= UNTIL                            \   end of line
\   ------------------------------------\
    BEGIN                               \
        BIT #$10,&I2CS_CTLW0            \       test UCTR
    0<> UNTIL                           \   wait until Master reSTART in RX mode after sending 'LF'
\   ------------------------------------\
    MOV #PAD_ORG,X                      \   then interpret new line in PAD :
\   ------------------------------------\
    CMP.B #'q',0(X)                     \   end of file ?
    0<> IF                              \
\       --------------------------------\   if not 
        BEGIN                           \       line interpret loop
\           ----------------------------\
BW2         MOV.B @X+,Y                 \
            CMP.B #'LF',Y               \  
            0= IF                       \
                MOV.B Y,&I2CS_TXBUF     \           echoes 'LF' char
                BEGIN                   \
                    BIT #TX,&I2CS_IFG   \ 
                0<> UNTIL               \           wait TX buffer empty
                GOTO BW1                \           then go to accept new line (out of loop)
            THEN                        \
            CMP.B #'CR',Y               \           (case of empty lines before address or data line)
            0= ?GOTO BW2                \       if char = 'CR'
            CMP.B #'@',Y                \       '@' = compilation address ?
            0= IF                       \       if yes, go to process 4 chars:
                BIS #UF3,SR             \           set Address flag
                MOV #4,S                \           S = count of chars 
                MOV.B @X+,Y             \           skip char '@'
            THEN                        \
            CMP.B #':',Y                \       ':' = char after char '9'
            U>= IF                      \   
                SUB.B #7,Y              \           for each char U>= '9'
            THEN                        \   
            SUB.B #$30,Y                \       Y = Hex digit
            RLAM #4,W                   \       W = %xxxx_xxxx_xxxx_0000
            BIS Y,W                     \       W = %xxxx_xxxx_xxxx_YYYY
            SUB.B #1,S                  \       dec count
            0= IF                       \       if count = 0
                BIT #UF3,SR             \   
                0<> IF                  \           if Address flag is set
                    BIC #UF3,SR         \               clear Address flag
                    MOV W,T             \               T = compilation address
                ELSE                    \           else if Hex byte
                    MOV.B W,0(T)        \               store byte
                    ADD #1,T            \               post increment address
                THEN                    \   
                MOV #2,S                \           after each (address|byte), set count for next byte.
                ADD #1,X                \           skip next char('BL'|'CR')
            THEN                        \   
\           ----------------------------\   
        AGAIN                           \       loop back
\       --------------------------------\
    THEN                                \   if yes
    MOV #$A504,&PMMCTL0                 \   software BOR, that send I2C_Ctrl_Char ABORT
\   ------------------------------------\
    ENDCODE EXECUTE                     \ leave at least one line for cosmetic...   85 words 


