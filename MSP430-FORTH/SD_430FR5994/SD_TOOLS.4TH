

; ---------------------------------------------------------------
; SD_TOOLS.4th for MSP_EXP430FR5994
; BASIC TOOLS for SD Card : DIR FAT SECTOR. CLUSTER.
; ---------------------------------------------------------------

    CODE ?ABORT_SD_TOOLS
    SUB @R2,R15
    MOV R14,2(R15)
    [UNDEFINED] LOAD"
    [IF]
        MOV @R3+,0(R15)
    [ELSE]
        MOV R3,0(R15)
    [THEN]
    MOV &$1808,R14
    SUB #402,R14
    COLON
    $0D EMIT
    ABORT" FastForth V4.2 please!"
    ABORT" Build FastForth with SD_CARD_LOADER addon!"
    RST_RET
    ;

    ?ABORT_SD_TOOLS

    MARKER {SD_TOOLS}

; ------------------------------------------------------------------
; first we download the set of definitions we need (from CORE_ANS.4th for MSP_EXP430FR5994)
; ------------------------------------------------------------------

    [UNDEFINED] HERE [IF]
    CODE HERE
    MOV #BEGIN,R0
    ENDCODE
    [THEN]

    [UNDEFINED] + [IF]
    CODE +
    ADD @R15+,R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] MAX
    [IF]
    CODE MAX
    CMP @R15,R14
    S<  ?GOTO FW1
BW1 ADD @R3,R15
    MOV @R13+,R0
    ENDCODE

    CODE MIN
    CMP @R15,R14
    S<  ?GOTO BW1
FW1 MOV @R15+,R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] C@
    [IF]
    CODE C@
    MOV.B @R14,R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] SPACE
    [IF]
    : SPACE
    $20 EMIT ;
    [THEN]

    [UNDEFINED] SPACES
    [IF]
    CODE SPACES
    CMP R3,R14
    0<> IF
        PUSH R13
        BEGIN
            LO2HI
            $20 EMIT
            HI2LO
            SUB @R3,R13
            SUB 0(R3),R14
        0= UNTIL
        MOV @R1+,R13
    THEN
    MOV @R15+,R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] SWAP
    [IF]
    CODE SWAP
    MOV @R15,R10
    MOV R14,0(R15)
    MOV R10,R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] OVER
    [IF]
    CODE OVER
    MOV R14,-2(R15)
    MOV @R15,R14
    SUB @R3,R15
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] >R
    [IF]
    CODE >R
    PUSH R14
    MOV @R15+,R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] R>
    [IF]
    CODE R>
    SUB @R3,R15
    MOV R14,0(R15)
    MOV @R1+,R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] -
    [IF]
    CODE -
    SUB @R15+,R14
    XOR @R3+,R14
    ADD 0(R3),R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] DO
    [IF]
    HDNCODE XDO
    MOV #$8000,R9
    SUB @R15+,R9
    MOV R14,R8
    ADD R9,R8
    PUSHM #2,R9
    MOV @R15+,R14
    MOV @R13+,R0
    ENDCODE

    CODE DO
    SUB @R3,R15
    MOV R14,0(R15)
    ADD @R3,&$1DBE
    MOV &$1DBE,R14
    MOV #XDO,-2(R14)
    ADD @R3,&LEAVEPTR
    MOV &LEAVEPTR,R10
    MOV R3,0(R10)
    MOV @R13+,R0
    ENDCODE IMMEDIATE

    HDNCODE XLOOP
    ADD 0(R3),0(R1)
BW1 BIT #$100,R2
    0= IF
        MOV @R13,R13
        MOV @R13+,R0
    THEN
    ADD @R2,R1
    ADD @R3,R13
    MOV @R13+,R0
    ENDCODE

    CODE LOOP
    MOV #XLOOP,R9
BW2 ADD @R2,&$1DBE
    MOV &$1DBE,R10
    MOV R9,-4(R10)
    MOV R14,-2(R10)
    BEGIN
        MOV &LEAVEPTR,R14
        SUB @R3,&LEAVEPTR
        MOV @R14,R14
        CMP R3,R14
    0<> WHILE
        MOV R10,0(R14)
    REPEAT
    MOV @R15+,R14
    MOV @R13+,R0
    ENDCODE IMMEDIATE

    HDNCODE XPLOO
    ADD R14,0(R1)
    MOV @R15+,R14
    GOTO BW1
    ENDCODE

    CODE +LOOP
    MOV #XPLOO,R9
    GOTO BW2
    ENDCODE IMMEDIATE
    [THEN]

    [UNDEFINED] I
    [IF]
    CODE I
    SUB @R3,R15
    MOV R14,0(R15)
    MOV @R1,R14
    SUB 2(R1),R14
    MOV @R13+,R0
    ENDCODE
    [THEN]

    [UNDEFINED] CR
    [IF]
    CODE CR
    MOV #NEXT_ADR,R0
    ENDCODE

    :NONAME
    $0D EMIT $0A EMIT
    ; IS CR
    [THEN]

; ------------------------------------------------------------------
; then we download the set of definitions we need (from UTILITY.4th for MSP_EXP430FR5994)
; ------------------------------------------------------------------

    [UNDEFINED] U.R4
    [IF]
    : U.R4
    >R  <# 0 # #S #>
    R> OVER - 0 MAX SPACES TYPE
    ;
    [THEN]

    [UNDEFINED] DUMP
    [IF]
    CODE DUMP
    PUSH R13
    PUSH &$1DB6
    MOV #$10,&$1DB6
    ADD @R15,R14
    LO2HI
    SWAP
    CR
    4 SPACES $10 0
    DO I 3 U.R4  LOOP
    DO  CR
        I 4 U.R4
        I $10 + I
        DO I C@ 3 U.R4 LOOP
        SPACE SPACE
        I $10 + I
        DO I C@ $7E MIN $20 MAX EMIT LOOP
    $10 +LOOP
    R> $1DB6 !
    ;
    [THEN]

; --------------------------
; end of definitions we need
; --------------------------

    CODE SECTOR.
BW1 MOV     R14,R9
    MOV     @R15,R10
    CALL    #RD_SECT
    COLON
    SPACE <# #S #> TYPE
    $1E00 $200 DUMP CR ;

    CODE CLUSTER.
BW2 BIT.B   @R2,&$260
    0<> IF
        MOV &$FFFA,R0
    THEN
    MOV.B &$2012,R10
    MOV @R15,R9
    BEGIN
        RRA R10
    U< WHILE
        ADD R9,R9
        ADDC R14,R14
    REPEAT
    ADD     &$2010,R9
    MOV     R9,0(R15)
    ADDC    R3,R14
    GOTO    BW1
    ENDCODE

    CODE FAT
    SUB     @R2,R15
    MOV     R14,2(R15)
    MOV     &$2008,0(R15)
    MOV     R3,R14
    GOTO    BW1
    ENDCODE

    CODE DIR
    SUB     @R2,R15
    MOV     R14,2(R15)
    MOV     &$202C,0(R15)
    MOV     &$202E,R14
    CMP     R3,R14
    0<>     ?GOTO BW2
    CMP     0(R3),0(R15)
    0<>     ?GOTO BW2
    MOV     &$200E,0(R15)
    GOTO    BW1
    ENDCODE

    RST_SET 

    [THEN]

    ECHO
