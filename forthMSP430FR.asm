;
;-------------------------------------------------------------------------------
; Vingt fois sur le m�tier remettez votre ouvrage,
; Polissez-le sans cesse, et le repolissez,
; Ajoutez quelquefois, et souvent effacez.               Boileau, L'Art po�tique
;-------------------------------------------------------------------------------

;===============================================================================
; SCITE editor: copy https://www.scintilla.org/Sc531.exe to \prog\scite.exe
;               copy \config\SciTEUser.properties in your %HOME% directory
;===============================================================================

;===============================================================================
; MACRO ASSEMBLER AS:  unzip to \prog
; http://john.ccac.rwth-aachen.de:8000/ftp/as/precompiled/i386-unknown-win32/aswcurr.zip
;===============================================================================
    .listing purecode   ; reduce listing to true conditionnal parts
    MACEXP_DFT noif     ; reduce macros listing to true part
    .PAGE  0            ; assembly.lst file without pagination
;-------------------------------------------------------------------------------

VER .equ "V402"     ; FORTH version

;===============================================================================
; before assembling or programming you must set TARGET in scite param1 (SHIFT+F8)
; according to the selected (uncommented) TARGET below
;===============================================================================
;    TARGET        ;
;MSP_EXP430FR5739  ; compile for MSP-EXP430FR5739 launchpad
MSP_EXP430FR5969  ; compile for MSP-EXP430FR5969 launchpad
;MSP_EXP430FR5994  ; compile for MSP-EXP430FR5994 launchpad
;MSP_EXP430FR6989  ; compile for MSP-EXP430FR6989 launchpad
;MSP_EXP430FR4133  ; compile for MSP-EXP430FR4133 launchpad
;MSP_EXP430FR2355  ; compile for MSP-EXP430FR2355 launchpad
;MSP_EXP430FR2433  ; compile for MSP-EXP430FR2433 launchpad
;LP_MSP430FR2476   ; compile for LP_MSP430FR2476  launchpad
;CHIPSTICK_FR2433  ; compile for "CHIPSTICK" of M. Ken BOAK

; choose DTC model (Direct Threaded Code); if you don't know, choose 2
DTC .equ 2  ; DTC model 1 : DOCOL = CALL rDOCOL           14 cycles 1 word      shortest DTC model
            ; DTC model 2 : DOCOL = PUSH IP, CALL rEXIT   13 cycles 2 words     best compromize to mix FORTH/ASM code
            ; DTC model 3 : inlined DOCOL (and LO2HI)      9 cycles 4 words     fastest

;THREADS     .equ 1 ;  1,  2 ,  4 ,  8 ,  16,  32  search entries in word-set. @24MHz 6MBds, download CORETEST.4TH = 242 kBds eff
;THREADS     .equ 2 ;  1,  2 ,  4 ,  8 ,  16,  32  search entries in word-set. @24MHz 6MBds, download CORETEST.4TH =
;THREADS     .equ 4 ;  1,  2 ,  4 ,  8 ,  16,  32  search entries in word-set. @24MHz 6MBds, download CORETEST.4TH = 574 kBds eff
;THREADS     .equ 8 ;  1,  2 ,  4 ,  8 ,  16,  32  search entries in word-set. @24MHz 6MBds, download CORETEST.4TH =
;THREADS     .equ 16;  1,  2 ,  4 ,  8 ,  16,  32  search entries in word-set. @24MHz 6MBds, download CORETEST.4TH = 945 kBds eff
THREADS     .equ 32;;  1,  2 ,  4 ,  8 ,  16,  32  search entries in word-set. @24MHz 6MBds, download CORETEST.4TH = 1043 kBds eff
                    ; +0, +32, +44, +60, +94,+158 bytes, usefull to speed up compilation;         unwired download = 2253 kBds
                    ; the FORTH interpreter is speed up by about a square root factor of THREADS.
FREQUENCY   .equ 16 ; fully tested at 1,2,4,8,16 MHz, plus 24 MHz for MSP430FR57xx,MSP430FR2355

; ==============================================================================
;UART_TERMINAL ; COMMENT TO SWITCH FROM UART TO I2C TERMINAL
; ==============================================================================
    .IFDEF UART_TERMINAL
TERMINALBAUDRATE    .equ 4000000
TERMINAL3WIRES      ;; + 18 bytes  enable 3 wires XON/XOFF software flow control
;TERMINAL4WIRES      ; + 12 bytes  enable 4 wires XON/XOFF + RTS hardware flow control
;TERMINAL5WIRES      ; + 10 bytes  enable 5 wires XON/XOFF + RTS/CTS hardware flow control
    .ELSE
I2C_TERM_ADR .equ $20 ; I2C_TERMINAL_Slave_Address (what you want, address 7<<1 bits)
    .ENDIF

;===============================================================================
; KERNEL ADDONs that can't be added later
;===============================================================================
DOUBLE_INPUT        ;; +   60 bytes : adds the interpretation engine for double numbers (numbers with dot)
FIXPOINT_INPUT      ;; +   68 bytes : adds the interpretation engine for Q15.16 numbers (numbers with comma)
;VOCABULARY_SET      ; +  170 bytes : adds words: WORDSET FORTH hidden PREVIOUS ONLY DEFINITIONS
;SD_CARD_LOADER      ; + 1664 bytes : to load source files from SD_card
;SD_CARD_READ_WRITE  ; + 1168 bytes : to read, create, write and del files + copy text files from PC to target SD_Card
;LARGE_CODE          ; +  506 bytes : extended assembler to 20 bits addresses (1MB).
;LARGE_DATA          ; + 1212 bytes : extended assembler to 20 bits datas.
;SYS_NMI             ; +   16 bytes : to display SYSUNIV and SYSSNIV with SYSRSTIV
;PROMPT              ; +   18 bytes : to display the prompt "ok" (deprecated).
;===============================================================================

;-------------------------------------------------------------------------------
; OPTIONS that can be added later by downloading their source file >------------------------------------+
; however, added here, they are protected against Deep COLD|RESET.                                      |
;-------------------------------------------------------------------------------                        v
;CORE_COMPLEMENT     ; + 2304 bytes, if you want a conventional FORTH ANS94 compliant               CORE_ANS.f
;FIXPOINT            ; +  422/528 bytes add HOLDS F#S F. S>F F+ F- F/ F*                            FIXPOINT.f
;UTILITY             ; +  434/524 bytes (1/16threads) : add .S .RS WORDS U.R DUMP ?                 UTILITY.f
;SD_TOOLS            ; +  142 bytes for trivial DIR, FAT, CLUSTR. and SECTOR. view, (adds UTILITY)  SD_TOOLS.f
;DOUBLE              ;              DOUBLE word set                                                 DOUBLE.f

; ------------------------------------------------------------------------------
    .include "ThingsInFirst.inc" ; macros, target definitions, RAM & INFO variables...
;-------------------------------------------------------------------------------

;===============================================================================
; DTCforthMSP430FR5xxx program (FRAM) memory
;===============================================================================
    .org    MAIN_ORG

;-------------------------------------------------------------------------------
; TERMINAL INPUT / OUTPUT
;-------------------------------------------------------------------------------
    .IFNDEF UART_TERMINAL
        .include "forthMSP430FR_TERM_I2C.asm"
    .ELSE
        .IFDEF HALFDUPLEX
            .include "forthMSP430FR_TERM_HALF.asm"
        .ELSE
            .include "forthMSP430FR_TERM_UART.asm"
        .ENDIF
    .ENDIF
    .IFDEF SD_CARD_LOADER
        .include "forthMSP430FR_SD_ACCEPT.asm"
    .ENDIF

            FORTHWORD "TYPE"
;https://forth-standard.org/standard/core/TYPE
;C TYPE    adr u --     type string to terminal
TYPE        PUSH IP                 ;3
            MOV #TYPE_NEXT,IP       ;2
            MOV @PSP+,X             ;2 -- u             X = adr
TYPELOOP    SUB #2,IP               ;1                  [IP] = address of TYPE_NEXT address
            SUB #2,PSP              ;1 -- x u
            MOV TOS,0(PSP)          ;3 -- u u
            MOV.B @X+,TOS           ;2 -- u char
            JMP EMIT                ;2 + 25/12          use only Y
            .word TYPE_NEXT         ;  -- u
TYPE_NEXT   SUB.B #1,TOS            ;1 -- u-1           byte operation, according to the /COUNTED-STRING value
            JNZ TYPELOOP            ;2                  37~/24~ EMIT loop 270/416 kBds/MHz --> 6.5MBds @ 24 MHz
            JZ DROPEXIT             ;2

; ------------------------------------------------------------------------------
; COMPILING OPERATORS
; ------------------------------------------------------------------------------
; Primitive LIT; compiled by LITERAL
; LIT      -- x    fetch inline literal to stack
; This is the run-time code of LITERAL.
LIT         SUB #2,PSP          ; 1  save old TOS..
            MOV TOS,0(PSP)      ; 3  ..onto stack
            MOV @IP+,TOS        ; 2  fetch new TOS value
NEXT        MOV @IP+,PC         ; 4  NEXT

TWODUP_XSQUOTE                  ; see [ELSE]
            MOV @PSP,-4(PSP)    ; 4
            MOV TOS,-2(PSP)     ; 3
            SUB #4,PSP          ; 1
; Primitive XSQUOTE; compiled by SQUOTE
; (S")     -- addr u   run-time code to get address and length of a compiled string.
XSQUOTE     SUB #4,PSP          ; 1                 push old TOS on stack
            MOV TOS,2(PSP)      ; 3                 and reserve one cell on stack
            MOV.B @IP+,TOS      ; 2 -- ? u          u = lenght of string
            MOV IP,0(PSP)       ; 3 -- addr u       IP is odd...
            ADD TOS,IP          ; 1                 IP=addr+u=addr(end_of_string)
            BIT #1,IP           ; 1                 IP=addr+u   Carry set/clear if odd/even
            ADDC #0,IP          ; 1                 IP=addr+u aligned
            MOV @IP+,PC         ; 4  16~

; : SETIB SOURCE 2! 0 >IN ! ;
; SETIB      org len' --        set Input Buffer, shared by INTERPRET and [ELSE]
SETIB       MOV #0,&TOIN            ;3
            MOV @PSP+,&SOURCE_ORG   ;4 -- len
            MOV TOS,&SOURCE_LEN     ;3 -- len
            MOV @PSP+,TOS           ;2 --
            MOV @IP+,PC             ;4

; REFILL    accept one line to input buffer and leave org len' of the filled input buffer
; as it has no more host OS and as waiting command is done by ACCEPT, REFILL's flag is useless
; : REFILL TIB DUP CIB_LEN ACCEPT   ;   -- org len'     shared by QUIT and [ELSE]
REFILL      SUB #4,PSP              ;1
            MOV TOS,2(PSP)          ;3                  save TOS
TWODROP_REFILL                      ;                   see [ELSE]
            MOV #CIB_LEN,TOS        ;2  -- x len        Current Input Buffer LENght
            MOV #TIB_ORG,0(PSP)     ;4  -- org len
            MOV @PSP,-2(PSP)        ;4  -- org len
            SUB #2,PSP              ;1  -- org org len
            JMP ACCEPT              ;2  org org len -- org len'
CIB_ORG     .equ REFILL+12          ;                   CIB_ORG = TIB_ORG address

; Primitive QFBRAN; compiled by IF UNTIL
;Z ?FalseBranch   x --              ; branch if TOS is FALSE (TOS = 0)
QFBRAN      CMP #0,TOS              ; 1  test TOS value
            MOV @PSP+,TOS           ; 2  pop new TOS value (doesn't change flags)
ZBRAN       JNZ SKIPBRANCH          ; 2  if TOS was <> 0, skip the branch; 10 cycles
BRAN        MOV @IP,IP              ; 2  take the branch destination
            MOV @IP+,PC             ; 4  ==> branch taken, 11 cycles

XDODOES                             ; 4 for CALL rDODOES
            SUB #2,PSP              ;+1
            MOV TOS,0(PSP)          ;+3 save TOS on parameters stack
            MOV @RSP+,TOS           ;+2 TOS = PFA address of master word, i.e. address of its first cell after DOES>
            PUSH IP                 ;+3 save IP on return stack
            MOV @TOS+,IP            ;+2 IP = CFA of Master word, TOS = BODY address of created word
            MOV @IP+,PC             ;+4 = 19~ = ITC-2

XDOCON                              ; 4 for CALL rDOCON
            SUB #2,PSP              ;+1
            MOV TOS,0(PSP)          ;+3 save TOS on parameters stack
            MOV @RSP+,TOS           ;+2 TOS = PFA address of master word CONSTANT
FETCH       MOV @TOS,TOS            ;+2 TOS = CONSTANT value
            MOV @IP+,PC             ;+4 = 16~ = ITC+4

    .IF DTC = 1                     ; DOCOL = CALL rDOCOL, [rDOCOL] = XDOCOL
XDOCOL      MOV @RSP+,W             ; 2
            PUSH IP                 ; 3     save old IP on return stack
            MOV W,IP                ; 1     set new IP to PFA
            MOV @IP+,PC             ; 4     = NEXT
    .ENDIF                          ; 10 cycles

; ------------------------------------------------------------------------------
; CONDITIONNAL COMPILATION
; ------------------------------------------------------------------------------
            FORTHWORDIMM "[THEN]"   ; does nothing
; https://forth-standard.org/standard/tools/BracketTHEN
            MOV @IP+,PC

; BRanch if BAD strings COMParaison, [COMPARE ZEROEQUAL QFBRAN] replacement
QBRBADCOMP                  ; addr1 u1 addr2 u2 --
            MOV TOS,W       ;1          W = u2
            MOV @PSP+,Y     ;2          Y = addr2
            CMP @PSP+,W     ;2          u1 = u2 ?
            MOV @PSP+,X     ;2          X = addr1
            MOV @PSP+,TOS   ;2 --
            JNZ BRAN        ;2 --       if u1<>u2; 11+6 cycles
COMPLOOP    CMP.B @X+,0(Y)  ;4
            JNZ BRAN        ;2 --       if char1<>char2; branch on first char <> in 17+6 cycles
            ADD #1,Y        ;1          addr+1
            SUB #1,W        ;1          u-1
            JNZ COMPLOOP    ;2          10 cycles char comp loop
SKIPBRANCH  ADD #2,IP       ;1          if comparison success
            MOV @IP+,PC     ;4

; [TWODROP ONEMINUS ?DUP ZEROEQUAL QFBRAN next_comp EXIT] replacement
QBRNEXTCMP                  ;    -- cnt addr u
            ADD #2,PSP      ;1   -- cnt u       NIP
            MOV @PSP+,TOS   ;2   -- cnt         + DROP = TWODROP 
            SUB #1,TOS      ;3   -- cnt-1       ONEMINUS
            JNZ BRAN        ;2   -- cnt-1       branch to next comparaison if <> 0
DROPEXIT    MOV @RSP+,IP    ;2
DROP        MOV @PSP+,TOS   ;2   --
            MOV @IP+,PC     ;4
FDROPEXIT   .word   DROPEXIT

            FORTHWORDIMM  "[ELSE]"
; https://forth-standard.org/standard/tools/BracketELSE
;Compilation:
;Perform the execution semantics given below.
;Execution:
;( "<spaces>name ..." -- )
;Skipping leading spaces, parse and discard space-delimited words from the parse area,
;including nested occurrences of [IF] ... [THEN] and [IF] ... [ELSE] ... [THEN],
;until the word [THEN] has been parsed and discarded.
;If the parse area becomes exhausted, it is refilled as with REFILL.
BRACKETELSE mCOLON
            .word   LIT,1                   ;
            .word   BRAN,BRACKTELSE1        ; -- cnt=1          6~ versus 5~ for ONEPLUS
BRACKTELSE0 .word   XSQUOTE                 ; -- cnt addr 0
            .byte   5,13,"ko ",10           ;
            .word   TYPE                    ;                   send CR + "ko " then LF
            .word   TWODROP_REFILL          ; -- cnt            REFILL Input Buffer with next line
            .word   SETIB                   ;                   SET Input Buffer pointers SOURCE_LEN, SOURCE_ORG and clear >IN
BRACKTELSE1 .word   BL_WORD,COUNT           ; -- cnt addr u     Z = 0 if u <> 0
            .word   ZBRAN,BRACKTELSE0       ; -- cnt addr 0     Z = 1 --> end of line, -6~
            .word   TWODUP_XSQUOTE          ;                   24 ~
            .byte   6,"[THEN]"              ; -- cnt addr u addr1 u1 addr2 u2
            .word   QBRBADCOMP,BRACKTELSE2  ; -- cnt addr u     if [THEN] not found, jump for next comparaison
            .word   QBRNEXTCMP,BRACKTELSE1  ; -- cnt-1          if found, 2DROP,  count-1, loop back if count <> 0, else DROP EXIT
BRACKTELSE2 .word   TWODUP_XSQUOTE          ;
            .byte   6,"[ELSE]"              ; -- cnt addr u addr1 u1 addr2 u2
            .word   QBRBADCOMP,BRACKTELSE3  ; -- cnt addr u     if [ELSE] not found, jump for next comparaison
            .word   QBRNEXTCMP,BRACKTELSE4  ; -- cnt-1          if found, 2DROP, count-1, loop back if count <> 0, else DROP EXIT
BRACKTELSE3 .word   XSQUOTE                 ;                   16 ~
            .byte   4,"[IF]"                ; -- cnt addr1 u1 addr2 u2
            .word   QBRBADCOMP,BRACKTELSE1  ; -- cnt            if [IF] not found, loop back for next word comparaison
BRACKTELSE4 .word   ONEPLUS                 ; -- cnt+1          if found,  same loop back with count+1
            .word   BRAN,BRACKTELSE1        ;         

            FORTHWORDIMM "[IF]" ; flag --
; https://forth-standard.org/standard/tools/BracketIF
;Compilation:
;Perform the execution semantics given below.
;Execution: ;( flag | flag "<spaces>name ..." -- )
;If flag is true, do nothing. Otherwise, skipping leading spaces,
;   parse and discard space-delimited words from the parse area,
;   including nested occurrences of [IF] ... [THEN] and [IF] ... [ELSE] ... [THEN],
;   until either the word [ELSE] or the word [THEN] has been parsed and discarded.
;If the parse area becomes exhausted, it is refilled as with REFILL. [IF] is an immediate word.
;An ambiguous condition exists if [IF] is POSTPONEd,
;   or if the end of the input buffer is reached and cannot be refilled before the terminating [ELSE] or [THEN] is parsed.
            CMP #0,TOS      ; -- f
            MOV @PSP+,TOS   ; --
            JZ BRACKETELSE  ;       if false flag output
            MOV @IP+,PC     ;

            FORTHWORDIMM  "[UNDEFINED]"
; https://forth-standard.org/standard/tools/BracketUNDEFINED
;Compilation:
;Perform the execution semantics given below.
;Execution: ( "<spaces>name ..." -- flag )
;Skip leading space delimiters. Parse name delimited by a space.
;Return a false flag if name is the name of a word that can be found,
;otherwise return a true flag.
            mCOLON
            .word   BL_WORD,FIND
            mHI2LO
            SUB #1,TOS      ;1 borrow if TOS was 0
            SUBC TOS,TOS    ;1 TOS=-1 if borrow is set
NIP_EXIT    MOV @RSP+,IP
NIP         ADD #2,PSP      ;1
            MOV @IP+,PC     ;4

            FORTHWORDIMM  "[DEFINED]"
; https://forth-standard.org/standard/tools/BracketDEFINED
;Compilation:
;Perform the execution semantics given below.
;Execution:
;( "<spaces>name ..." -- flag )
;Skip leading space delimiters. Parse name delimited by a space.
;Return a true flag if name is the name of a word that can be found,
;otherwise return a false flag. [DEFINED] is an immediate word.
            mCOLON
            .word   BL_WORD,FIND
            .word   NIP_EXIT

;-------------------------------------------------------------------------------
; STACK OPERATIONS
;-------------------------------------------------------------------------------
; https://forth-standard.org/standard/core/SWAP
SWAP        PUSH @PSP+      ; 3

; VARIABLE run time called by CALL rDOVAR
XDOVAR
; https://forth-standard.org/standard/core/Rfrom
; R>    -- x    R: x --   pop from return stack
RFROM       SUB #2,PSP      ; 1
            MOV TOS,0(PSP)  ; 3
            MOV @RSP+,TOS   ; 2
            MOV @IP+,PC     ; 4

; https://forth-standard.org/standard/core/DUP
; DUP      x -- x x      duplicate top of stack
DUP         SUB #2,PSP      ; 1
            MOV TOS,0(PSP)  ; 3
            MOV @IP+,PC     ; 4

;-------------------------------------------------------------------------------
; ARITHMETIC OPERATIONS
;-------------------------------------------------------------------------------
; https://forth-standard.org/standard/core/Minus
; -      n1/u1 n2/u2 -- n3/u3      n3 = n1-n2
MINUS       SUB @PSP+,TOS   ;2  -- n2-n1
NEGATE      XOR #-1,TOS     ;1
ONEPLUS     ADD #1,TOS      ;1  -- n3 = -(n2-n1) = n1-n2
            MOV @IP+,PC

; ------------------------------------------------------------------------------
; STRINGS PROCESSING
; ------------------------------------------------------------------------------
            FORTHWORDIMM "S\34" ; immediate
; use SQUOTE+10 if you want to define another separator
; https://forth-standard.org/standard/core/Sq
; S"       --                 compile in-line string
SQUOTE      SUB #2,PSP              ;               first choose separator
            MOV TOS,0(PSP)          ;
            MOV #'"',TOS            ;               separator = '"'
            MOV #0,T                ;               T = volatile CAPS OFF, paired with WORDD+4
            mCOLON                  ;
            .word LIT,XSQUOTE,COMMA ;               obviously LIT,XSQUOTE,COMMA don't use T register...
            .word WORDD+4           ; -- c-addr     W=Count_of_chars
            mHI2LO                  ;
            ADD #1,W                ;               to include count of chars
            BIT #1,W                ;               C = /Z
            ADDC W,&DP              ; -- addr       new DP is aligned
            JMP DROPEXIT            ; --
            
            FORTHWORDIMM ".\34"     ; immediate
; https://forth-standard.org/standard/core/Dotq
; ."       --              compile string to print
DOTQUOTE    mCOLON
            .word   SQUOTE
            .word   LIT,TYPE,COMMA
            .word   EXIT

;-------------------------------------------------------------------------------
; NUMERIC OUTPUT
;-------------------------------------------------------------------------------
; Numeric conversion is done last digit first, so
; the output buffer is built backwards in memory.

            FORTHWORD "<#"
; https://forth-standard.org/standard/core/num-start
; <#    --       begin numeric conversion (initialize Hold Pointer)
LESSNUM     MOV #HOLD_BASE,&HP
            MOV @IP+,PC

; primitive MU/MOD; used by ?NUMBER UM/MOD, and M*/ in DOUBLE word set
; MU/MOD    UDVDlo UDVDhi UDIVlo -- UREMlo UQUOTlo UQUOThi
;-------------------------------------------------------------------------------
; unsigned 32-BIT DiViDend : 16-BIT DIVisor --> 32-BIT QUOTient 16-BIT REMainder
;-------------------------------------------------------------------------------
; two times faster if 16 bits DiViDend (cases of U. and . among others)

; reg     division            MU/MOD      NUM                       M*/
; ---------------------------------------------------------------------
; S     = DVD(15-0)         = ud1lo     = ud1lo                     ud1lo
; TOS   = DVD(31-16)        = ud1hi     = ud1hi                     ud1mi
; W     = DVD(47-32)/REM    = rem       = digit --> char --> -[HP]  ud1hi
; T     = DIV(15-0)         = BASE      = BASE                      ud2
; X     = QUOTlo            = ud2lo     = ud2lo                     QUOTlo
; Y     = QUOThi            = ud2hi     = ud2hi                     QUOThi
; rDODOES = count

MUSMOD      MOV TOS,T               ;1  T = DIVlo
            MOV 2(PSP),S            ;3  S = DVDlo
            MOV @PSP,TOS            ;2  TOS = DVDhi
MUSMOD1     MOV #0,W                ;1  W = REMlo = 0
            MOV #32,rDODOES         ;2  init loop count
            CMP #0,TOS              ;1  DVDhi=0 ?
            JNZ MDIV1               ;2  no
; ----------------------------------;
MDIV1DIV2   RRA rDODOES             ;1  yes:loop count / 2
            MOV S,TOS               ;1      DVDhi <-- DVDlo
            MOV #0,S                ;1      DVDlo <-- 0
            MOV #0,X                ;1      QUOTlo <-- 0 (to do QUOThi = 0 at the end of division)
; ----------------------------------;
MDIV1       CMP T,W                 ;1  REMlo U>= DIVlo ?
            JNC MDIV2               ;2  no : carry is reset
            SUB T,W                 ;1  yes: REMlo - DIVlo ; carry is set
MDIV2       ADDC X,X                ;1  RLC quotLO
            ADDC Y,Y                ;1  RLC quotHI
            SUB #1,rDODOES          ;1  Decrement loop counter
            JN ENDMDIV              ;2
            ADD S,S                 ;1  RLA DVDlo
            ADDC TOS,TOS            ;1  RLC DVDhi
            ADDC W,W                ;1  RLC REMlo
            JNC MDIV1               ;2  14~
            SUB T,W                 ;1  REMlo - DIVlo
            BIS #1,SR               ;1  SETC
            JMP MDIV2               ;2  14~
ENDMDIV     MOV #XDODOES,rDODOES    ;2  restore rDODOES
            MOV W,2(PSP)            ;3  REMlo in 2(PSP)
            MOV X,0(PSP)            ;3  QUOTlo in 0(PSP)
            MOV Y,TOS               ;1  QUOThi in TOS
RET_ADR     MOV @RSP+,PC            ;4  35 words, about 240/440 cycles, not FORTH executable !

            FORTHWORD "#"
; https://forth-standard.org/standard/core/num
; #     ud1lo ud1hi -- ud2lo ud2hi          convert 1 digit of output
NUM         MOV &BASEADR,T          ;3
NUM1        MOV @PSP,S              ;2          -- DVDlo DVDhi              S = DVDlo
            SUB #2,PSP              ;1          -- x x DVDhi                TOS = DVDhi
            CALL #MUSMOD1           ;244/444    -- REMlo QUOTlo QUOThi      T is unchanged W=REMlo X=QUOTlo Y=QUOThi
            MOV @PSP+,0(PSP)        ;4          -- QUOTlo QUOThi            W = REMlo
TODIGIT     CMP.B #10,W             ;2
            JNC TODIGIT1            ;2  jump if U<
            ADD.B #7,W              ;2
TODIGIT1    ADD.B #30h,W            ;2
HOLDW       SUB #1,&HP              ;3  store W=char --> -[HP]
            MOV &HP,Y               ;3
            MOV.B W,0(Y)            ;3
            MOV @IP+,PC             ;4 22 words, about 276|476 cycles for u|ud one digit

            FORTHWORD "#S"
; https://forth-standard.org/standard/core/numS
; #S    udlo udhi -- 0 0       convert remaining digits
NUMS        mCOLON
            .word   NUM             ;       X=QUOTlo
            mHI2LO                  ;       next adr
            SUB #2,IP               ;1      restore NUM return
            BIS TOS,X               ;1
            CMP #0,X                ;1 --   ud2lo ud2hi      ud = 0 ?
            JNZ NUM1                ;2
EXIT        MOV @RSP+,IP            ;2      when DTC=2 rDOCOL is loaded with this EXIT address
            MOV @IP+,PC             ;4 10 words, about 294|494 cycles for u|ud one digit

            FORTHWORD "#>"
; https://forth-standard.org/standard/core/num-end
; #>    udlo:udhi -- addr u    end conversion, get string
NUMGREATER  MOV &HP,0(PSP)          ; -- addr 0
            MOV #HOLD_BASE,TOS      ; -- addr addr
            SUB @PSP,TOS            ; -- addr u
            MOV @IP+,PC

            FORTHWORD "HOLD"
; https://forth-standard.org/standard/core/HOLD
; HOLD  char --        add char to output string
HOLD        MOV.B TOS,W             ;1
            MOV @PSP+,TOS           ;2
            JMP HOLDW               ;15

            FORTHWORD "SIGN"
; https://forth-standard.org/standard/core/SIGN
; SIGN  n --           add minus sign if n<0
SIGN        CMP #0,TOS              ; 1
            MOV @PSP+,TOS           ; 2
            MOV.B #'-',W            ; 2
            JN HOLDW                ; 2 jump if 0<
            MOV @IP+,PC             ; 4

            FORTHWORD "U."
; https://forth-standard.org/standard/core/Ud
; U.    u --           display u (unsigned)
; note: DDOT = UDOT + 10 (see DOUBLE.f)
UDOT        MOV #0,S                ; 1                 S=sign
            SUB #2,PSP              ; 1
            MOV TOS,0(PSP)          ; 3 -- |lo| x
            MOV #0,TOS              ; 1 -- |lo| |hi|     
UDOTNEXT    PUSHM #2,IP             ; 4                 R-- IP sign
            mLO2HI                  ;10
            .word   LESSNUM
            .word   LIT,' ',HOLD    ;                   add a trailing space
            .word   NUMS            ;
            .word   RFROM,SIGN      ;           IP sign R-- IP
            .word   NUMGREATER,TYPE ;
            .word   EXIT            ;                IP R-- 

            FORTHWORD "."
; https://forth-standard.org/standard/core/d
; .     n --           display n (signed)
DOT         CMP #0,TOS
            JGE UDOT
            XOR #-1,TOS             ;1 set TOS = |lo|
            ADD #1,TOS              ;1
            MOV #-1,S               ;1 set S = minus sign
            JMP UDOT+2

;-------------------------------------------------------------------------------
; INTERPRETER
;-------------------------------------------------------------------------------
; https://forth-standard.org/standard/core/WORD
; WORD   char -- addr        Z=1 if len=0
; parse a word delimited by char separator.
; the resulting c-string is left at HERE.
; if CAPS is ON, this word is CAPITALIZED unless for a 'char' input.
; notice that the average lenght of all CORE definitions is about 4.
; BL_WORD = 106/126 cycles for 4 upper/lower case chars.
            FORTHWORD "WORD"
            JMP WORDD           ;2
;-------------------------------;
BL_WORD     SUB #2,PSP          ;               )
            MOV TOS,0(PSP)      ;               > 6~ instead of 16~ for CONSTANT BL runtime
            MOV #' ',TOS        ;  -- BL        ) 
WORDD       MOV #32,T           ;2 -- sep       CAPS OFF = 0, CAPS ON = $20.
            MOV #SOURCE_LEN,S   ;2              WORDD+4 = WORD_CFA+16
            MOV @S+,X           ;2              X = src_len
            MOV @S+,Y           ;2              Y = src_org
            ADD Y,X             ;1              X = src_len + src_org = src_end
            ADD @S+,Y           ;2              Y = >IN + src_org = src_ptr
            MOV @S,W            ;2              W = HERE = dst_ptr
;-------------------------------;15
SKIPSEPLOOP CMP X,Y             ;1              src_ptr >= src_end ?
            JC WORDEND          ;2              if yes : End Of Line !
            CMP.B @Y+,TOS       ;2              char = separator ?
            JZ SKIPSEPLOOP      ;2              if yes; 7~ loop (one loop minimum)
            SUB #1,Y            ;1              decrement the post incremented src_ptr
;-------------------------------;30
SCANTICK    CMP.B #"'",2(Y)     ;4              third char = TICK ? (that allows ' as first char for a definition name)
            JNZ SCANWRDLOOP     ;2              no
            MOV #0,T            ;1              yes, don't capitalize 'char' input
;-------------------------------;34
SCANWRDLOOP MOV.B S,0(W)        ;3              first, make room at dst_ptr for string length; next, put it char.
            CMP X,Y             ;1              src_ptr = src_end ?
            JZ WORDEND          ;2              if yes
            MOV.B @Y+,S         ;2              S=char
            CMP.B S,TOS         ;1 -- sep       does char = separator ?
            JZ WORDEND          ;2              if yes
            ADD #1,W            ;1              increment dst_ptr
            CMP.B #'a',S        ;2              char U< 'a' ?  this condition is tested at each loop
            JNC SCANWRDLOOP     ;2              16~ upper case char loop
            CMP.B #'z'+1,S      ;2              char U> 'z' ?
            JC SCANWRDLOOP      ;2              loopback if yes
            SUB.B T,S           ;1              convert a...z to A...Z if CAPS ON (T=$20)
            JMP SCANWRDLOOP     ;2              19~ lower case char loop
;-------------------------------;19
WORDEND     SUB &SOURCE_ORG,Y   ;3 -- sep       Y=src_ptr - src_org = new >IN (first char separator next)
            MOV Y,&TOIN         ;3              update >IN for next search in this input stream
            MOV &DP,TOS         ;3 -- addr      TOS = HERE
            SUB TOS,W           ;1              W = Word_Length >= 0
            MOV.B W,0(TOS)      ;3 -- c-addr
            MOV @IP+,PC         ;4              Z=1 <==> Word_Length = 0 <==> EOL, tested by INTERPRET

            FORTHWORD "FIND"    ;
; https://forth-standard.org/standard/core/FIND
; FIND   c-addr -- c-addr 0    if not found ; flag Z=1  c-addr = HERE
;                  CFA -1      if found     ; flag Z=0
;                  CFA  1      if immediate ; flag Z=0
; compare WORD at c-addr (HERE)  with each of words in each of listed vocabularies in CONTEXT
; start of FIND         : 10/17 cycles (mono/multi threads),
; WORDLOOP on len       : 14 cycles,
; WORDLOOP on 1st char  : 21 cycles,
; CHARCOMP loop         : 10 cycles,
; WORDFOUND to end      : 16 cycles,
; WORDNOTFOUND to end   : 20 cycles.
FIND        SUB #2,PSP          ;1 -- ???? c-addr       reserve one cell, not at FINDEND that would kill the Z flag
            MOV TOS,S           ;1                      S=c-addr
            MOV #CONTEXT,T      ;2                      T = first word-set addr of CONTEXT stack
VOCLOOP     MOV @T+,TOS         ;2 -- ???? VOC_THREAD0  T=CTXT+2
            CMP #0,TOS          ;1                      no more word-set in CONTEXT stack ?
            JZ FINDEND          ;2 -- ???? 0            yes ==> exit with Z=1
    .IF THREADS<>1              ;                       searching thread x adds 7 cycles & 6 words
            MOV.B 1(S),Y        ;3                      S=c-addr, Y=first char of c-addr string
            AND.B #(THREADS-1),Y;2                      Y=thread_x number
            ADD Y,Y             ;1                      Y=thread_x offset address
            ADD Y,TOS           ;1 -- ???? VOC_THRDx
    .ENDIF
            ADD #2,TOS          ;1 -- ???? VOC_THRDx+2
WORDLOOP    MOV -2(TOS),TOS     ;3 -- ???? NFA          -2(TOS) = [VOC_THRDx] first, then [LFA]
            CMP #0,TOS          ;1                      no (more) definition in the (selected) thread ?
            JZ VOCLOOP          ;2                      yes ==> search next word-set in CONTEXT stack
            MOV TOS,X           ;1
            MOV.B @X+,Y         ;2                      TOS = NFA,  X= NFA+1, Y = NFA_first_byte = cnt<<1 + i (i= immediate flag)
            RRA.B Y             ;1                      remove immediate flag, the remainder is the count of the definition name.
LENCOMP     CMP.B @S,Y          ;2                      compare lenght
            JNZ WORDLOOP        ;2                      14~ word loop on lenght mismatch
            MOV S,W             ;1                      S=W=c-addr
CHARCOMP    CMP.B @X+,1(W)      ;4                      compare chars
            JNZ WORDLOOP        ;2                      21~ word loop on first char mismatch
            ADD #1,W            ;1
            SUB.B #1,Y          ;1                      decr count
            JNZ CHARCOMP        ;2                      10~ char loop
WORDFOUND   BIT #1,X            ;1
            ADDC #0,X           ;1
            MOV X,S             ;1                      S=aligned CFA
            MOV.B @TOS,TOS      ;2 -- ???? NFA_1st_byte 
            AND #1,TOS          ;1 -- ???? 0|1          test immediate flag
            JNZ FINDEND         ;2 -- ???? 1            jump if bit 1 is set, as immediate bit
            SUB #1,TOS          ;1 -- ???? -1
FINDEND     MOV S,0(PSP)        ;3 -- xt -1/1           if found, not_immediate/immediate, flag Z=0
                                ;  -- c-addr 0          if not found, flag Z=1 
            MOV @IP+,PC         ;4 34/40 words          return to interpreter, S = CFA

            FORTHWORD ">NUMBER"
; >NUMBER  ud1lo ud1hi addr1 cnt1 -- ud2lo ud2hi addr2 cnt2
; https://forth-standard.org/standard/core/toNUMBER
; ud2 is the unsigned result of converting the characters within the string specified by c-addr1 u1 into digits,
; using the number in BASE, and adding each into ud1 after multiplying ud1 by the number in BASE.
; Conversion continues left-to-right until a character that is not convertible (including '.'  ','  '_')
; is encountered or the string is entirely converted. c-addr2 is the location of the first unconverted character
; or the first character past the end of the string if the string was entirely converted.
; cnt2 is the number of unconverted characters in the string.
; An ambiguous condition exists if ud2 overflows during the conversion.
            MOV &BASEADR,T      ;3                      T = base
            MOV @PSP+,S         ;2 -- ud1lo ud1hi cnt1  S = addr1
            MOV @PSP+,Y         ;2 -- ud1lo cnt1        Y = ud1hi
            MOV @PSP,X          ;2 -- x cnt1            X = ud1lo
            SUB #4,PSP          ;1 -- x x x cnt1
    .IFDEF MPY_32 ; if 32 bits hardware multiplier
TONUM_INPUT MOV T,&MPY          ;3                      base = MPY = OP1, loaded out of TONUMLOOP
    .ELSE
TONUM_INPUT                     ;                       <-- QNUMBER
    .ENDIF
TONUMLOOP   MOV.B @S,W          ;2 -- x x x cnt         S=adr, T=base, W=char, X=udlo, Y=udhi
DDIGITQ     SUB.B #':',W        ;2                      all Ctrl_Chars and nums < '9' become negative
            JNC DDIGITQNEXT     ;2                      accept all chars U< ':'  (accept $0 up to $39)
            SUB.B #7,W          ;2                      W = char - (':' + $07 = 'A')
            JNC TONUMEND        ;2 -- x x x cnt         reject all Chars U< 'A', (with Z flag = 0)
DDIGITQNEXT ADD.B #10,W         ;2                      restore digit value: 0 to 9 (and beyond)
            CMP T,W             ;1                      digit - base
            BIC #Z,SR           ;1                      clear Z to avoid conversion true with digit=base :-(
            JC TONUMEND         ;2                      
    .IFDEF MPY_32 ; if 32 bits hardware multiplier      (ud * base) + digit --> ud 
            MOV X,&OP2L         ;3                      Load 2nd operand (ud1lo)
            MOV Y,&OP2H         ;3                      Load 2nd operand (ud1hi)
            MOV &RES0,X         ;3                      lo result in X (ud2lo)
            MOV &RES1,Y         ;3                      hi result in Y (ud2hi)
ADD_DIGIT   ADD W,X             ;1                      ud2lo + digit
            ADDC #0,Y           ;1                      ud2hi + carry
TONUMPLUS   ADD #1,S            ;1                      adr+1
            SUB #1,TOS          ;1 -- x x x cnt-1       cnt-1
            JNZ TONUMLOOP       ;2                      if count <>0    33~ per digit
TONUMEND    MOV S,0(PSP)        ;3 -- x x addr2 cnt2
    .ELSE ; no hardware multiplier                      (ud * base) + digit --> ud 
            MOV #0,rDODOES      ;1 RESlo=0
            MOV #0,rDOCON       ;1 REShi=0
            MOV #1,rDOVAR       ;1 BIT TEST for base
MUL_LOOP    BIT rDOVAR,T        ;1 test actual bit in base
            JZ MUL_SHIFT        ;2
            ADD X,rDODOES       ;1 IF 1: ADD ud1lo TO RESlo (OP2L + RES0 --> RES0)
            ADDC Y,rDOCON       ;1      ADDC ud1hi TO REShi (OP2H + RES1 + C --> RES1)
MUL_SHIFT   ADD X,X             ;1 (RLA LSBs) ud1lo *2      (OP2L*2)
            ADDC Y,Y            ;1 (RLC MSBs) ud1hi *2      (OP2H*2)
            ADD rDOVAR,rDOVAR   ;1 (RLA) NEXT BIT TO TEST   (BIT_TEST<1)
            JNC MUL_LOOP        ;2 IF BIT IN CARRY: FINISHED
            MOV rDODOES,X       ;1 RESlo --> ud2lo
            MOV rDOCON,Y        ;1 REShi --> ud2hi
ADD_DIGIT   ADD W,X             ;1                      ud2lo + digit
            ADDC #0,Y           ;1                      ud2hi + carry
TONUMPLUS   ADD #1,S            ;1                      adr+1
            SUB #1,TOS          ;1 -- x x x cnt-1       cnt-1
            JNZ TONUMLOOP       ;2                      if count <>0
TONUMEND    MOV #INIT_DOXXX,W   ;2
            MOV @W,rDOVAR       ;2                        RFROM --> R7=rDOVAR
            MOV @W+,rDOCON      ;2                       XDOCON --> R6=rDOCON
            MOV @W+,rDODOES     ;2                      XDODOES --> R5=rDODOES
            MOV S,0(PSP)        ;3 -- x x addr2 cnt2
    .ENDIF
            MOV Y,2(PSP)        ;3 -- x ud2hi addr2 cnt2
            MOV X,4(PSP)        ;3 -- ud2lo ud2hi addr2 cnt2
            MOV @IP+,PC         ;4 ret to TONUMEXIT

; ?NUMBER makes the interface between INTERPRET and >NUMBER; also used by ASSEMBLER.
; convert a string to a signed number; FORTH 2012 prefixes $  %  # are recognized,
; FORTH 2012 'char' numbers and digits separator '_' also.
; with DOUBLE_INPUT option, 32 bits signed numbers (with decimal point) are recognized,
; with FIXPOINT_INPUT option, Q15.16 signed numbers (with comma) are recognized.
; prefixes ' # % $ - are processed before calling >NUMBER
; chars . , _  are processed as >NUMBER exits.
;Z ?NUMBER  addr -- n|d -1  if convert ok ; flag Z=0, UF1=1 if double
;Z          addr -- addr 0  if convert ko ; flag Z=1

INTQNUM     MOV #INTQNUMNEXT,IP ;2                          INTQNUMNEXT is the next of QNUMBER     
        .IFDEF DOUBLE_NUMBERS   ;                           DOUBLE_NUMBERS = DOUBLE_INPUT | FIXPOINT_INPUT
QNUMBER     BIC #UF1,SR         ;2                          clear UserFlag_1 used as double number flag
            SUB #8,PSP          ;1 -- x x x x addr          make room for >NUMBER
        .ELSE                   ;
QNUMBER     SUB #8,PSP          ;1 -- x x x x addr          make room for >NUMBER
        .ENDIF
            MOV TOS,6(PSP)      ;3 -- addr x x x addr       save TOS for TONUMEXIT 
            MOV #0,Y            ;1                          Y=ud1hi=0
            MOV #0,X            ;1                          X=ud1lo=0
            MOV &BASEADR,T      ;3                          T=BASE
            MOV TOS,S           ;1                          S=addr
            MOV #0,TOS          ;1                          TOS=sign of result
            PUSHM #2,TOS        ;4 R-- sign IP              PUSH TOS,IP
            MOV #TONUMEXIT,IP   ;2                          set TONUMEXIT as return from >NUMBER
            MOV.B @S+,TOS       ;2 -- addr x x x cnt        TOS=count, S=addr+1
QNUMLDCHAR  MOV.B @S,W          ;2                          W=char
            SUB.B #'-',W        ;2                          char '-' ?
            JZ QNUMMINUS        ;2                              negate sign
            JC TONUM_INPUT      ;2 -- addr x x x cnt        jump if char U> '-', case of numeric chars
QBINARY     MOV #2,T            ;1                          preset base 2
            ADD.B #8,W          ;1                          binary '%' prefix ?     '%' + 8 = '-'
            JZ PREFIXNEXT       ;2                          yes
QDECIMAL    ADD #8,T            ;1                          preset base 10
            ADD.B #2,W          ;1                          decimal '#' prefix ?    '#' + 2 = '%'
            JZ PREFIXNEXT       ;2                          yes
QHEXA       MOV #16,T           ;2                          preset base 16
            CMP.B #1,W          ;1                          hex '$' prefix ?        '#' + 1 = '$'
            JZ PREFIXNEXT       ;2                          yes
QTICK       CMP.B #4,W          ;1                          ' prefix ?              '#' + 4 = "'"
            JNZ QNUMNEXT        ;2 -- addr x x x cnt        no, abort because other prefixes not recognized
            CMP #3,TOS          ;2                          count = 3 ?
            JNZ QNUMNEXT        ;2                          no, abort
            CMP.B @S+,1(S)      ;4 -- addr x x x 3          3rd char = 1st char = "'" ?
            MOV.B @S,S          ;2                          does byte to word conversion
            MOV S,4(PSP)        ;3 -- addr ud2lo x x 3      ud2lo = ASCII code of 'char'
            JMP QNUMNEXT        ;2 -- addr ud2lo x x 3      with happy end only if 3rd char = 1st char = "'"
QNUMMINUS   MOV #-1,2(RSP)      ;3 R-- sign IP              set sign flag
PREFIXNEXT  SUB #1,TOS          ;1 -- addr x x x cnt-1      TOS=count-1
            CMP.B @S+,0(S)      ;4                          S=adr+1; same prefix ?
            JNZ QNUMLDCHAR      ;2                          loopback if no
            JZ TONUM_INPUT      ;2                          if yes, this double prefix will be rejected by >NUMBER
; ------------------------------;46 
TONUMEXIT   mHI2LO              ;  -- addr ud2lo-hi addr2 cnt2      R-- IP sign BASE    S=addr2
            JZ QNUMNEXT         ;2                                  TOS=0 and Z=1 if conversion is ok
            SUB #2,IP           ;1                                  redefines TONUMEXIT as >NUMBER return, if loopback applicable
            MOV.B @S,W          ;2                                  reload rejected char
            CMP.B #'_',W        ;2                                  rejected char by >NUMBER is a underscore ?
            JZ TONUMPLUS        ;2                                  yes: return to >NUMBER to skip char then resume conversion, 30~ loopback
        .IFDEF DOUBLE_NUMBERS   ;                                   DOUBLE_NUMBERS = DOUBLE_INPUT | FIXPOINT_INPUT
            BIT #UF1,SR         ;2                                  UF1 already set ? ( if you have typed .. )
            JNZ QNUMNEXT        ;2                                  yes, goto QNUMKO
            BIS #UF1,SR         ;2                                  set double number flag
        .ENDIF
        .IFDEF DOUBLE_INPUT     ;
            SUB.B #'.',W        ;2                                  rejected char by >NUMBER is a decimal point ?
            JZ TONUMPLUS        ;2                                  yes, loopback to >NUMBER to skip char, 45~ loopback
        .ENDIF                  ;
        .IFDEF FIXPOINT_INPUT   ;
            .IFDEF DOUBLE_INPUT
            ADD.B #2,W          ;1                                  rejected char by >NUMBER is a comma ? (',' - '.' + 2 = 0)
            .ELSE               ;
            CMP.B #',',W        ;2                                  rejected char by >NUMBER is a comma ?
            .ENDIF              ;
            JNZ QNUMNEXT        ;2                                  no: with Z=0 ==> goto QNUMKO
; ------------------------------;  -- addr ud2lo   x      x     x   S=addr2, T=base
S15Q16      MOV TOS,W           ;1 -- addr ud2lo   x      x     x   W=cnt2
            MOV #0,X            ;1 -- addr ud2lo   x      x     x   init X = ud2lo' = 0
S15Q16LOOP  MOV X,2(PSP)        ;3 -- addr ud2lo ud2lo'   x     x   2(PSP) = X = ud2lo' = 0 then uqlo
            SUB.B #1,W          ;1                                  decrement cnt2
            MOV W,X             ;1                                  X = cnt2-1
            ADD S,X             ;1                                  X = end_of_string-1,-2,-3...
            MOV.B @X,X          ;2                                  X = last char of string first (reverse conversion)
            SUB.B #':',X        ;2
            JNC QS15Q16DIGI     ;2                                  accept all chars U< ':'
            SUB.B #7,X          ;2
            JNC S15Q16EOC       ;2                                  reject all chars U< 'A'
QS15Q16DIGI ADD.B #10,X         ;2                                  restore digit value
            CMP T,X             ;1                                  T=Base, is X a digit ?
            JC S15Q16EOC        ;2 -- addr ud2lo ud2lo'   x     x   if not a digit, --> goto QNUMKO
            MOV X,0(PSP)        ;3 -- addr ud2lo ud2lo' digit   x   
            MOV T,TOS           ;1 -- addr ud2lo ud2lo' digit  base                 R-- IP sign
            PUSHM #3,S          ;5 -- addr ud2lo ud2lo' ud2hi' base PUSH S,T,W:     R-- IP sign addr2 base cnt2
            CALL #MUSMOD        ;4                                  CALL MU/MOD
            POPM #3,S           ;5 -- addr ud2lo   ur   uqlo   uqhi restore W,T,S:  R-- IP sign
            JMP S15Q16LOOP      ;2                                  X=uqlo
S15Q16EOC   MOV 4(PSP),2(PSP)   ;5 -- addr ud2lo ud2hi  uqlo   uqhi ud2lo from >NUMBER becomes ud2hi part of Q15.16
            MOV @PSP,4(PSP)     ;4 -- addr ud2lo ud2hi  uqlo   uqhi uqlo becomes ud2lo part of Q15.16
            CMP.B #0,W          ;1                                  count = 0 if end of conversion ok
        .ENDIF ; FIXPOINT_INPUT ;
; ------------------------------;
QNUMNEXT    POPM #2,TOS         ;4 -- addr ud2lo-hi x sign  R: --   POP TOS,IP  TOS = sign flag = {-1;0}
            JZ QNUMOK           ;2 -- addr ud2lo-hi x sign          conversion OK if Z=1
QNUMKO      ADD #6,PSP          ;2 -- addr sign
            AND #0,TOS          ;1 -- addr ff
            MOV @IP+,PC         ;4                           =====> TOS=0 and Z=1 ==> conversion ko
        .IFDEF DOUBLE_NUMBERS   ;  -- addr ud2lo-hi x sign
QNUMOK      ADD #2,PSP          ;1 -- addr ud2lo-hi sign
            MOV 2(PSP),4(PSP)   ;5 -- udlo udlo udhi sign
            MOV @PSP+,0(PSP)    ;4 -- udlo udhi sign                note : PSP is incremented before write back.
            XOR #-1,TOS         ;1 -- udlo udhi inv(sign)
            JNZ QDOUBLE         ;2                                  if jump : TOS=-1 and Z=0 ==> conversion ok
            XOR #-1,TOS         ;1 -- udlo udhi -1
QDNEGATE    XOR #-1,2(PSP)      ;3
            XOR #-1,0(PSP)      ;3
            ADD #1,2(PSP)       ;3
            ADDC #0,0(PSP)      ;3
QDOUBLE     BIT #UF1+UF2,SR     ;2                                  UF1 (decimal point or comma fixpoint) or UF2 (ASM extended instructions) flags ?
            JZ NIP              ;2 -- dlo dhi tf                    no, remove dhi and set Z=0
QNUMEND     MOV @IP+,PC         ;4                                  TOS=tf and Z=0 ==> conversion ok
        .ELSE                   ;  -- addr ud2lo-hi x sign
QNUMOK      ADD #4,PSP          ;1 -- addr ud2lo sign
            MOV @PSP+,0(PSP)    ;4 -- ud2lo sign                    note : PSP is incremented before write back.
            XOR #-1,TOS         ;1 -- u inv(sign)
            JNZ QNUMEND         ;2                                  with TOS=-1 and Z=0 ==> conversion ok
            XOR #-1,TOS         ;1 -- u -1
QNEGATE     XOR #-1,0(PSP)      ;3
            ADD #1,0(PSP)       ;3 -- n tf
QNUMEND     MOV @IP+,PC         ;4 -- n|u tf                 =====> TOS=-1 and Z=0 ==> conversion ok
        .ENDIF ; DOUBLE_NUMBERS ;


            FORTHWORDIMM "\\"       ; immediate
; https://forth-standard.org/standard/block/bs
; \         --      backslash
; everything up to the end of the current line is a comment.
BACKSLASH   MOV &SOURCE_LEN,&TOIN   ;
            MOV @IP+,PC

; ------------------------------;
; INTERPRET    i*x addr u -- j*x      interpret given buffer
; This is the common factor of EVALUATE and QUIT.
; set addr u as input buffer then parse it word by word
INTERPRET   mCOLON              ;               INTERPRET = BACKSLASH + 8
            .word SETIB         ; --            set input buffer pointers
INTLOOP     .word BL_WORD       ; -- c-addr     flag Z = 1 <=> End Of Line
            .word ZBRAN,FDROPEXIT;              early return if End of Line
            .word FIND          ;               X = CFA
            mHI2LO              ; -- xt|c-addr|xt -1|0|+1   Z=1 --> not found
            MOV TOS,W           ;                           W = flag = (-1|0|+1) as (not_immediate|not_found|immediate)
            MOV @PSP+,TOS       ; -- xt|c-addr|xt
            JZ INTQNUM          ;2              if Z=1 --> name not found, search a number from c_addr
            MOV #INTLOOP,IP     ;2              INTLOOP is the next of EXECUTE|COMMA
            XOR &STATE,W        ;3
            JZ COMMA            ;2 -- xt        if W xor STATE = 0 compile xt, then loop back to INTLOOP
EXECUTE     PUSH TOS            ;3 -- xt
            MOV @PSP+,TOS       ;2 --
            MOV @RSP+,PC        ;4              xt --> PC, then loop back to INTLOOP
; ------------------------------;               <---- return of INTQNUM
INTQNUMNEXT mHI2LO              ;  -- n|c-addr fl   Z = 1 --> not a number, SR(UF1) double number request
            MOV @PSP+,TOS       ;2 -- n|c-addr
            MOV #INTLOOP,IP     ;2              INTLOOP is the next of LITERAL.
            JNZ LITERAL         ;2 n --         Z = 0 --> is a number, execute LITERAL then loop back to INTLOOP
NOTFOUND    MOV #FQABORT_YES,IP ;2              QABORT_YES becomes the end of INTERPRET
            ADD.B #1,0(TOS)     ;3 c-addr --    Z = 1 --> Not a Number : incr string count to add '?'
            MOV.B @TOS,Y        ;2              Y=count+1
            ADD TOS,Y           ;1              Y=end of string addr
            MOV.B #'?',0(Y)     ;5              add '?' to end of string
            JMP COUNT           ;2 -- addr len  return to ABORT_TERM

            FORTHWORDIMM "LITERAL"  ; immediate
    .IFDEF DOUBLE_NUMBERS       ; are recognized
; https://forth-standard.org/standard/core/LITERAL
; LITERAL  n --        append single numeric literal if compiling state
;          d --        append two numeric literals if compiling state and UF1<>0 (not ANS)
LITERAL     CMP #0,&STATE       ;3
            JZ LITERALNEXT      ;2 if interpreting state, does nothing else to clear UF1 flag
            MOV TOS,X           ;1          X = n|dhi
LITERALLOOP MOV &DP,W           ;3
            ADD #4,&DP          ;3
            MOV #LIT,0(W)       ;4
            MOV X,2(W)          ;3 pass 1: compile n, if pass 2: compile dhi
            MOV @PSP+,TOS       ;2
            BIT #UF1,SR         ;2 double number ?
LITERALNEXT BIC #UF1,SR         ;2    in all case, clear UF1
            JZ LITERALEND       ;2 no  goto end if n|interpret_state
            MOV TOS,2(W)        ;3 yes compile dlo over dhi
            JMP LITERALLOOP     ;2
LITERALEND  MOV @IP+,PC         ;4
    .ELSE
; https://forth-standard.org/standard/core/LITERAL
; LITERAL  n --        append single numeric literal if compiling state
LITERAL     CMP #0,&STATE       ;3
            JZ LITERALEND       ;2 if interpreting state, does nothing
            MOV &DP,W           ;3
            ADD #4,&DP          ;3
            MOV #LIT,0(W)       ;4
            MOV TOS,2(W)        ;3
            MOV @PSP+,TOS       ;2
LITERALEND  MOV @IP+,PC         ;4
    .ENDIF

; https://forth-standard.org/standard/core/DEPTH
; DEPTH    -- +n        number of items on stack, must leave 0 if stack empty
QDEPTH      SUB #2,PSP
            MOV TOS,0(PSP)      ; 3
            MOV #PSTACK-2,TOS   ; 2 because previous PSP-2
            SUB PSP,TOS         ; 1 PSP-S0  --> TOS
            RRA TOS             ; 1 TOS/2   --> TOS
; https://forth-standard.org/standard/core/Zeroless
; 0<     n -- flag      true if TOS negative
ZEROLESS    ADD TOS,TOS         ;1 set carry if TOS negative
            SUBC TOS,TOS        ;1 TOS=-1 if carry was clear
INVERT      XOR #-1,TOS         ;1 TOS=-1 if carry was set
            MOV @IP+,PC         ;4

QFRAM_FULL  SUB #2,PSP          ; 2
            MOV TOS,0(PSP)      ; 3
            MOV #0,TOS          ; 1
            CMP #FRAM_FULL,&DP  ; 4
            JC INVERT           ; 2
            MOV @IP+,PC         ; 4 16~

            FORTHWORD "COUNT"
; https://forth-standard.org/standard/core/COUNT
; COUNT   c-addr1 -- adr len   counted->adr/len
COUNT       SUB #2,PSP          ;1
            MOV.B @TOS+,W       ;2
            MOV TOS,0(PSP)      ;3
            MOV W,TOS           ;1
            AND #-1,TOS         ;1       Z is set if u=0
            MOV @IP+,PC         ;4 12~

            FORTHWORD "ALLOT"
; https://forth-standard.org/standard/core/ALLOT
; ALLOT   n --         allocate n bytes
            ADD TOS,&DP
            MOV @PSP+,TOS
            MOV @IP+,PC

ABORT_DOWNLOAD
            CALL #SKIP_DOWNLOAD     ; to discard pending download if any
;            FORTHWORD "ABORT"
; https://forth-standard.org/standard/core/ABORT
; Empty the data stack and perform the function of QUIT
ABORT       MOV #PSTACK,PSP         ; ABORT = ALLOT+$0C
            MOV #0,TOS              ; clear TOS for SYS use. 
; https://forth-standard.org/standard/core/QUIT
; QUIT  --     clear return stack then interpret line by line the input stream
QUIT        MOV #RSTACK,RSP         ; QUIT = ALLOT+$12
            mLO2HI
    .IFDEF PROMPT
QUIT_PR     .word   XSQUOTE         ;
            .byte   5,13,10,"ok "   ; CR+LF + Forth prompt
            .word   TYPE            ; 79~
    .ELSE
QUIT_XS     .word   XSQUOTE         ; 16~                   
            .byte   2,13,10         ; CR+LF
            .word   TYPE            ; 79~
    .ENDIF
            .word   REFILL          ; -- org len            refill the input line buffer from ACCEPT
BTSTRP_QUIT .word   INTERPRET       ; --                    store org len and interpret this new line
            .word   QDEPTH          ; 15~                   stack empty test
            .word   XSQUOTE         ; 16~                   ABORT" stack empty"
            .byte   11,"stack empty";
            .word   QABORT          ; 14~                   see QABORT in forthMSP430FR_TERM_xxx.asm
            .word   QFRAM_FULL      ; 16~                   FRAM full test
            .word   XSQUOTE         ; 16~                   ABORT" MAIN full"
            .byte   9,"FRAM full"   ;
            .word   QABORT          ; 14~ 
    .IFDEF PROMPT
            .word   LIT,STATE,FETCH ; STATE @
            .word   QFBRAN,QUIT_PR  ; 0= case of interpret state
            .word   XSQUOTE         ; 0<> case of compile state
            .byte   5,13,10,"   "   ; CR+LF + 3 spaces
    .ENDIF
            .word   BRAN,QUIT_XS    ; 6~

             FORTHWORDIMM "ABORT\34"
; https://forth-standard.org/standard/core/ABORTq
; ABORT" " (empty string) displays nothing
; ABORT"  i*x flag -- i*x   R: j*x -- j*x  flag=0
;         i*x flag --       R: j*x --      flag<>0
             PUSH IP
; ; ----------------------------------; uncomment to enable ABORT" in interpret mode
;             CMP #0,&STATE           ;
;             JNZ COMP_QABORT         ;
; EXEC_QABORT MOV #0,T                ; CAPS OFF
;             mLO2HI                  ;
;             .word   LIT,'"',WORDD+4 ;
;             .word   COUNT,QABORT    ;
;             .word   DROPEXIT        ;
; ; ----------------------------------;
COMP_QABORT mLO2HI
            .word   SQUOTE
            .word   LIT,QABORT,COMMA  ; see QABORT in forthMSP430FR_TERM_xxx.asm
FEXIT       .word   EXIT

             FORTHWORD "'"
; https://forth-standard.org/standard/core/Tick
; '    -- xt           find word in dictionary and leave on stack its execution address if exist else error.
TICK        mCOLON
            .word   BL_WORD,FIND
            .word   QFBRAN,FNOTFOUND;
            .word   EXIT
FNOTFOUND   .word   NOTFOUND        ; see INTERPRET

            FORTHWORDIMM "[']"      ; immediate word, i.e. word executed during compilation
; https://forth-standard.org/standard/core/BracketTick
; ['] <name>        --         find word & compile it as literal
BRACTICK    mCOLON
            .word   TICK            ; get xt of <name>
            .word   LIT,LIT,COMMA   ; append LIT action
            .word   COMMA,EXIT      ; append xt literal

            FORTHWORDIMM "["    ; immediate
; https://forth-standard.org/standard/core/Bracket
; [        --      enter interpretative state
LEFTBRACKET MOV #0,&STATE
            MOV @IP+,PC

            FORTHWORD "]"
; https://forth-standard.org/standard/core/right-bracket
; ]        --      enter compiling state
RIGHT_BRKT  MOV  #-1,&STATE
            MOV @IP+,PC

;-------------------------------------------------------------------------------
; COMPILER
;-------------------------------------------------------------------------------
            FORTHWORD ","
; https://forth-standard.org/standard/core/Comma
; ,    x --           append cell (two bytes) to dict, to allow mixing C, and ,
COMMA       ADD #2,&DP          ;1
            MOV &DP,W           ;3
            MOV.B TOS,-2(W)     ;3
            SWPB TOS            ;
            MOV.B TOS,-1(W)     ;
            MOV @PSP+,TOS       ;2
            MOV @IP+,PC         ;4 17~      W = DP

            FORTHWORDIMM "POSTPONE"
; https://forth-standard.org/standard/core/POSTPONE
POSTPONE    mCOLON
            .word   BL_WORD,FIND
            .word   ZBRAN,FNOTFOUND ; BRANch to FNOTFOUND if Z = 1
            .word   ZEROLESS        ; immediate word ?
            .word   QFBRAN,POST1    ;
            .word   LIT,LIT,COMMA   ; if no immediate   compile LIT
            .word   COMMA           ;                   compile xt_of_word
            .word   LIT,COMMA       ;
POST1       .word   COMMA,EXIT      ; compile xt_of_word if immediate else compile COMMA

            FORTHWORD ":"
; https://forth-standard.org/standard/core/Colon
; : <name>     --      begin a colon definition
            PUSH #COLONNEXT         ;3              define COLONNEXT as HEADER return
;-----------------------------------;
HEADER      BIT #1,&DP              ;3              carry set if odd
            ADDC #2,&DP             ;4              align and make room for LFA
            mCOLON                  ;
            .word BL_WORD           ; -- HERE       W = Count_of_chars definition, up to 127
            .word HEADERNEXT        ;
HEADERNEXT  MOV @RSP+,IP            ;
            MOV TOS,Y               ;               Y=HERE=NFA
            ADD.B @TOS+,-1(TOS)     ;               NFA_1st_byte<<1 (make room for immediate flag, clear it)
            BIS.B #1,W              ;               make Count_of_chars always odd
            ADD.B #1,W              ;               W=add one byte to include Count_of_chars byte
            ADD Y,W                 ;               W=Aligned_CFA
            MOV &CURRENT,X          ;               X = Thread(0) of [CURRENT]
    .IF THREADS<>1                  ;               multithreading add 5~ 4words
            MOV.B @TOS,TOS          ; -- char       TOS=first CHAR of new word
            AND #(THREADS-1),TOS    ; -- offset     TOS= thread(n)
            ADD TOS,TOS             ;               TOS= thread_offset (bytes)
            ADD TOS,X               ;               X = Thread(n) addr
    .ENDIF                          ;
            MOV @PSP+,TOS           ; --
HEADEREND   MOV Y,&LAST_NFA         ;               NFA --> LAST_NFA                used by QREVEAL, IMMEDIATE
            MOV X,&LAST_THREAD      ;               Thread(n) addr --> LAST_THREAD  used by QREVEAL
            MOV W,&LAST_CFA         ;               DP = CFA --> LAST_CFA           used by DOES>, RECURSE
            MOV PSP,&LAST_PSP       ;               save PSP for check compiling,   used by QREVEAL
            MOV W,&DP               ;
            MOV @RSP+,PC            ; RET           W is the new DP value )
;-----------------------------------;               X is LAST_THREAD      > used by compiling words: CREATE DEFER : CODE ...
COLONNEXT                           ;               Y is NFA              )
    .SWITCH DTC                     ; Direct Threaded Code select:
    .CASE 1                         ; [rDOCOL] = XDOCOL
            MOV #DOCOL,0(W)         ;   compile CALL R4 = rDOCOL
            ADD #2,&DP              ;   adjust DP
    .CASE 2                         ; [rDOCOL] = EXIT
            MOV #120Dh,0(W)         ;   compile PUSH IP       3~
            MOV #DOCOL,2(W)         ;   compile CALL R4 = rDOCOL
            ADD #4,&DP              ;   adjust DP
    .CASE 3                         ; [rDOCOL] = ???
            MOV #120Dh,0(W)         ;   compile PUSH IP       3~
            MOV #400Dh,2(W)         ;   compile MOV PC,IP     1~
            MOV #522Dh,4(W)         ;   compile ADD #4,IP     1~
            MOV #4D30h,6(W)         ;   compile MOV @IP+,PC   4~
            ADD #8,&DP              ;   adjust DP
    .ENDCASE                        ;
            JMP RIGHT_BRKT          ; enter compiling state
;-----------------------------------;

            FORTHWORD ":NONAME"
; https://forth-standard.org/standard/core/ColonNONAME
; :NONAME        -- xt
; W is DP
; X is the LAST_THREAD lure value for REVEAL
; Y is the LAST_NFA lure value for REVEAL and IMMEDIATE
; ...because we don't want to modify the word set !
            PUSH #COLONNEXT         ; define COLONNEXT as HEADEREND RET
HEADERLESS  SUB #2,PSP              ; --        <-- CODENNM
            MOV TOS,0(PSP)          ;
            MOV &DP,W               ;
            BIT #1,W                ;
            ADDC #0,W               ;           W = aligned CFA
            MOV W,TOS               ; -- xt     aligned CFA of :NONAME | CODENNM
            MOV #212h,X             ;           MOV Y,0(X)   will write to 212h = unused PA register address (lure for REVEAL)
            MOV X,Y                 ;           MOV @X,-2(Y) will write to 210h = unused PA register address (lure for REVEAL and IMMEDIATE)
            JMP HEADEREND           ;

;;Z ?REVEAL   --      if no stack mismatch, link this new word in the CURRENT vocabulary
QREVEAL     CMP PSP,&LAST_PSP       ; Check SP with its saved value by , :NONAME CODE...
            JNZ REVEAL_KO           ; if stack mismatch
QREVEAL_OK  MOV #0,&LAST_PSP        ; <-- added to avoid a crash with unpaired ENDCODE
            MOV &LAST_NFA,Y         ;                   if no error, link this definition in its thread
            MOV &LAST_THREAD,X      ;
REVEAL      MOV @X,-2(Y)            ; [LAST_THREAD] --> LFA         (for NONAME: LFA --> 210h unused PA reg)
            MOV Y,0(X)              ; LAST_NFA --> [LAST_THREAD]    (for NONAME: [LAST_THREAD] --> 212h unused PA reg)
            MOV @IP+,PC

REVEAL_KO   mLO2HI                  ;
            .word XSQUOTE           ;
            .byte 15,"stack mismatch!" ; -- addr cnt
FQABORT_YES .word QABORT_YES        ;

            FORTHWORDIMM ";"
; https://forth-standard.org/standard/core/Semi
; ;            --      end a colon definition
SEMICOLON   CMP #0,&STATE           ; if interpreting mode, semicolon becomes a comment identifier
            JZ BACKSLASH            ; tip: ";" is transparent to the preprocessor, so semicolon comments are kept in file.4th
            mCOLON                  ; compile mode
            .word   LIT,EXIT,COMMA
            .word   QREVEAL,LEFTBRACKET,EXIT

            FORTHWORD "IMMEDIATE"
; https://forth-standard.org/standard/core/IMMEDIATE
; IMMEDIATE        --   make last definition immediate
IMMEDIATE   MOV &LAST_NFA,Y         ;3
            BIS.B #1,0(Y)           ;4 FIND process more easier with bit0 than bit7 for IMMEDIATE flag
CODE_NEXT   MOV @IP+,PC

            FORTHWORD "CREATE"
; https://forth-standard.org/standard/core/CREATE
; CREATE <name>        --          define a CONSTANT with its next address
; Execution: ( -- a-addr )          ; a-addr is the address of name's data field
;                                   ; the execution semantics of name may be extended by using DOES>
CREATE      CALL #HEADER            ; --        W = DP
            ADD #4,W                ;1
            MOV #DOCON,-4(W)        ;4          -4(W) = CFA, [CFA] = CALL rDOCON
            MOV W,-2(W)             ;3          -2(W) = PFA, [PFA] = >BODY
CREATE_NEXT MOV W,&DP               ;4          update DP
            JMP REVEAL              ;           to link the definition in vocabulary

;; https://forth-standard.org/standard/core/DEFER
;; Skip leading space delimiters. Parse name delimited by a space.
;; Create a definition for name with the execution semantics defined below.
;;
;; name Execution:   --
;; Execute the xt that name is set to execute, i.e. NEXT (nothing),
;; until the phrase ' word IS name is executed, causing a new value of xt to be assigned to name.
;            FORTHWORD "DEFER"
;            CALL #HEADER
;            MOV #4030h,-4(W)        ;4 first CELL = MOV @PC+,PC = BR #addr
;            MOV #CODE_NEXT,-2(W)    ;3 second CELL              =   ...mNEXT : do nothing by default
;            JMP REVEAL              ; to link created word in vocabulary

; used like this (high level defn.):
;   DEFER DISPLAY                       create a "do nothing" definition (2 CELLS)

; inline command : ' U. IS DISPLAY      U. becomes the runtime of the word DISPLAY
; or in a definition : ... ['] U. IS DISPLAY ... ;
; KEY, EMIT, CR, ACCEPT are examples of DEFERred words

; DEFER is replaced with this more elegant low level defn.:
;   CODE DISPLAY                        create a "do nothing" definition (2 CELLS)
;   MOV #CODE_NEXT,PC                   CODE_NEXT is the address of NEXT code: MOV @IP+,PC
;   ENDCODE

; https://forth-standard.org/standard/core/IS
; ( xt "<spaces>name" -- )
; Skip leading spaces and parse name delimited by a space. Set name to execute xt
; FastForth plus : set name of a CREATE definition to redirect another CREATE definition 
            FORTHWORDIMM "IS"       ; immediate
IS          PUSH IP                 ; -- xt
            CMP #0,&STATE
            JNZ IS_COMPILE
IS_EXEC     mLO2HI
            .word   TICK            ; -- xt CFA_name
            mHI2LO                  ;
            MOV @RSP+,IP
DEFERSTORE  MOV @PSP+,2(TOS)        ; -- CFA_name          xt --> [PFA_name]
            MOV @PSP+,TOS           ; --
            MOV @IP+,PC             ;

IS_COMPILE  mLO2HI
            .word   BRACTICK        ; find the word, compile its CFA as literal
            .word   LIT,DEFERSTORE  ; compile DEFERSTORE
            .word   COMMA,EXIT

;-------------------------------------------------------------------------------
    .IFDEF VOCABULARY_SET
; WORDS SET for VOCABULARY, not ANS94 compliant,
;-------------------------------------------------------------------------------

VOCDOES     mHI2LO                  ;
ALSO        MOV #14,X               ;2 -- move up 7 words, first word in last
ALSOLOOP    SUB #2,X
            MOV CONTEXT(X),CONTEXT+2(X) ; X=src < Y=dst copy W bytes beginning with the end
            JNZ ALSOLOOP
            MOV TOS,CONTEXT(X)      ;3  copy word-set BODY  --> first cell of CONTEXT
            MOV #DROPEXIT,PC

            FORTHWORD "WORDSET"
;X VOCABULARY       -- create a new word_set
VOCABULARY  CALL #HEADER            ; W = CFA
            ADD #4,W                ;
            MOV #DODOES,-4(W)       ; replace CALL rDOCON of CREATE by CALL rDODOES
            MOV #VOCDOES,-2(W)      ; replace PFA by the execution address
    .IF THREADS=1                   ;
            MOV #0,0(W)             ; W = BODY, init thread with 0
            ADD #2,W                ;
    .ELSE                           ;
            MOV #THREADS,T          ; count
VOCABULOOP  MOV #0,0(W)             ; init threads area with 0
            ADD #2,W                ;
            SUB #1,T                ;
            JNZ VOCABULOOP          ;
    .ENDIF                          ;
            MOV &LASTVOC,0(W)       ; link LASTVOC
            MOV W,&LASTVOC          ;
            ADD #2,W                ; W = new DP
            JMP CREATE_NEXT         ; then NEXT

            FORTHWORD "DEFINITIONS"
;X DEFINITIONS  --      set last context vocabulary as entry for further defining words
            MOV &CONTEXT,&CURRENT   ;
            MOV @IP+,PC             ;

            FORTHWORD "ONLY"
;X ONLY     --      fill the context stack with 0 to access only the first word-set, ex.: FORTH ONLY
            MOV #16,X               ; clear 8 words in context stack
ONLY_LOOP   MOV #0,CONTEXT(X)       ;
            SUB #2,X                ;
            JNZ ONLY_LOOP           ;
            MOV @IP+,PC             ;

            FORTHWORD "PREVIOUS"
;X  PREVIOUS   --               pop first word-set out of context stack
PREVIOUS    MOV #8,W                ;1 move down 8 words, first with CONTEXT+2 addr, last with NULL_WORD one
            MOV #CONTEXT+2,X        ;2 X = org = CONTEXT+2, X-2 = dst = CONTEXT
            CMP #0,0(X)             ;3 [org] = 0 ?
            JZ PREVIOUSEND          ;2 to avoid scratch of the first CONTEXT cell by human mistake...
PREVIOUSLOO MOV @X+,-4(X)           ;4
            SUB #1,W                ;1 dec word
            JNZ PREVIOUSLOO         ;2 8~ loop * 8 = 64 ~
PREVIOUSEND MOV @IP+,PC             ;4

            FORTHWORD "FORTH"       ; add FORTH as first context word-set
            CALL rDODOES            ;
            .word   VOCDOES         ;
    .ENDIF ; VOCABULARY_SET

BODYFORTH   .word   lastforthword   ; BODY of FORTH
    .SWITCH THREADS
    .CASE   2
            .word   lastforthword1
    .CASE   4
            .word   lastforthword1
            .word   lastforthword2
            .word   lastforthword3
    .CASE   8
            .word   lastforthword1
            .word   lastforthword2
            .word   lastforthword3
            .word   lastforthword4
            .word   lastforthword5
            .word   lastforthword6
            .word   lastforthword7
    .CASE   16
            .word   lastforthword1
            .word   lastforthword2
            .word   lastforthword3
            .word   lastforthword4
            .word   lastforthword5
            .word   lastforthword6
            .word   lastforthword7
            .word   lastforthword8
            .word   lastforthword9
            .word   lastforthword10
            .word   lastforthword11
            .word   lastforthword12
            .word   lastforthword13
            .word   lastforthword14
            .word   lastforthword15
    .CASE   32
            .word   lastforthword1
            .word   lastforthword2
            .word   lastforthword3
            .word   lastforthword4
            .word   lastforthword5
            .word   lastforthword6
            .word   lastforthword7
            .word   lastforthword8
            .word   lastforthword9
            .word   lastforthword10
            .word   lastforthword11
            .word   lastforthword12
            .word   lastforthword13
            .word   lastforthword14
            .word   lastforthword15
            .word   lastforthword16
            .word   lastforthword17
            .word   lastforthword18
            .word   lastforthword19
            .word   lastforthword20
            .word   lastforthword21
            .word   lastforthword22
            .word   lastforthword23
            .word   lastforthword24
            .word   lastforthword25
            .word   lastforthword26
            .word   lastforthword27
            .word   lastforthword28
            .word   lastforthword29
            .word   lastforthword30
            .word   lastforthword31
    .ENDCASE
            .word   voclink
voclink     .set    $-2

    .IFDEF VOCABULARY_SET
            FORTHWORD "hidden"      ; cannot be found by FORTH interpreter because the string is not capitalized
hidden      CALL rDODOES            ;
            .word   VOCDOES         ;
    .ENDIF
BODYhidden  .word   lastasmword     ; BODY of hidden words
    .SWITCH THREADS
    .CASE   2
            .word   lastasmword1
    .CASE   4
            .word   lastasmword1
            .word   lastasmword2
            .word   lastasmword3
    .CASE   8
            .word   lastasmword1
            .word   lastasmword2
            .word   lastasmword3
            .word   lastasmword4
            .word   lastasmword5
            .word   lastasmword6
            .word   lastasmword7
    .CASE   16
            .word   lastasmword1
            .word   lastasmword2
            .word   lastasmword3
            .word   lastasmword4
            .word   lastasmword5
            .word   lastasmword6
            .word   lastasmword7
            .word   lastasmword8
            .word   lastasmword9
            .word   lastasmword10
            .word   lastasmword11
            .word   lastasmword12
            .word   lastasmword13
            .word   lastasmword14
            .word   lastasmword15
    .CASE   32
            .word   lastasmword1
            .word   lastasmword2
            .word   lastasmword3
            .word   lastasmword4
            .word   lastasmword5
            .word   lastasmword6
            .word   lastasmword7
            .word   lastasmword8
            .word   lastasmword9
            .word   lastasmword10
            .word   lastasmword11
            .word   lastasmword12
            .word   lastasmword13
            .word   lastasmword14
            .word   lastasmword15
            .word   lastasmword16
            .word   lastasmword17
            .word   lastasmword18
            .word   lastasmword19
            .word   lastasmword20
            .word   lastasmword21
            .word   lastasmword22
            .word   lastasmword23
            .word   lastasmword24
            .word   lastasmword25
            .word   lastasmword26
            .word   lastasmword27
            .word   lastasmword28
            .word   lastasmword29
            .word   lastasmword30
            .word   lastasmword31
    .ENDCASE
            .word   voclink
voclink     .set    $-2

;-------------------------------------------------------------------------------
; ASSEMBLER environment definitions
;-------------------------------------------------------------------------------

            FORTHWORD "CODE"        ;
ASMCODE     CALL #HEADER            ;
    .IFDEF VOCABULARY_SET           ; if VOCABULARY_SET
            JMP hidden              ;
    .ELSE                           ; if no VOCABULARY_SET 
hidden      MOV #BODYFORTH,&CONTEXT+2 ; add hidden word set in CONTEXT stack
            MOV #BODYhidden,&CONTEXT;
            MOV @IP+,PC             ;

PREVIOUS    MOV #BODYFORTH,&CONTEXT ; remove hidden word set from CONTEXT stack
            MOV #0,&CONTEXT+2       ;
            MOV @IP+,PC             ;
    .ENDIF                          ;

            FORTHWORD "CODENNM"     ; CODENoNaMe is the assembly counterpart of :NONAME
            PUSH #hidden            ; define HEADERLESS return
            JMP HEADERLESS          ;

; HDNCODE (hidden CODE) is used to define a CODE definition which must not to be executed by FORTH interpreter
; i.e. typically the case of an assembler definition called by CALL and ended by RET, or an interrupt routine.
; HDNCODE definitions are only usable in ASSEMBLY mode.
            FORTHWORD "HDNCODE"
            PUSH &CURRENT           ; save CURRENT word-set
            MOV #BODYhidden,&CURRENT; select hidden word set as CURRENT
            CALL #HEADER            ;
            MOV @RSP+,&CURRENT      ; restore CURRENT word_set
            JMP hidden              ; to add ASSEMBLER in CONTEXT stack

            FORTHWORD "ENDCODE"
ENDCODE     mCOLON                  ;
            .word QREVEAL,PREVIOUS  ;
            .word EXIT              ;

; here are 3 words used to switch FORTH <--> ASSEMBLER
; these 3 words are only used in interpreting mode

; COLON --      compile DOCOL, remove ASSEMBLER from CONTEXT stack, switch to compilation state
            asmword "COLON"
            MOV &DP,W               ;
        .SWITCH DTC                 ;
        .CASE 1                     ;
            MOV #DOCOL,0(W)         ; compile CALL R4 = rDOCOL ([rDOCOL] = XDOCOL)
            ADD #2,&DP              ;
        .CASE 2                     ;
            MOV #120Dh,0(W)         ; compile PUSH IP
            MOV #DOCOL,2(W)         ; compile CALL R4 = rDOCOL
            ADD #4,&DP              ;
        .CASE 3 ; inlined DOCOL     ;
            MOV #120Dh,0(W)         ; compile PUSH IP       ;3
LO2HI3      MOV #400Dh,2(W)         ; compile MOV PC,IP     ;1
            MOV #522Dh,4(W)         ; compile ADD #4,IP     ;1
            MOV #4D30h,6(W)         ; compile MOV @IP+,PC   ;4
            ADD #8,&DP              ;
        .ENDCASE ; DTC              ;
LO2HI12     MOV #-1,&STATE          ; enter in compile state
            JMP PREVIOUS            ; to remove ASSEMBLER from CONTEXT stack

; LO2HI --       same as COLON, minus save IP
            asmword "LO2HI"
        .SWITCH DTC
        .CASE 1                     ; compile 2 words
            MOV &DP,W               ;
            MOV #12B0h,0(W)         ; compile CALL #EXIT, 2 words  4+6=10~
            MOV #EXIT,2(W)          ;
            ADD #4,&DP              ;
            JMP LO2HI12             ;
        .CASE 2                     ; CASE 2 : compile 1 word
            MOV &DP,W               ;
            MOV #DOCOL,0(W)         ; compile CALL R4 = rDOCOL
            ADD #2,&DP              ;
            JMP LO2HI12             ;
        .ELSECASE                   ; CASE 3 : compile 3 words
            SUB #2,&DP              ; to skip PUSH IP
            MOV &DP,W               ;
            JMP LO2HI3              ;
        .ENDCASE                    ;

; HI2LO --       immediate, switch to low level, set interpretation state, add ASSEMBLER to CONTEXT
            FORTHWORDIMM "HI2LO"    ;
            ADD #2,&DP              ; HERE+2
            MOV &DP,W               ; W = HERE+2
            MOV W,-2(W)             ; compile HERE+2 to HERE
            MOV #0,&STATE           ; LEFTBRACKET
            JMP hidden              ; to add ASSEMBLER in CONTEXT stack

;-------------------------------------------------------------------------------
; FASTFORTH environment management: RST_SET RST_RET MARKER
;-------------------------------------------------------------------------------

            FORTHWORD "RST_SET"     ; define actual environment as new RESET environment, does same as MARKER
RST_SET     MOV #RAM_ENV,X          ; org = RAM value (DP first)
            MOV #RST_ENV,W          ; dst = FRAM value (RST_DP first), see \inc\ThingsInFirst.inc
            PUSH #NEXT              ; RET of ENV_COPY = NEXT = addr of MOV @IP+,PC
;-----------------------------------;
ENV_COPY    MOV #ENV_LEN/2,T        ; words count for basic environment: DP,LASTVOC,CURRENT,CONTEXT(0)
;-----------------------------------;
MOV_TW_X2W  MOV @X+,0(W)            ; MOV T Words from X to W
            ADD #2,W                ; 1 post increment dst
            SUB #1,T                ; 1 words count -1
            JNZ MOV_TW_X2W          ; 2
            MOV @RSP+,PC            ;

            FORTHWORD "RST_RET"     ; return_to_previous RST environment
RST_RET     MOV #RST_ENV,X          ; org = FRAM value (first RST_DP), see \inc\ThingsInFirst.inc
            MOV #RAM_ENV,W          ; dst = RAM value (first DP)
            MOV @X,S                ; S = DP FRAM value, used below for comparaison with NFAs
            CALL #ENV_COPY          ; copy T words of environment from X to W
            MOV &LASTVOC,W          ; W = LASTVOC in RAM
    .IF THREADS=1                   ;    mono thread word-set
MARKALLVOC  MOV W,Y                 ;1   W=VLK   Y = VLK
MRKWORDLOOP         MOV -2(Y),Y     ;  3 W=VLK   Y = [THD0] then [LFA] = NFA
                    CMP Y,S         ;  1 Y=NFA   S = DP
                    JNC MRKWORDLOOP ;  2 loop back if DP < NFA
UPDATETHRD0     MOV Y,-2(W)         ; 3  W=VLK   -2(W)=THD0   Y=NFA   refresh thread0 with good NFA
    .ELSE                           ;    multi threads word-set = mono + (6 cycles * (n-1)threads)
MARKALLVOC  MOV #THREADS,T          ;2   S=DP     T=THDcnt (THreaDs count), VLK = THD_n+1 !!!
            MOV W,X                 ;1   W = VLK
MRKTHRDLOOP     MOV X,Y             ; 1
                SUB #2,X            ; 1  X = THD_n, THD_n-1, ... THD_0
MRKWORDLOOP         MOV -2(Y),Y     ;  3 Y = [THD_x] then [LFA] = NFA
                    CMP Y,S         ;  1 Y = NFA   S = DP
                    JNC MRKWORDLOOP ;  2 loop back if DP < NFA
UPDATETHRD_X    MOV Y,0(X)          ; 3  Y=NFA     X=THD_x   refresh thread with good NFA
                SUB #1,T            ; 1  T=THDcnt-1
                JNZ MRKTHRDLOOP     ; 2
    .ENDIF                          ;
            MOV @W,W                ;2   W=[VLK] = VLK-1
            CMP #0,W                ;1   end of vocs ?
            JNZ MARKALLVOC          ;2   no : loopback
            MOV @IP+,PC             ;

; https://forth-standard.org/standard/core/MARKER
;--------------------------------
; MARKER name Execution: ( -- )
;Restore all dictionary allocation and search order pointers to the state they had just prior to the
;definition of name. Remove the definition of name and all subsequent definitions. Restoration
;of any structures still existing that could refer to deleted definitions or deallocated data space is
;not necessarily provided. No other contextual information such as numeric base is affected.

MARKER_DOES                         ; execution part of MARKER definition
            mHI2LO                  ; -- BODY
            MOV TOS,X               ;           X = ENV_COPY org (first : BODY = MARKER_DP)
            MOV #RST_ENV,W          ;           W = ENV_COPY dst (first : RST_DP), see \inc\ThingsInFirst.inc
            CALL #ENV_COPY          ;           restore previous FORTH environment from FRAM_MARKER to FRAM_RST
            MOV @PSP+,TOS           ; --
            MOV @RSP+,IP            ;
            JMP RST_RET             ;           which restores FORTH environment from FRAM to RAM

            FORTHWORD "MARKER"      ; definition part       
;( MARKER "<spaces>name" -- )
;Skip leading space delimiters. Parse name delimited by a space. Create a definition for name
;with the execution semantics defined above.
;-----------------------------------------------------------------------------------
; before that, if "name" is already defined executes its DOES> part if present :
; MARKER 'name' does: [DEFINED] 'name' [IF] execute 'name' [THEN] define 'name'
; if the MARKER definition is already defined in the kernel, discard downloading
;-----------------------------------------------------------------------------------
; furthermore, in assembly mode, MARKER definition does same as CREATE definition :
; MARKER TRUC | CREATE TRUC
;
; 4 ALLOT
;
; CODE MACHIN
;   MOV #TRUC,X
;   MOV @X+,&EDE
;   MOV Y,0(X)
;   ...  
; ENDCODE
;-----------------------------------------------------------------------------------
            PUSH &TOIN              ; --                save >IN
            mCOLON                  ;
            .word BL_WORD,FIND      ; -- c-addr|xt flag
            .word QFBRAN,NO_MARKER  ; -- c-addr         if MARKER not found
            .word DUP,FETCH         ; -- xt CFA_CODE
            .word LIT,DODOES,MINUS  ; -- xt flag        CFA_CODE = DODOES ?
            .word QFBRAN,MARKER_EXEC; -- xt             if yes execute it
            .word XSQUOTE           ;                   else
            .word 0                 ; -- xt addr 0          defines a null string (word aligned)..
            .word QABORT_YES        ;                       ..to discard downloading without display
MARKER_EXEC .word EXECUTE           ; --
            .word MARKER_NEXT       ; 
NO_MARKER   mHI2LO                  ; -- c-addr
            MOV @PSP+,TOS           ; --
MARKER_NEXT MOV @RSP+,IP            ; 
            MOV @RSP+,&TOIN         ; restore >IN for HEADER use
            CALL #HEADER            ; W = CFA, Y = NFA
            ADD #4,W                ; W = BODY = ENV_COPY dst
            MOV #DODOES,-4(W)       ; CFA = CALL rDODOES
            MOV #MARKER_DOES,-2(W)  ; PFA = MARKER_DOES
            SUB #2,Y                ; Y = NFA-2 = LFA = DP value to be restored
            MOV #RAM_ENV,X          ;   X = ENV_COPY org in RAM
            MOV Y,0(X)              ;   save first LFA in RAM as previous DP
            CALL #ENV_COPY          ;   MOV T Words from X to W, copy RAM environment --> FRAM MARKER
            MOV W,&DP               ; update DP
            JMP QREVEAL_OK          ; then NEXT

;-------------------------------------------------------------------------------
; FASTFORTH INIT environment : SYS, COLD, deep_COLD, RESET, deep_RESET, WARM
;-------------------------------------------------------------------------------

;-----------------------------------;
INIT_SYS                            ; common QABORT|PUC subroutine
;-----------------------------------;
            MOV @RSP+,IP            ; init IP with the CALLER next address
;-----------------------------------;
            CALL &SOFT_APP          ; default SOFT_APP = INIT_SOFT = RET_ADR, value set by DEEP_RESET.
;-----------------------------------;
            MOV #PUC_SYS_ORG,X      ; FRAM INFO         FRAM MAIN
;-----------------------------------; ---------         ---------
            MOV @X+,&PFAACCEPT      ; BODYACCEPT    --> PFAACCEPT
            MOV @X+,&PFAEMIT        ; BODYEMIT      --> PFAEMIT
            MOV @X+,&PFAKEY         ; BODYKEY       --> PFAKEY
            MOV @X+,&CIB_ORG        ; TIB_ORG       --> CIB_ORG             TIB = Terminal Input Buffer, CIB = Current Input Buffer
;-----------------------------------;
;                                   ; FRAM INFO         REG|RAM
;-----------------------------------; ---------         -------
            MOV @X+,&BASEADR        ; INIT_BASE     --> RAM BASE            init decimal base
            MOV @X+,&LSP            ; INIT_LSTACK   --> RAM LSP             init software Leave Stack Pointer
            MOV #0,&STATE           ; 0             --> RAM STATE           init interpretation state
            MOV @X+,rDOVAR          ; RFROM         --> R7=rDOVAR
            MOV @X+,rDOCON          ; XDOCON        --> R6=rDOCON
            MOV @X+,rDODOES         ; XDODOES       --> R5=rDODOES
            MOV @X+,rDOCOL          ; XDOCOL/EXIT/0 --> R4=rDOCOL           (DTC=1/2/3)
            BIC #UF123,SR           ; clear User Flags 1 2 3
;-----------------------------------;
; SELECT COLD|RESET                 ;
;-----------------------------------;
            SXT TOS                 ; to display -n SYS with FORTH numbers convention
            CMP.B #0,TOS            ;
            JGE RST_RET             ; if TOS >= 0   (USERSYS|RSTSYSIV)
            BIT.B #1,TOS            ;
            JNZ RST_RET             ; if TOS < 0 and odd value (USERSYS)
;-----------------------------------;
; PUC 7 : DEEP COLD|RESET           ; if TOS < 0 and even value (USERSYS)
;-----------------------------------;
;   INIT SIGNATURES AREA            ;
;-----------------------------------;
            MOV #16,X               ; max known SIGNATURES length = 12 bytes
SIGNATLOOP  SUB #2,X                ;
            MOV #-1,SIGNATURES(X)   ; reset signatures; WARNING ! DON'T CHANGE IMMEDIATE VALUE !
            JNZ SIGNATLOOP          ;
;-----------------------------------;
;   INIT INT VECTORS                ; X = 0 ;-)
;-----------------------------------;
            MOV #RESET,-2(X)        ; write RESET at addr X-2 = FFFEh
INIVECLOOP  SUB #2,X                ;
            MOV #COLD,-2(X)         ; -2(X) = FFFCh first
            CMP #VECT_ORG+2,X       ; all vectors are initialised to execute COLD routine
            JNZ INIVECLOOP          ;
;-----------------------------------;
            MOV #DEEP_ORG,X         ; DEEP_ORG values are in FRAM INFO, see \inc\ThingsInFirst.inc
;-----------------------------------;
;   INIT Terminal Int vector        ;
;-----------------------------------;
            MOV @X+,&TERM_VEC       ; TERMINAL_INT  as default vector --> FRAM TERM_VEC
;-----------------------------------;
;   INIT FRAM RST values from DEEP  ; init {COLD,ABORT,SOFT,HARD,BACKGRND}_APP and RST_{DP,LASTVOC,CURRENT,CONTEXT,0}
;-----------------------------------;
            MOV #RST_LEN/2,T        ; T = words count
            MOV #RST_ORG,W          ; X = org, W = dst
            CALL #MOV_TW_X2W        ;
    .IFDEF SD_CARD_LOADER           ;
            MOV #WARM,&PUCNEXT      ; removes XBOOT from PUC chain to avoid a bootstrap after DEEP COLD.
    .ENDIF                          ;
;-----------------------------------;
;   ENDOF DEEP_RESET                ;
;-----------------------------------;
            JMP RST_RET             ; then return to QABORT_YES|PUCNEXT
;-----------------------------------;

;***********************************;
COLD                                ; <--- USER_NMI_VEC <----------------------------- <RESET>|<RESET+SW1>
;***********************************;
; because pin RST acts as NMI, <RESET> is redirected here to execute SYS.
            MOV.B #4,TOS            ; TOS = SYSRSTIV value for <RESET>
    .IFNDEF NO_SWITCHES             ; see ThingsInFirst.inc
            BIT.B #SW1,&SW1_IN      ; <SW1> pressed ?
            JNZ SYS                 ; no
            SUB.B #8,TOS            ; set negative value to force DEEP_RESET
    .ENDIF                          ;
            JMP SYS                 ;
;***********************************;

; SYS is an enhanced COLD|WARM system
; a signed byte (displayed by WARM) 
; allows any form of restart
; without b  --> WARM               ; WARM displays #0
; b = 0      --> WARM               ; WARM displays #0       +--- end of SYS_failures_area
; b odd  > 0 --> WARM               ; WARM displays #b       v
; b even > 0 --> RESET (PUC)        ;         "        with $3E < b <= $7E
; b even < 0 --> deep RESET (PUC)   ;         "        with $80 <= b < $100
;-----------------------------------;
            FORTHWORD "SYS"         ; [b] --
;-----------------------------------;
SYS         CALL &STOP_APP          ; default STOP_APP = INIT_STOP
SYS_NO_STOP CMP.B #0,TOS            ;
            JZ TOS2WARM             ; b = 0
            BIT.B #1,TOS            ;
            JZ TOS2SWBOR            ; b odd value
TOS2WARM    CALL #INIT_SYS          ; b even value
FWARM       .word WARM              ; no return
TOS2SWBOR   MOV.B TOS,&USERSYS      ;
SOFTWARE_BOR MOV #0A504h,&PMMCTL0   ; --------------------------------------------->+
;***********************************;                                               v  
RESET                               ; <--- RST vector <--- PUC <--- POR <--- BOR <--+<--- SYS_failures 
;***********************************;
; PUC 1: NMI <- RST, stops WDT_RESET;
;-----------------------------------;
            BIS #1,&SFRRPCR         ; pin RST becomes pin NMI with rising edge
            BIS #10h,&SFRIE1        ; enable NMI interrupt (USER_NMI_VEC --> COLD)
            MOV #5A80h,&WDTCTL      ; disable WDT RESET
;-----------------------------------;
; PUC 2: INIT STACK                 ;
;-----------------------------------;
            MOV #PSTACK,PSP         ; init parameter stack (mandatory for FR57xx family)
            MOV #RSTACK,RSP         ; init return stack
;-----------------------------------;
; PUC 3: I/O, RAM, RTC, CS, SYS initialisation limited to FastForth usage.
;          All unused I/O are set as input with pullup resistor.
;-----------------------------------;
        .include "SelTargetInit.asm" ; include target specific init code
;-----------------------------------;
; PUC 4: init RAM to 0              ;
;-----------------------------------;
            MOV #RAM_LEN,X          ; 2
INITRAMLOOP SUB #2,X                ; 1
            MOV #0,RAM_ORG(X)       ; 3
            JNZ INITRAMLOOP         ; 2     6 cycles loop !
    .IFDEF SYS_NMI
;-----------------------------------;
; PUC 5: SYSUNIV_SYSSNIV_SYSRSTIV   ;
;-----------------------------------;
            MOV &SYSUNIV,X          ; 0 --> SYSUNIV --> X   (%0000_0000_000U_UUU0) (up to 16 failures)
            RLAM #4,X               ; make room for SYSSNIV (%0000_000U_UUU0_0000)
            BIS &SYSSNIV,X          ; 0 --> SYSSNIV --> X   (%0000_000U_UUUN_NNN0) (up to 16 failures)
            RLAM #4,X               ; make room for SYSRSTIV(%000U_UUUN_NNN0_0000)
            RLAM #3,X               ;                       (%UUUU_NNNN_0000_0000)
            BIS &SYSRSTIV,X         ; 0 --> SYSRSTIV --> X  (%UUUU_NNNN_0RRR_RRR0) (up to 32 SYS failures + 32 USER_SYS)
    .ELSE
;-----------------------------------;
; PUC 5: get SYSRSTIV               ;
;-----------------------------------;
            MOV &SYSRSTIV,X         ; X <-- SYSRSTIV <-- 0
    .ENDIF
            MOV.B &USERSYS,TOS      ; TOS = USERSYS (FRAM) = TOS2COLD TOS value
            MOV.B #0,&USERSYS       ; clear USERSYS
            BIT.B #-1,TOS           ; we favor the soft over the hard.
            JNZ PUC6                ; if TOS <> 0, keep USERSYS value
            MOV X,TOS               ; else keep SYSRSTIV value
;-----------------------------------;
; PUC 6: START FORTH engine         ;
;-----------------------------------;
PUC6        CALL #INIT_SYS          ; common part of QABORT_YES|PUC
PUCNEXT     .word WARM              ; no return. May be replaced by XBOOT by BOOT ;-)
;-----------------------------------;

    .include "forthMSP430FR_ASM.asm"
    .IFDEF SD_CARD_LOADER
;===============================================================================
; SD CARD KERNEL OPTIONS
;===============================================================================
        .include "forthMSP430FR_SD_LOAD.asm"    ; SD LOAD driver
        .IFDEF SD_CARD_READ_WRITE
            .include "forthMSP430FR_SD_RW.asm"  ; SD Create/Read/Write/Del driver
        .ENDIF
    .ENDIF
;===============================================================================
; ADDONS OPTIONS; if included here they will be protected against DEEP RST|COLD
;===============================================================================
    .IFDEF CORE_COMPLEMENT
;-------------------------------------------------------------------------------
; COMPLEMENT of definitions to pass ANS94 CORETEST
;-------------------------------------------------------------------------------
        FORTHWORD "{CORE_ANS}"  ; MARKER lure
        MOV @IP+,PC

        .include "ADDON/CORE_ANS.asm"
    .ENDIF
    .IFDEF UTILITY
;-------------------------------------------------------------------------------
; UTILITY WORDS
;-------------------------------------------------------------------------------
        FORTHWORD "{UTILITY}"   ; MARKER lure
        MOV @IP+,PC

        .include "ADDON/UTILITY.asm"
    .ENDIF

    .IFDEF FIXPOINT
;-------------------------------------------------------------------------------
; FIXED POINT basic OPERATORS
;-------------------------------------------------------------------------------
        FORTHWORD "{FIXPOINT}"  ; MARKER lure
        MOV @IP+,PC

        .include "ADDON/FIXPOINT.asm"
    .ENDIF

    .IFDEF DOUBLE
;-------------------------------------------------------------------------------
; DOUBLE word set
;-------------------------------------------------------------------------------
        FORTHWORD "{DOUBLE}"    ; MARKER lure
        MOV @IP+,PC

        .include "ADDON/DOUBLE.asm"
    .ENDIF

    .IFDEF SD_CARD_LOADER
        .IFDEF SD_TOOLS
;-------------------------------------------------------------------------------
; BASIC SD TOOLS
;-------------------------------------------------------------------------------
            FORTHWORD "{SD_TOOLS}"  ; MARKER lure
            MOV @IP+,PC

            .include "ADDON/SD_TOOLS.asm"
        .ENDIF
    .ENDIF
;-------------------------------------------------------------------------------
; ADD HERE YOUR CODE TO BE INCLUDED IN KERNEL and protected against Deep_RST
;vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
;           FORTHWORD "{YOUR_CODE}"  ; MARKER lure
;           MOV @IP+,PC
;           .include "ADDON/YOUR_CODE.asm"
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
; ADD HERE YOUR CODE TO BE INCLUDED IN KERNEL (protected against Deep_RST)
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
; RESOLVE ASSEMBLY pointers, init interrupt Vectors
;-------------------------------------------------------------------------------
    .include "ThingsInLast.asm"
