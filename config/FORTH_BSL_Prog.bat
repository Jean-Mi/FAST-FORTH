::
::@ECHO OFF

:: called by scite(tools(Prog 1:[target] with 3:FORTH_xxxx_BSL)|(Prog target CurrentSelection with 3:FORTH_xxxx_BSL))

:: %~dp0 = *.bat *.ttl *.properties subdirectory
:: %~dp0..\inc\ = declarations subdirectory
:: %~dp0..\MSP430-FORTH\ = FORTH source files subdirectory
:: %~dp0..\binaries\ = binaries subdirectory
:: %~n1.pat = scite($1).pat as pattern file for gema use
:: %~n1.txt = scite($1).txt as binary file assembled by scite(tools(assemble))
:: %~n2.f = scite ($3).f as FORTH_xxxx_BSL.f


::preprocess FORTH_xxxx_BSL.f
%~dp0..\prog\gema.exe -nobackup -line -t '-\r\n=\r\n' -f  %~dp0..\inc\%~n1.pat %~dp0..\MSP430-FORTH\%~n2.f %~dp0..\MSP430-FORTH\LAST.1ST
%~dp0..\prog\gema.exe -nobackup -line -t '-\r\n=\r\n' -f  %~dp0..\inc\%~n1.pat %~dp0..\MSP430-FORTH\LAST.1ST %~dp0..\MSP430-FORTH\LAST.4TH

::concatenate FORTH_xxxx_BSL.4TH + binary.txt --> binary.4th
copy %~dp0..\MSP430-FORTH\LAST.4TH + %~dp0..\binaries\%~n1.txt %~dp0..\binaries\%~n1.4TH

::DownloadF
taskkill /F /IM ttermpro.exe 1> NUL 2>&1

:: select %deviceid% for device test in sendfile.ttl
set target=%~n1
call %~dp0Select.bat SelectDeviceId %%target%%

::Win64F
"C:\Program Files\teraterm\ttpmacro.exe" /V %~dp0SendFile.ttl %~dp0..\binaries\%~n1.4TH /C ECHO %deviceid% 1> NUL 2>&1
IF NOT ERRORLEVEL 1 GOTO EndF

::Win32F
"C:\Program Files (x86)\teraterm\ttpmacro.exe" /V %~dp0SendFile.ttl %~dp0..\binaries\%~n1.4TH /C ECHO %deviceid%

:EndF

exit
