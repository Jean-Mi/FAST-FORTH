
@goto %1


:SelectTarget
:: called by PreprocessSourceFile.bat, SendSourceFileToTarget.bat and CopySourceFileToTarget_SD_Card.bat
:: just before calling Preprocess.bat,             SendSource.bat and           CopyToTarget_SD_Card.bat

::%1 = "SelectTemplate"
::%2 = file.f name

@echo select your target:
@echo 1  MSP_EXP430FR5739
@echo 2  MSP_EXP430FR5969
@echo 3  MSP_EXP430FR5994
@echo 4  MSP_EXP430FR6989
@echo 5  MSP_EXP430FR4133
@echo 6  MSP_EXP430FR2433
@echo 7  CHIPSTICK_FR2433
@echo 8  MSP_EXP430FR2355
@echo 9  LP_MSP430FR2476

@echo 10 MY_SP430FR5738
@echo 11 MY_SP430FR5738_1
@echo 12 MY_SP430FR5738_2
@echo 13 MY_SP430FR5948
@echo 14 MY_SP430FR5948_1
@echo 15 JMJ_BOX_2022_07_28
@echo 16 PA8_PA_MSP430
@echo 17 PA_PA_MSP430
@echo 18 PA_Core_MSP430
@echo 19 FP_MAIN_2024_10_14
@echo 20 FP_XLR_2018_03_26
@echo 21 FP_XLR_2024_10_14
@echo 22 JMJ_BOX_2355_2024_03_21
@echo 23 JMJ_BOX_5738_2024_02_21
@set /p choice=your choice: 

@if %choice% == 1    set target=MSP_EXP430FR5739
@if %choice% == 2    set target=MSP_EXP430FR5969
@if %choice% == 3    set target=MSP_EXP430FR5994
@if %choice% == 4    set target=MSP_EXP430FR6989
@if %choice% == 5    set target=MSP_EXP430FR4133
@if %choice% == 6    set target=MSP_EXP430FR2433
@if %choice% == 7    set target=CHIPSTICK_FR2433
@if %choice% == 8    set target=MSP_EXP430FR2355
@if %choice% == 9    set target=LP_MSP430FR2476
@if %choice% == 10   set target=MY_MSP430FR5738
@if %choice% == 11   set target=MY_MSP430FR5738_1 
@if %choice% == 12   set target=MY_MSP430FR5738_2
@if %choice% == 13   set target=MY_MSP430FR5948
@if %choice% == 14   set target=MY_MSP430FR5948_1 
@if %choice% == 15   set target=JMJ_BOX_2022_07_28
@if %choice% == 16   set target=PA8_PA_MSP430 
@if %choice% == 17   set target=PA_PA_MSP430 
@if %choice% == 18   set target=PA_Core_MSP430 
@if %choice% == 19   set target=FP_MAIN_2024_10_14 
@if %choice% == 20   set target=FP_XLR_2018_03_26 
@if %choice% == 21   set target=FP_XLR_2024_10_14 
@if %choice% == 22   set target=JMJ_BOX_2355_2024_03_21 
@if %choice% == 23   set target=JMJ_BOX_5738_2024_02_21 

@exit /b

:SelectDevice
:: fonction called by FET_prog.bat and preprocess.bat with variable %target%
@if /I %target:~0,16%  == MSP_EXP430FR5739 set device=MSP430FR5739
@if /I %target:~0,16%  == MSP_EXP430FR5969 set device=MSP430FR5969
@if /I %target:~0,16%  == MSP_EXP430FR5994 set device=MSP430FR5994
@if /I %target:~0,16%  == MSP_EXP430FR6989 set device=MSP430FR6989
@if /I %target:~0,16%  == MSP_EXP430FR4133 set device=MSP430FR4133
@if /I %target:~0,16%  == MSP_EXP430FR2433 set device=MSP430FR2433
@if /I %target:~0,16%  == CHIPSTICK_FR2433 set device=MSP430FR2433
@if /I %target:~0,16%  == MSP_EXP430FR2355 set device=MSP430FR2355
@if /I %target:~0,15%  == LP_MSP430FR2476  set device=MSP430FR2476

@if /I %target:~0,15%  == MY_MSP430FR5738       set device=MSP430FR5738
@if /I %target:~0,15%  == MY_MSP430FR5948       set device=MSP430FR5948
@if /I %target:~0,13%  == PA8_PA_MSP430         set device=MSP430FR5738
@if /I %target:~0,17%  == PA_PA_MSP430_2021     set device=MSP430FR2355
@if /I %target:~0,19%  == PA_CORE_MSP430_2021   set device=MSP430FR2355
@if /I %target:~0,12%  == PA_PA_MSP430          set device=MSP430FR5738
@if /I %target:~0,14%  == PA_CORE_MSP430        set device=MSP430FR5948
@if /I %target:~0,12%  == JMJ_BOX_2021          set device=MSP430FR2355
@if /I %target:~0,18%  == JMJ_BOX_2018_10_29    set device=MSP430FR5738
@if /I %target:~0,18%  == JMJ_BOX_2022_07_28    set device=MSP430FR5738
@if /I %target:~0,23%  == JMJ_BOX_2355_2024_03_21    set device=MSP430FR2355
@if /I %target:~0,23%  == JMJ_BOX_5738_2024_02_21    set device=MSP430FR5738
@if /I %target:~0,18%  == FP_MAIN_2024_10_14    set device=MSP430FR2355
@if /I %target:~0,17%  == FP_XLR_2018_03_26     set device=MSP430FR5738
@if /I %target:~0,17%  == FP_XLR_2024_10_14     set device=MSP430FR2355

echo %device%

::pause

@exit /b

:SelectDeviceId
:: fonction called by SendSource.bat

::echo %target%
@if /I %target:~0,16%  == MSP_EXP430FR5739 set deviceid=$8103
@if /I %target:~0,16%  == MSP_EXP430FR5969 set deviceid=$8169
@if /I %target:~0,16%  == MSP_EXP430FR5994 set deviceid=$82A1
@if /I %target:~0,16%  == MSP_EXP430FR6989 set deviceid=$81A8
@if /I %target:~0,16%  == MSP_EXP430FR4133 set deviceid=$81F0
@if /I %target:~0,16%  == MSP_EXP430FR2433 set deviceid=$8240
@if /I %target:~0,16%  == CHIPSTICK_FR2433 set deviceid=$8240
@if /I %target:~0,16%  == MSP_EXP430FR2355 set deviceid=$830C
@if /I %target:~0,15%  == LP_MSP430FR2476  set deviceid=$832A

@if /I %target:~0,15%  == MY_MSP430FR5738       set deviceid=$8102
@if /I %target:~0,15%  == MY_MSP430FR5948       set deviceid=$8160
@if /I %target:~0,13%  == PA8_PA_MSP430         set deviceid=$8102
@if /I %target:~0,17%  == PA_PA_MSP430_2021     set deviceid=$830C
@if /I %target:~0,19%  == PA_CORE_MSP430_2021   set deviceid=$830C
@if /I %target:~0,12%  == PA_PA_MSP430          set deviceid=$8102
@if /I %target:~0,14%  == PA_CORE_MSP430        set deviceid=$8160
@if /I %target:~0,12%  == JMJ_BOX_2021          set deviceid=$830C
@if /I %target:~0,18%  == JMJ_BOX_2018_10_29    set deviceid=$8102
@if /I %target:~0,18%  == JMJ_BOX_2022_07_28    set deviceid=$8102
@if /I %target:~0,23%  == JMJ_BOX_2355_2024_03_21    set deviceid=$830C
@if /I %target:~0,23%  == JMJ_BOX_5738_2024_02_21    set deviceid=$8102
@if /I %target:~0,18%  == FP_MAIN_2024_10_14    set deviceid=$830C
@if /I %target:~0,17%  == FP_XLR_2018_03_26     set deviceid=$8102
@if /I %target:~0,17%  == FP_XLR_2024_10_14     set deviceid=$830C

::echo %deviceid%
::%1 = "SelectDevice"
::%2 = file.pat name

@exit /b


:SelectPortCOM
:: fonction called by BSL_prog.bat
@set /p number=select your Port: COM
@set PortCOM=COM%number%

@exit /b


:SelectBridge
@echo select your bridge:
@echo 1  USB to UART
@echo 2  WIFI
@set /p choice=your choice: 

@if %choice% == 1    set bridge="C/ ECHO"
@if %choice% == 2    set bridge="FastForth:3000 HALF"

@exit /b


