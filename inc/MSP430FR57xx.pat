@set-syntax{C;\;}!  tell GEMA to replace default Comment separator '!' by ';'

;MSP430FR57xx.pat

;=========================
; SYS events
;=========================
;Brownout                   $02
;RSTIFG RST/NMI             $04
;PMMSWBOR software BOR      $06
;LPMx.5 wake up             $08
;Security violation         $0A
;SVSLIFG SVSL event         $0C
;SVSHIFG SVSH event         $0E         (same as 02)
;Reserved                   $10
;Reserved                   $12
;PMMSWPOR software POR      $14
;WDTIFG watchdog time-out   $16
;WDT password violation     $18
;FRCTL password violation   $1A
;FRAM double bit error      $1C
;Peripheral area fetch      $1E
;PMMPW password violation   $20
;MPUPW password violation   $22
;CSPW password violation    $24
;MPUSEGIIFG info violation  $26
;MPUSEG1IFG mem 1 violation $28
;MPUSEG2IFG mem 2 violation $2A
;MPUSEG3IFG mem 3 violation $2C
;Reserved                   $2E to $3E

PWR_ON_EVENT=2;     Brownout
RST_EVENT=4;
SWBOR_EVENT=6;
LPMX.5_EVENT=8;
SECU_EVENT=\#10;
SVSL_EVENT=\#12; 
SVSH_EVENT=\#14;    same as 02
;Reserved
;Reserved
SWPOR_EVENT=\#20;
WDT_OUT_EVENT=\#22;
WDT_SECU_EVENT=\#24;
FRCTLPSW_EVENT=\#26;
FRAM_EVENT=\#28;
PERIFTCH_EVENT=\#30;
PMMPW_EVENT=\#32;
MPUPW_EVENT=\#34;
CSPW_EVENT=\#36;
MPUSEG_EVENT=\#38;
MPUSEG1_EVENT=\#40;
MPUSEG2_EVENT=\#42;
MPUSEG3_EVENT=\#44;
;Reserved #46 to #62

; ============================================
; FAST FORTH V4.2
; ============================================

; ----------------------------------------------
; FORTH RAM areas :
; ----------------------------------------------
LSTACK_SIZE=\#16; words
PSTACK_SIZE=\#48; words
RSTACK_SIZE=\#48; words
PAD_LEN=\#84; bytes
CIB_LEN=\#84; bytes
HOLD_SIZE=\#34; bytes
;
; ----------------------------------------------
; FastForth RAM memory map (>= 1k):
; ----------------------------------------------
LSP=\$1C00;             Leave-stack pointer, init by QUIT
LSATCK=\$1C00;          leave stack,      grow up
PSTACK=\$1C80;          parameter stack,  grow down
RSTACK=\$1CE0;          Return stack,     grow down
;
PAD_I2CADR=\$1CE0;      RX I2C address
PAD_I2CCNT=\$1CE2;      count of bytes
PAD_I2CEXC=\$1CE3;      count of exchanged bytes
PAD_ORG=\$1CE4;         user scratch pad buffer, 84 bytes, grow up
;
TIB_I2CADR=\$1D38;      TX I2C address 
TIB_I2CCNT=\$1D3A;      count of bytes
TIB_I2CEXC=\$1D3B;      count of exchanged bytes
TIB_ORG=\$1D3C;         Terminal input buffer, 84 bytes, grow up
;
HOLDS_ORG=\$1D90;       base address for HOLDS
HOLD_BASE=\$1DB2;       BASE HOLD area, grow down
;
HP=\$1DB2;              HOLD ptr
STATEADR=\$1DB4;        Interpreter state
BASEADR=\$1DB6;         base
SOURCE_LEN=\$1DB8;      len of input stream
SOURCE_ORG=\$1DBA;      adr of input stream
TOIN=\$1DBC;            >IN
;
DP=\$1DBE;              dictionary ptr
LASTVOC=\$1DC0;         keep VOC-LINK
CURRENT=\$1DC2;         CURRENT dictionnary ptr
CONTEXT=\$1DC4;         CONTEXT dictionnary space (8 + Null CELLS)
;
; ---------------------------------------
; RAM_ORG + $1D8 : may be shared between FORTH compiler and user application
; ---------------------------------------
LAST_NFA=\$1DD6;
LAST_THREAD=\$1DD8;
LAST_CFA=\$1DDA;
LAST_PSP=\$1DDC;
ASMBW1=\$1DDE;          3 backward labels
ASMBW2=\$1DE0;
ASMBW3=\$1DE2;
ASMFW1=\$1DE4;          3 forward labels
ASMFW2=\$1DE6;
ASMFW3=\$1DE8;
; ---------------------------------------
; RAM_ORG + $1EC RAM free 
; ---------------------------------------
;
