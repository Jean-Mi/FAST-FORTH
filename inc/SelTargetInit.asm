;-----------------------------------------------------------------------
; SelTargetIni.asm : minimum FORTH I/O init
;-----------------------------------------------------------------------
; reset state : Px{DIR,REN,SEL0,SEL1,SELC,IE,IFG,IV} = 0 ; Px{IN,OUT,IES} = ?

    .IFDEF MSP_EXP430FR5739
;-----------------------------------
        .include "MSP_EXP430FR5739.asm"
    .ENDIF
    .IFDEF MSP_EXP430FR5969
;-----------------------------------
        .include "MSP_EXP430FR5969.asm"
    .ENDIF
    .IFDEF MSP_EXP430FR5994
;-----------------------------------
        .include "MSP_EXP430FR5994.asm"
    .ENDIF
    .IFDEF MSP_EXP430FR6989
;-----------------------------------
    .INCLUDE "MSP_EXP430FR6989.asm"
    .ENDIF
    .IFDEF MSP_EXP430FR4133
;-----------------------------------
        .INCLUDE "MSP_EXP430FR4133.asm"
    .ENDIF
    .IFDEF MSP_EXP430FR2433
;-----------------------------------
        .include "MSP_EXP430FR2433.asm"
    .ENDIF
    .IFDEF MSP_EXP430FR2355
;-----------------------------------
        .include "MSP_EXP430FR2355.asm"
        .IFDEF UART_TERMINAL
            .IFDEF TERMINAL4WIRES
; RTS output is wired to the CTS input of UART2USB bridge
; configure RTS as output high (false) to disable RX TERM during start FORTH
            BIS.B #RTS,&P2DIR   ; RTS as output high
            BIC.B #RTS,&P2REN   ;
                .IFDEF TERMINAL5WIRES
; CTS input must be wired to the RTS output of UART2USB bridge
; configure CTS as input low (true) to avoid lock when CTS is not wired
            BIC.B #CTS,&HANDSHAKOUT   ; CTS input resistor is pulled down
                .ENDIF  ; TERMINAL5WIRES
            .ENDIF  ; TERMINAL4WIRES
        .ENDIF
    .ENDIF
    .IFDEF LP_MSP430FR2476
;-----------------------------------
    .include "LP_MSP430FR2476.asm"
    .ENDIF
    .IFDEF CHIPSTICK_FR2433
;-----------------------------------
    .include "CHIPSTICK_FR2433.asm"
    .ENDIF
    .IFDEF YOUR_TARGET
;-----------------------------------
; vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
; add here your target.asm item:
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    .ENDIF
    .IFDEF MY_MSP430FR5738
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF MY_MSP430FR5738_1
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF MY_MSP430FR5738_2
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF MY_MSP430FR5948
;-----------------------------------
        .include "MY_MSP430FR5948.asm"
    .ENDIF
    .IFDEF MY_MSP430FR5948_1
;-----------------------------------
        .include "MY_MSP430FR5948_1.asm"
    .ENDIF
    .IFDEF JMJ_BOX_2355_2024_03_21
;-----------------------------------
        .include "MSP_EXP430FR2355.asm"
    .ENDIF
    .IFDEF JMJ_BOX_5738_2024_02_21
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF JMJ_BOX_2018_10_29
;-----------------------------------
        .include "MY_MSP430FR5738.asm" 
    .ENDIF
    .IFDEF JMJ_BOX_2018_08
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF JMJ_BOX_GUILLAUME
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF JMJ_BOX_FAVRE
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF PA8_PA_MSP430
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF PA_PA_MSP430
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF PA_Core_MSP430
;-----------------------------------
        .include "MY_MSP430FR5948_1.asm"
    .ENDIF
    .IFDEF FP_MAIN_2021_04_09
;-----------------------------------
        .include "MSP_EXP430FR2355.asm"
; RTS output is wired to the CTS input of UART2USB bridge
; configure RTS as output high (false) to disable RX TERM during start FORTH
            BIS.B #RTS,&P4DIR       ; RTS as output high
            BIC.B #RTS,&P4REN       ;
    .ENDIF
    .IFDEF FP_MAIN_2024_10_14
;-----------------------------------
        .include "MSP_EXP430FR2355.asm"
; RTS output is wired to the CTS input of UART2USB bridge
; configure RTS as output high (false) to disable RX TERM during start FORTH
            BIS.B #RTS,&P4DIR       ; RTS as output high
            BIC.B #RTS,&P4REN       ;
    .ENDIF
    .IFDEF FP_XLR_2018_03_26
;-----------------------------------
        .include "MY_MSP430FR5738.asm"
    .ENDIF
    .IFDEF FP_XLR_2024_10_14
;-----------------------------------
        .include "MSP_EXP430FR2355.asm"
    .ENDIF
