; -*- coding: utf-8 -*-
; forthMSP430FR_SD_LOAD.asm

; Tested with MSP-EXP430FR5994 launchpad
; Copyright (C) <2019>  <J.M. THOORENS>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; ==================================;
; LOW LEVEL SD_SPI PRIMTIVESS       ;
; ==================================;

BytsPerSec      .equ 512

; all sectors are computed as logical, then physically translated at last time by RW_Sector_CMD

; in SPI mode CRC is not required, but CMD frame must be ended with a stop bit
; ==================================;
RW_Sector_CMD                       ;WX <=== CMD17 or CMD24 (read or write Sector CMD)
; ==================================;
    BIC.B   #CS_SD,&SD_CSOUT        ; set Chip Select low
; ----------------------------------;
;ComputePhysicalSector              ; input = logical sector...
; ----------------------------------;
    ADD     &BS_FirstSectorL,W      ;3
    ADDC    &BS_FirstSectorH,X      ;3
; ----------------------------------; ...output = physical sector
;Compute CMD                        ;
; ----------------------------------;
    MOV     #1,&SD_CMD_FRM          ;3 $(01 00 xx xx xx CMD) set stop bit in CMD frame
FAT32_CMD                           ;  FAT32 : CMD17/24 sector address
    MOV.B   W,&SD_CMD_FRM+1         ;3 $(01 ll xx xx xx CMD)
    SWPB    W                       ;1
    MOV.B   W,&SD_CMD_FRM+2         ;3 $(01 ll LL xx xx CMD)
    MOV.B   X,&SD_CMD_FRM+3         ;3 $(01 ll LL hh xx CMD)
    SWPB    X                       ;1
    MOV.B   X,&SD_CMD_FRM+4         ;3 $(01 ll LL hh HH CMD)
; ==================================;
WaitIdleBeforeSendCMD               ; <=== CMD41, CMD1, CMD16 (forthMSP430FR_SD_INIT.asm)
; ==================================;
    CALL #SPI_GET                   ;
    ADD.B   #1,W                    ; expected value = FFh <==> MISO = 1 = SPI idle state
    JNZ WaitIdleBeforeSendCMD       ; loop back if <> FFh
; ==================================; W = 0 = expected R1 response = ready, for CMD41,CMD16, CMD17, CMD24
sendCommand                         ;
; ==================================;
                                    ; input : SD_CMD_FRM : {CRC,byte_l,byte_L,byte_h,byte_H,CMD}
                                    ;         W = expected return value
                                    ; output  W is unchanged, flag Z is positionned
                                    ; reverts CMD bytes before send : $(CMD hh LL ll 00 CRC)
    MOV     #5,X                    ; X = SD_CMD_FRM_ptr AND countdown
; ----------------------------------;
Send_CMD_PUT                        ; performs little endian --> big endian conversion
; ----------------------------------;
    MOV.B   SD_CMD_FRM(X),&SD_TXBUF ;5
    CMP     #0,&SD_BRW              ;3 full speed ?
    JZ      FullSpeedSend           ;2 yes
Send_CMD_Loop                       ;  case of low speed during memCardInit
    BIT     #RX_SD,&SD_IFG          ;3
    JZ      Send_CMD_Loop           ;2
    CMP.B   #0,&SD_RXBUF            ;3 to clear UCRXIFG
FullSpeedSend                       ;
;   NOP                             ;0 NOPx adjusted to avoid SD error
    SUB.B   #1,X                    ;1
    JC      Send_CMD_PUT            ;2 U>= : don't skip SD_CMD_FRM(0) !
                                    ; host must provide height clock cycles to complete operation
                                    ; here X=255, so wait for CMD return expected value with PUT FFh 256 times
;    MOV     #4,X                    ; to pass made in PRC SD_Card init
;    MOV     #16,X                   ; to pass Transcend SD_Card init
;    MOV     #32,X                   ; to pass Panasonic SD_Card init
;    MOV     #64,X                   ; to pass SanDisk SD_Card init
; ----------------------------------;
Wait_Command_Response               ; expect W = return value during X = 255 times
; ----------------------------------;
    SUB     #1,X                    ;1
    JN      SPI_WAIT_RET            ;2 error on time out with flag Z = 0
    MOV.B   #-1,&SD_TXBUF           ;3 PUT FFh
    CMP     #0,&SD_BRW              ;3 full speed ?
    JZ      FullSpeedGET            ;2 yes
cardResp_Getloop                    ;  case of low speed during memCardInit (CMD0,CMD8,ACMD41,CMD16)
    BIT     #RX_SD,&SD_IFG          ;3
    JZ      cardResp_Getloop        ;2
FullSpeedGET                        ;
;    NOP                            ;  NOPx adjusted to avoid SD_error
    CMP.B   &SD_RXBUF,W             ;3 return value = ExpectedValue ?
    JNZ     Wait_Command_Response   ;2 16~ full speed loop
SPI_WAIT_RET                        ; flag Z = 1 <==> Returned value = expected value
    MOV @RSP+,PC                    ; W = expected value, unchanged
; ----------------------------------;

; ----------------------------------;
sendCommandIdleRet                  ; <=== CMD0, CMD8, CMD55: W = 1 = R1 expected response = idle (forthMSP430FR_SD_INIT.asm)
; ----------------------------------;
    MOV     #1,W                    ; expected R1 response (first byte of SPI R7) = 01h : idle state
    JMP     sendCommand             ;
; ----------------------------------;


; SPI_GET and SPI_PUT are adjusted for SD_CLK = MCLK
; PUT value must be a word or  byte:byte because little endian to big endian conversion

; ==================================;
SPI_GET                             ; PUT(FFh) one time, output : W = received byte, X = 0
; ==================================;
    MOV #1,X                        ;
; ==================================;
SPI_X_GET                           ; PUT(FFh) X times, output : W = last received byte, X = 0
; ==================================;
    MOV #-1,W                       ;
; ==================================;
SPI_PUT                             ; PUT(W) X times, output : W = last received byte, X = 0
; ==================================;
            SWPB W                  ;1
            MOV.B W,&SD_TXBUF       ;3 put W high byte then W low byte and so forth, that performs little to big endian conversion
            CMP #0,&SD_BRW          ;3 full speed ?
            JZ FullSpeedPut         ;2
SPI_PUTWAIT BIT #RX_SD,&SD_IFG      ;3
            JZ SPI_PUTWAIT          ;2
            CMP.B #0,&SD_RXBUF      ;3 reset RX flag
FullSpeedPut
;           NOP                     ;  NOPx adjusted to avoid SD error
            SUB #1,X                ;1
            JNZ SPI_PUT             ;2 12~ loop
SPI_PUT_END MOV.B &SD_RXBUF,W       ;3
            MOV @RSP+,PC            ;4
; ----------------------------------;

    .IFDEF SD_CARD_READ_WRITE
        ASMWORD "RD_SECT"           ; ReaD SECTor W=lo, X=Hi
    .ENDIF ; SD_CARD_READ_WRITE
; ==================================;
ReadSectorWX                        ; SWX read a logical sector
; ==================================;
    BIS     #1,S                    ; preset sd_read error
    MOV.B   #51h,&SD_CMD_FRM+5      ; CMD17 = READ_SINGLE_BLOCK
    CALL    #RW_Sector_CMD          ; which performs logical sector to physical sector then little endian to big endian conversion
    JNE     SD_CARD_ERROR           ; time out error if R1 <> 0
; ----------------------------------;
WaitFEhResponse                     ; wait for SD_Card response = FEh
; ----------------------------------;
    CALL #SPI_GET                   ;
    ADD.B   #2,W                    ;1 W = 0 ?
    JZ  ReadSectorFirstByte         ;2 yes, X = 0
    JNZ WaitFEhResponse             ;2
; ----------------------------------;
ReadSectorLoop                      ; get 512 bytes + CRC16 in SD_BUF
; ----------------------------------;
    MOV.B   &SD_RXBUF,SD_BUF-1(X)   ; 5
ReadSectorFirstByte                 ; W=0
    MOV.B   #-1,&SD_TXBUF           ; 3 put FF
    NOP                             ; 1 NOPx adjusted to avoid read SD_error
    ADD     #1,X                    ; 1
    CMP     #BytsPerSec+3,X         ; 2
    JNZ     ReadSectorLoop          ; 2 14 cycles loop read byte
; ----------------------------------;
ReadWriteHappyEnd                   ; <==== WriteSector
; ----------------------------------;
    BIC #3,S                        ; Clear read and write errors
    BIS.B #CS_SD,&SD_CSOUT          ; Chip Select high
    MOV @RSP+,PC                    ; W = 0 SR(Z) = 1
; ----------------------------------;

    .IFDEF SD_CARD_READ_WRITE
        ASMWORD "WR_SECT"           ; WRite SECTor W=lo, X=Hi
; ==================================;
WriteSectorWX                       ; write a logical sector
; ==================================;
    BIS     #2,S                    ; preset sd_write error
    MOV.B   #058h,SD_CMD_FRM+5      ; CMD24 = WRITE_SINGLE_BLOCK
    CALL    #RW_Sector_CMD          ; which performs logical sector to physical sector then little endian to big endian conversions
    JNE     SD_CARD_ERROR           ; ReturnError = 2
    MOV     #0FFFEh,W               ; PUT FFFEh as preamble requested for sector write
    MOV     #2,X                    ; to put 16 bits value
    CALL    #SPI_PUT                ; which performs little endian to big endian conversion
; ----------------------------------;
WriteSectorLoop                     ; 11 cycles loop write, starts with X = 0
; ----------------------------------;
    MOV.B   SD_BUF(X),&SD_TXBUF     ; 5
    NOP                             ; 1 NOPx adjusted to avoid write SD_error
    ADD     #1,X                    ; 1
    CMP     #BytsPerSec,X           ; 2
    JNZ     WriteSectorLoop         ; 2
; ----------------------------------;
;WriteSkipCRC16                     ; CRC16 not used in SPI mode
; ----------------------------------;
    MOV     #3,X                    ; PUT 3 times to skip CRC16
    CALL    #SPI_X_GET              ; and to get data token in W
; ----------------------------------;
;CheckWriteState                    ;
; ----------------------------------;
    BIC.B   #0E1h,W                 ; apply mask for Data response
    SUB.B   #4,W                    ; data accepted
    JZ      ReadWriteHappyEnd       ;
; ----------------------------------;

    .ENDIF ; SD_CARD_READ_WRITE

; ----------------------------------;
; SD ERRORS
; ----------------------------------;
; 0   = NO SD_CARD
; ----------------------------------;
; 
; ----------------------------------;
; High byte = 0 : FILE level error
; ----------------------------------;
;       low byte
;       $1  = PathNameNotFound
;       $2  = NoSuchFile      
;       $4  = alreadyOpen     
;       $8  = NomoreHandle    
;       $10 = InvalidPathname
;       $20 = DiskFull
;       
; ----------------------------------;
; High byte <> 0 : SD_CARD level error 
; ----------------------------------;
; 1   = CMD17    read error
; 2   = CMD24    write error
; 4   = CMD0     time out (GO_IDLE_STATE)
; 8   = ACMD41   time out (APP_SEND_OP_COND)
; $10 = partition error, low byte = partition ID <> FAT32
;
;       low byte, if CMD R1 response:
;       bit0 = In Idle state
;       bit1 = Erase reset
;       bit2 = Illegal command
;       bit3 = Command CRC error
;       bit4 = erase sequence error
;       bit5 = address error
;       bit6 = parameter error

;       low byte if Data Response Token
;       Every data block written to the card will be acknowledged by a data response token.
;       It is one byte long and has the following format:
;       %xxxx_sss0 with bits(3-1) = Status
;       The meaning of the status bits is defined as follows:
;       '010' - Data accepted.
;       '101' - Data rejected due to a CRC error.
;       '110' - Data Rejected due to a Write Error

; ------------------------------;
SD_CARD_ERROR                   ; <=== SD_INIT errors 4,8,$10 from SD_INIT
; ------------------------------;
        SWPB S                  ; High Level error in High byte
        BIS &SD_RXBUF,S         ; add SPI(GET) return value as low byte error
SD_CARD_INIT_ERROR              ; <=== from forthMSP430FR_SD_INIT.asm
SD_OPEN_FILE_ERROR              ; <=== from forthMSP430FR_SD_LOAD.asm, forthMSP430FR_SD_RW.asm 
NO_SD_CARD                      ; from forthMSP430FR_SD_LOAD(Open_File)
        MOV S,TOS               ;
        CALL #INIT_SYS          ;
    .IFDEF BOOTLOADER           ;
        .word   NOBOOT          ;
    .ENDIF                      ;
        .word   ECHO            ;
        .word   XSQUOTE         ;
        .byte   4,27,"[7m"      ;
        .word   TYPE            ;
        .word   XSQUOTE         ;
        .byte   9,"SD_ERROR "   ;
        .word   TYPE            ;
        .word   UDOT            ;
        .word   BRAN,SDABORT_END;   to set normal video display then goto ABORT
; ------------------------------;

; ==================================;
; SD_CARD FAT32 INIT                ;
; ==================================;

; =====================================================================
; goal : accept 64 MB up to 64 GB SD_CARD
; =====================================================================
; thus FAT and RootClus logical sectors are word addressable.

; FAT is a little endian structure.
; CMD frame is sent as big endian.

; we assume that SDSC Card (up to 2GB) is FAT16 with a byte addressing
; and that SDHC Card (4GB up to 64GB) is FAT32 with a sector addressing (sector = 512 bytes)
; for SDHC Card = 64 GB, cluster = 64 sectors ==> max clusters = 20 0000h ==> FAT size = 16384 sectors
; ==> FAT1 and FAT2 can be addressed with a single word.

; ref. https://en.wikipedia.org/wiki/Extended_boot_record
; ref. https://en.wikipedia.org/wiki/Partition_type

; Formatage FA16 d'une SDSC Card 2GB
; First sector of physical drive (sector 0) content :
; ---------------------------------------------------
; dec@| HEX@
; 446 |0x1BE    : partition table first record  ==> logical drive 0
; 462 |0x1CE    : partition table 2th record    ==> logical drive 1
; 478 |0x1DE    : partition table 3th record    ==> logical drive 2
; 494 |0x1EE    : partition table 4th record    ==> logical drive 3

; partition of first record content :
; ---------------------------------------------------
; 450 |0x1C2 = 0x0E         : type FAT16 using LBA addressing
; 454 |0x1C6 = 89 00 00 00  : FirstSector (of logical drive 0) BS_FirstSector  = 137


; Partition type Description
; 0	    empty / unused
; 1	    FAT12
; 4	    FAT16 for partitions <= 32 MiB
; 5	    extended partition
; 6	    FAT16 for partitions > 32 MiB
; 11	FAT32 for partitions <= 2 GiB
; 12	Same as type 11 (FAT32), but using LBA addressing, which removes size constraints
; 14	Same as type 6 (FAT16), but using LBA addressing
; 15	Same as type 5, but using LBA addressing
; ref. https://www.compuphase.com/mbr_fat.htm#BOOTSECTOR

; FirstSector of logical drive (sector 0) content :
; -------------------------------------------------
; dec@| HEX@ =  HEX                                                       decimal
; 11  | 0x0B = 00 02        : 512 bytes/sector          BPB_BytsPerSec  = 512
; 13  | 0x0D = 40           : 64 sectors/cluster        BPB_SecPerClus  = 64
; 14  | 0x0E = 01 00        : 2 reserved sectors        BPB_RsvdSecCnt  = 1
; 16  | 0x10 = 02           : 2 FATs                    BPB_NumFATs     = 2 (always 2)
; 17  | 0x11 = 00 02        : 512 entries/directory     BPB_RootEntCnt  = 512
; 19  | 0x13 = 00 00        : BPB_TotSec16 (if < 65535) BPB_TotSec16    = 0
; 22  | 0x16 = EB 00        : 235 sectors/FAT (FAT16)   BPB_FATSize     = 235
; 32  | 0x20 = 77 9F 3A 00  : ‭3841911‬ total sectors     BPB_TotSec32    = ‭3841911‬
; 54  | 0x36 = "FAT16"                                  BS_FilSysType   (not used)

; all values below are evaluated in logical sectors
; FAT1           = BPB_RsvdSecCnt = 1
; FAT2           = BPB_RsvdSecCnt + BPB_FATSz32 = 1 + 235 = 236
; OrgRootDirL    = BPB_RsvdSecCnt + (BPB_FATSize * BPB_NumFATs) = 471
; RootDirSize    = BPB_RootEntCnt * 32 / BPB_BytsPerSec         = 32 sectors
; OrgDatas       = OrgRootDir + RootDirSize                     = 503
; OrgCluster     = OrgRootDir - 2*BPB_SecPerClus                = 375 (virtual value)
; FirstSectorOfCluster(n) = OrgCluster + n*BPB_SecPerClus       ==> cluster(3) = 705

; ====================================================================================

; Formatage FA32 d'une SDSC Card 8GB
; First sector of physical drive (sector 0) content :
; ---------------------------------------------------
; dec@| HEX@
; 446 |0x1BE    : partition table first record  ==> logical drive 0
; 462 |0x1CE    : partition table 2th record    ==> logical drive 1
; 478 |0x1DE    : partition table 3th record    ==> logical drive 2
; 494 |0x1EE    : partition table 4th record    ==> logical drive 3

; partition record content :
; ---------------------------------------------------
; 450 |0x1C2 = 0x0C         : type FAT32 using LBA addressing
; 454 |0x1C6 = 00 20 00 00  : FirstSector (of logical drive 0) = BS_FirstSector = 8192

;
; FirstSector of logical block (sector 0) content :
; -------------------------------------------------
; dec@| HEX@ =  HEX                                                       decimal
; 11  | 0x0B = 00 02        : 512 bytes/sector          BPB_BytsPerSec  = 512
; 13  | 0x0D = 08           : 8 sectors/cluster         BPB_SecPerClus  = 8
; 14  | 0x0E = 20 00        : 32 reserved sectors       BPB_RsvdSecCnt  = 32
; 16  | 0x10 = 02           : 2 FATs                    BPB_NumFATs     = 2 (always 2)
; 17  | 0x11 = 00 00        : 0                         BPB_RootEntCnt  = 0 (always 0 for FAT32)

; 32  | 0x20 = 00 C0 EC 00  : BPB_TotSec32              BPB_TotSec32    = 15515648
; 36  | 0x24 = 30 3B 00 00  : BPB_FATSz32               BPB_FATSz32     = 15152
; 40  | 0x28 = 00 00        : BPB_ExtFlags              BPB_ExtFlags
; 44  | 0x2C = 02 00 00 00  : BPB_RootClus              BPB_RootClus    = 2
; 48  | 0x30 = 01 00        : BPB_FSInfo                BPB_FSInfo      = 1
; 50  | 0x33 = 06 00        : BPB_BkBootSec             BPB_BkBootSec   = 6
; 82  | 0x52 = "FAT32"      : BS_FilSysType             BS_FilSysType   (not used)

;
; all values below are evaluated in logical sectors
; FAT1           = BPB_RsvdSecCnt = 32
; FAT2           = BPB_RsvdSecCnt + BPB_FATSz32 = 32 + 15152 = 15184
; OrgRootDirL    = BPB_RsvdSecCnt + BPB_FATSz32 * BPB_NumFATs = 32 + 15152*2 = 30336
; OrgCluster     = OrgRootDir - 2*BPB_SecPerClus = 30320
; RootDirSize    = BPB_RootEntCnt * 32 / BPB_BytsPerSec         = 0
; OrgDatas       = OrgRootDir + RootDirSize                     = 30336
; FirstSectorOfCluster(n) = OrgCluster + n*BPB_SecPerClus       ==> cluster(6) = 30368


; ===========================================================
; WARNING! SD_INIT DRAW BIG CURRENT; IF THE SUPPLY IS TOO WEAK
; THE SD_CARD LOW VOLTAGE THRESHOLD MAY BE REACHED ==> SD_ERROR 4FF !
; ===========================================================

; ===========================================================
; Init SD_Card software, called by INIT_FORTH(SOFT_APP)
; ===========================================================
;-----------------------------------;
INIT_SOFT_SD                        ; called by INI_FORTH common part of ?ABORT|RST
;-----------------------------------;
;            MOV #0,&CurrentHdl      ; close all handles
;            PUSH #INIT_SOFT         ; link to previous INI_SOFT_APP then RET
;            CALL #CloseHandle       ;
;-----------------------------------;

; ===========================================================
; Init hardware SD_Card, called by WARM(HARD_APP)
; ===========================================================

; web search: "SDA simplified specifications"

;-----------------------------------;
INIT_HARD_SD CALL @PC+              ; link to previous HARD_APP first, because used by ERROR outputs
            .word INIT_HARD         ; which activates all previous I/O settings.
;-----------------------------------;
            BIT.B #CD_SD,&SD_CDIN   ; SD_memory in SD_Card module ?
            JNZ INIT_HSD_END        ; no
;-----------------------------------;
            MOV #0A981h,&SD_CTLW0   ; UCxxCTL1  = CKPH, MSB, MST, SPI_3, SMCLK  + UCSWRST
            MOV #FREQUENCY*3,&SD_BRW; UCxxBRW init SPI CLK = 333 kHz ( <= 400 kHz) for SD_Card initialisation
            BIS.B #CS_SD,&SD_CSDIR  ; SD Chip Select as output high
            BIS #BUS_SD,&SD_SEL     ; Configure pins as SIMO, SOMI & SCK (PxDIR.y are controlled by eUSCI module)
            BIC #1,&SD_CTLW0        ; release eUSCI from reset
;-----------------------------------;
            MOV #SD_LEN,X           ; clear all SD datas
ClearSDdata SUB #2,X                ; 1
            MOV #0,SD_ORG(X)        ; 3
            JNZ ClearSDdata         ; 2
;-----------------------------------;
    BIC.B   #CS_SD,&SD_CSOUT        ; preset Chip Select output low to switch in SPI mode
; ----------------------------------;
INIT_CMD0                           ; SD_CMD_FRM is zero fullfilled...
; ----------------------------------;
    MOV     #4,S                    ; preset error 4R1 for CMD0
    MOV     #0095h,&SD_CMD_FRM      ; $(95 00 00 00 00 00)
    MOV     #0,&SD_CMD_FRM+2        ;
    MOV     #4000h,&SD_CMD_FRM+4    ; $(95 00 00 00 00 40) = CMD0
; ----------------------------------;
SEND_CMD0_LOOP                      ; GO_IDLE_STATE (software reset), expected SPI_R1 response = 1 = idle state
; ----------------------------------;
    MOV     #8,Y                    ; this loop is mandatory, but not documented in "SDA simplified specifications"
    CALL    #sendCommandIdleRet     ;X send command (does little to big endian conversion)
    JZ      INIT_CMD8               ; if idle state reached (W=1)
    SUB     #1,Y                    ;
    JNZ     SEND_CMD0_LOOP          ; else loop back 8 times, because init time of SD_Card can be long...
SD_INIT_ERROR                       ;
    JMP     SD_CARD_ERROR           ; ReturnError = $04R1, case of defectuous card (or insufficient SD_POWER_ON clk)
; ----------------------------------; see forthMSP430FR_SD_lowLvl.asm
INIT_CMD8                           ; mandatory if SD_Card >= V2.x     [11:8]supply voltage(VHS)
; ----------------------------------;
    MOV     #0AA87h,&SD_CMD_FRM     ; $(87 AA ...)  (CRC:CHECK PATTERN)
    MOV     #1,&SD_CMD_FRM+2        ; $(87 AA 01 00 ...)  (CRC:CHECK PATTERN:VHS set as 2.7to3.6V:0)
    MOV     #4800h,&SD_CMD_FRM+4    ; $(87 AA 01 00 00 48)
; ----------------------------------;
SEND_CMD8                           ; SEND_IF_COND; expected R1 response (first byte of SPI R7) = 01h : idle state
; ----------------------------------;
    CALL    #sendCommandIdleRet     ;X time out occurs with SD_Card V1.x (and all MMC_card)
; ----------------------------------;
    MOV     #4,X                    ; skip end of SD_Card V2.x type R7 response (4 bytes), because useless
    CALL    #SPI_X_GET              ;WX
; ----------------------------------;
INIT_ACMD41                         ; no more CRC needed from here in simplified protocol
; ----------------------------------;
    MOV     #1,&SD_CMD_FRM          ; $(01 00 ...   set stop bit
    MOV     #0,&SD_CMD_FRM+2        ; $(01 00 00 00 ...
    MOV.B   #-1,Y                   ; init 255 * ACMD41 repeats ==> ~3 s time out
    MOV     #8,S                    ; preset error 8R1 for ACMD41
; ----------------------------------;
SEND_ACMD41                         ; send CMD55+CMD41
; ----------------------------------;
INIT_CMD55                          ;
    MOV     #7700h,&SD_CMD_FRM+4    ; $(01 00 00 00 00 77)
SEND_CMD55                          ; CMD55 = APP_CMD; expected SPI_R1 response = 1 : idle
    CALL    #sendCommandIdleRet     ;X
SEND_CMD41                          ; CMD41 = APP OPERATING CONDITION
    MOV     #6940h,&SD_CMD_FRM+4    ; $(01 00 00 00 40 69) (30th bit = HCS = High Capacity Support request)
    CALL    #WaitIdleBeforeSendCMD  ; wait until idle (needed to pass SanDisk ultra 8GB "HC I") then send Command CMD41
    JZ      SwitchSPIhighSpeed      ; if SD_Card ready (R1=0)
    SUB.B   #1,Y                    ; else decr time out delay
    JNZ     INIT_CMD55              ; then loop back while count of repeat not reached
    JMP     SD_CARD_ERROR           ; ReturnError on time out : unusable card  (or insufficient Vdd SD)
; ----------------------------------; W = R1 = 0
SwitchSPIhighSpeed                  ; end of SD init ==> SD_CLK = SMCLK
; ----------------------------------;
    BIS     #1,&SD_CTLW0            ; UC Software reset
    MOV     #0,&SD_BRW              ; UCxxBRW = 0 ==> SPI_CLK = MCLK
    BIC     #1,&SD_CTLW0            ; release from reset
; ----------------------------------;
Read_EBP_FirstSector                ; BS_FirstSectorHL=0
; ----------------------------------;
    MOV     #0,W                    ;
    MOV     #0,X                    ;
    CALL    #ReadSectorWX           ; read physical first sector, W=0
    MOV     #SD_BUF,Y               ;
; ----------------------------------;
    CMP     #0AA55h,1FEh(Y)         ; valid boot sector ?
    JZ      SetMBR                  ;
    MOV     #1000h,S                ; error Boot Sector
    JMP     SD_CARD_INIT_ERROR      ;
; ----------------------------------;
SetMBR                              ;
; ----------------------------------;
    MOV     1C6h(Y),&BS_FirstSectorL; logical sector = physical sector + BS_FirstSector
    MOV     1C8h(Y),&BS_FirstSectorH;
; ----------------------------------;
TestPartitionID                     ;
; ----------------------------------;
    MOV.B   1C2h(Y),S               ; S = partition ID
    SUB.B   #0Ch,S                  ; ID=0Ch Partition FAT32 using LBA ?
    JZ      Read_MBR_FirstSector    ;
    ADD.B   #1,S                    ; ID=0Bh Partition FAT32 using CHS & LBA ?
    JZ      Read_MBR_FirstSector    ;
    ADD.B   #4,S                    ; ID=07h assigned to FAT32 by MiniTools Partition Wizard....
    JZ      Read_MBR_FirstSector    ;
    ADD     #01007h,S               ; set ReturnError = $10 & restore ID value
    JMP     SD_CARD_INIT_ERROR      ; see: https://en.wikipedia.org/wiki/Partition_type
; ----------------------------------;
Read_MBR_FirstSector                ; read first logical sector
; ----------------------------------;
    MOV     #0,X                    ; W = 0
    CALL    #ReadSectorWX           ;
; ----------------------------------;
FAT32_SetFileSystem                 ;
; ----------------------------------;
    MOV     0Eh(Y),X                ;3 X = BPB_RsvdSecCnt (05FEh=1534)
    MOV     X,&OrgFAT1              ;3 set OrgFAT1
; ----------------------------------;
    MOV     24h(Y),W                ; no set W = BPB_FATSz32 (1D01h=7425)
    MOV     W,&FATSize              ; limited to 32767 sectors....
; ----------------------------------;
    ADD     W,X                     ;
    MOV     X,&OrgFAT2              ; X = OrgFAT1 + FATsize = OrgFAT32 (8959)
; ----------------------------------;
    ADD     W,X                     ; X = OrgFAT2 + FATsize = FAT32 OrgDatas (16384)
FATxx_SetFileSystemNext             ;
    MOV.B   0Dh(Y),Y                ; Logical sectors per cluster (8)
    MOV     Y,&SecPerClus           ;
    SUB     Y,X                     ; OrgDatas - 2*SecPerClus = OrgClusters
    SUB     Y,X                     ; no borrow expected
    MOV     X,&OrgClusters          ; X = virtual cluster 0 address (clusters 0 and 1 don't exist)
    MOV     #2,&DIRClusterL         ; init DIRcluster=cluster#2 as FAT32 RootDIR
    MOV     #0,&DIRClusterH         ;
INIT_HSD_END                        ;
    MOV     @RSP+,PC                ; RET
;-----------------------------------;
; used variables : BufferPtr, BufferLen

; ==================================;
; SD LOAD DRIVER
; ==================================;
; SD card OPEN, LOAD subroutines
;-----------------------------------;

; ==================================;
ReadFAT1SectorW                     ;SWX (< 65536) W = FATsector
; ==================================;
    ADD     &OrgFAT1,W              ; W = FAT1_SectorLO
    MOV     #0,X                    ; FAT1_SectorHI = 0
    JMP     ReadSectorWX            ;SWX read FAT1SectorW, W = 0
; ----------------------------------;

   .IFDEF SD_CARD_READ_WRITE
; this subroutine is called by Write_File (bufferPtr=512) and CloseHandle (0 =< BufferPtr =< 512)
; ==================================;
WriteSD_Buf                         ;SWX input: T = CurrentHDL
; ==================================;
    ADD &BufferPtr,HDLL_CurSize(T)  ; update handle CurrentSize
    ADDC    #0,HDLH_CurSize(T)      ;
; ==================================;
WriteSectorHL                       ;SWX
; ==================================;
    MOV     &SectorL,W              ; Low
    MOV     &SectorH,X              ; High
    JMP     WriteSectorWX           ; ...then RET
; ----------------------------------;

; ==================================;
FirstClus2FATsecWofsY               ;WXY Input: T=Handle, HDL_CurClustHL  Output: ClusterHL, FATsector, W = FATsector, Y = FAToffset
; ==================================;
    MOV HDLL_FirstClus(T),&ClusterL ;
    MOV HDLH_FirstClus(T),&ClusterH ;
    JMP Cluster2FAT1secWofsY        ;
; ----------------------------------;
    .ENDIF

; rules for registers use
; S = error
; T = CurrentHdl, pathname
; W = SectorL, (RTC) TIME
; X = SectorH, (RTC) DATE
; Y = BufferPtr, (DIR) DIREntryOfst
; ==================================;
CurClust2FATsectWofsY               ;WXY Input: T=Handle, HDL_CurClustHL  Output: ClusterHL, FATsector, W = FATsector, Y = FAToffset
; ==================================;
    MOV HDLL_CurClust(T),&ClusterL  ;
    MOV HDLH_CurClust(T),&ClusterH  ;
; ==================================;
Cluster2FAT1secWofsY                ;WXY Input : ClusterHL   Output: ClusterHL, FATsector, W = FATsector, Y = FAToffset
; ==================================;limited to $10000 sectors ==> $800000 clusters ==> 32GB for 4k clusters
    MOV.B &ClusterL+1,W             ;3 W = ClusterLoHI
    MOV.B &ClusterL,Y               ;3 Y = ClusterLOlo
; input : Cluster n, max = 7FFFFF,  (SD_card up to 256 GB with 64k clusters)
; ClusterLoLo*4 = displacement in 512 bytes sector   ==> FAToffset
; ClusterHiLo&ClusterLoHi +C  << 1 = relative FATsector + orgFAT1       ==> FATsector
; ----------------------------------;
    MOV.B &ClusterH,X               ;3 X = 0:ClusterHIlo
    SWPB X                          ;1 X = ClusterHIlo:0
    BIS X,W                         ;1 W = ClusterHIlo:ClusterLOhi
; ----------------------------------;
    SWPB Y                          ;1 Y = ClusterLOlo:0
    ADD Y,Y                         ;1 Y = ClusterLOlo:0 << 1  (carry report for FATsector)
    ADDC W,W                        ;1 FATsector = W = ClusterHIlo:ClusterLOhi<<1 + Carry
    SWPB Y                          ;1 Y = 0:ClusterLOlo
    ADD Y,Y                         ;1 FAToffset = Y = 0:ClusterLOlo<<2 for FAT32
    MOV @RSP+,PC                    ;4
; ----------------------------------;

; use no registers
; ==================================;
ClusterHLtoFrstSectorHL             ; Input : ClusterHL, output: first SectorHL of ClusterHL
; ==================================;
    .IFDEF MPY                      ; general case
; ----------------------------------;
    MOV     &ClusterL,&MPY32L       ;3
    MOV     &ClusterH,&MPY32H       ;3
    MOV     &SecPerClus,&OP2        ;5+3
    MOV     &RES0,&SectorL          ;5
    MOV     &RES1,&SectorH          ;5
    ADD     &OrgClusters,&SectorL   ;5 OrgClusters = sector of virtual cluster 0, word size
    ADDC    #0,&SectorH             ;3 32~
; ----------------------------------;
    .ELSE                           ; case of no hardware multiplier
; ----------------------------------; Cluster24<<SecPerClus --> ClusFrstSect; SecPerClus = {1,2,4,8,16,32,64}
    PUSHM  #3,W                     ;5 PUSHM W,X,Y
    MOV.B &SecPerClus,W             ;3 SecPerClus(5-1) = multiplicator
    MOV &ClusterL,X                 ;3 Cluster(16-1) --> MULTIPLICANDlo
    MOV.B &ClusterH,Y               ;3 Cluster(24-17) -->  MULTIPLICANDhi
    JMP CCFS_ENTRY                  ;
CCFS_LOOP                           ;
    ADD X,X                         ;1 (RLA) shift one left MULTIPLICANDlo16
    ADDC Y,Y                        ;1 (RLC) shift one left MULTIPLICANDhi8
CCFS_ENTRY
    RRA W                           ;1 shift one right multiplicator
    JNC CCFS_LOOP                   ;2 C = 0 loop back
    ADD &OrgClusters,X              ;3 OrgClusters = sector of virtual_cluster_0, word size
    ADDC #0,Y                       ;1
    MOV X,&SectorL                  ;3 low result
    MOV Y,&SectorH                  ;3 high result
    POPM  #3,W                      ;5 POPM Y,X,W
; ----------------------------------;32~ + 5~ by 2* shift
    .ENDIF ; MPY
; ----------------------------------;
    MOV @RSP+,PC                    ;
; ----------------------------------;

; ==================================;
CurClustPlsOfst2SectHL              ;SWX input: HDL (CurClust, ClustOfst) output: SectorHL
; ==================================;
    MOV HDLL_CurClust(T),&ClusterL  ;
    MOV HDLH_CurClust(T),&ClusterH  ;
; ==================================;
ClusterHL2sectorHL                  ;W input: ClusterHL, ClustOfst output: SectorHL
; ==================================;
    CALL #ClusterHLtoFrstSectorHL   ;
    MOV.B HDLB_ClustOfst(T),W       ; byte to word conversion
    ADD W,&SectorL                  ;
    ADDC #0,&SectorH                ;
    MOV @RSP+,PC                    ;
; ----------------------------------;

; if first open_load token, save DefaultInputStream
; if other open_load token, decrement token, save previous context

; OPEN subroutine
; Input : DIREntryOfst, Cluster = DIREntryOfst(HDLL_FirstClus())
; init handle(HDLL_DIRsect,HDLW_DIRofst,HDLL_FirstClus,HDLL_CurClust,HDLL_CurSize)
; Output: Cluster = first Cluster of file, X = CurrentHdl
; ==================================; input : Cluster, DIREntryOfst
GetFreeHandle                       ;STWXY init handle(HDLL_DIRsect,HDLW_DIRofst,HDLL_FirstClus = HDLL_CurClust,HDLL_CurSize)
; ==================================; output : T = new CurrentHdl
    MOV #4,S                        ; prepare file already open error
    MOV #FirstHandle,T              ;
    MOV #0,X                        ; X = init previous handle as 0
; ----------------------------------;
SearchHandleLoop                    ;
; ----------------------------------;
    CMP.B   #0,HDLB_Token(T)        ; free handle ?
    JZ      FreeHandleFound         ; yes
;AlreadyOpenTest                    ; no
    CMP     &ClusterH,HDLH_FirstClus(T);
    JNE     SearchNextHandle        ;
    CMP     &ClusterL,HDLL_FirstClus(T);
    JZ      OPEN_Error              ; error 4: Already Open abort ===>
SearchNextHandle                    ;
    MOV     T,X                     ; handle is occupied, keep it in X as previous handle
    ADD     #HandleLenght,T         ;
    CMP     #HandleEnd,T            ;
    JNC     SearchHandleLoop        ;
    MOV     #8,S                    ;
    JMP     OPEN_Error              ; error 8 = no more handle error, abort ===>
; ----------------------------------;
FreeHandleFound                     ; T = new handle, X = previous handle
; ----------------------------------;
    MOV     #0,S                    ; prepare Happy End (no error)
    MOV     T,&CurrentHdl           ;
    MOV     X,HDLW_PrevHDL(T)       ; link to previous handle
; ----------------------------------;
;CheckCaseOfPreviousLoadToken       ;
; ----------------------------------;
    CMP.B   #0,W                    ; open_type is negative (open_load type) ?
    JGE     InitHandle              ; no
    CMP     #0,X                    ; existing previous handle?
    JZ      InitHandle              ; no
    CMP.B   #0,HDLB_Token(X)        ; previous token also ?
    JGE     InitHandle              ; no
    ADD.B   HDLB_Token(X),W         ; token = previous token -1
; ----------------------------------;
InitHandle                          ;
; ----------------------------------;
    MOV.B   W,HDLB_Token(T)         ; marks handle as open type: <0=LOAD, 1=READ, 2=DEL, 4=WRITE, 8=APPEND
    MOV.B   #0,HDLB_ClustOfst(T)    ; clear ClustOfst
    MOV     &SectorL,HDLL_DIRsect(T); init handle DIRsectorL
    MOV     &SectorH,HDLH_DIRsect(T);
    MOV     &DIREntryOfst,Y         ;
    MOV     Y,HDLW_DIRofst(T)       ; init handle SD_BUF offset of DIR entry
    MOV SD_BUF+26(Y),HDLL_FirstClus(T); init handle firstcluster of file (to identify file)
    MOV SD_BUF+20(Y),HDLH_FirstClus(T); = 0 if new DIRentry (create write file)
    MOV SD_BUF+26(Y),HDLL_CurClust(T); init handle CurrentCluster
    MOV SD_BUF+20(Y),HDLH_CurClust(T); = 0 if new DIRentry (create write file)
    MOV SD_BUF+28(Y),HDLL_CurSize(T); init handle LOW currentSizeL
    MOV SD_BUF+30(Y),HDLH_CurSize(T); = 0 if new DIRentry (create write file)
    MOV     #0,&BufferPtr           ; reset BufferPtr all type of files
    CMP.B   #2,W                    ; del file request (2) ?
    JZ      HandleRET      	        ;
    JGE     CurClustPlsOfst2SectHL  ; set ClusterHL and SectorHL for all WRITE requests
; ----------------------------------;
    MOV     #0,HDLW_BUFofst(T)      ; < 2, is a READ or a LOAD request
    CMP.B   #-1,W                   ;
    JZ      ReplaceInputBuffer      ; case of first loaded file
    JL      SaveAcceptContext       ; case of other loaded file
    JMP     SetBufLenLoadCurSector  ; case of READ file
; ----------------------------------;
ReplaceInputBuffer                  ;
; ----------------------------------;
    MOV #SDIB_ORG,&CIB_ORG          ; set SD Input Buffer as Current Input Buffer before return to QUIT
    MOV #SD_ACCEPT,&PFAACCEPT       ; redirect ACCEPT to SD_ACCEPT before return to QUIT
; ----------------------------------;
SaveAcceptContext                   ;
; ----------------------------------;
    MOV &SOURCE_ORG,HDLW_PrevORG(T) ;
    ADD &TOIN,HDLW_PrevORG(T)       ;
    MOV &SOURCE_LEN,HDLW_PrevLEN(T) ;
    SUB &TOIN,HDLW_PrevLEN(T)       ;
;    MOV &YEMIT,HDLW_PrevECHO(T)     ;
    JMP SetBufLenLoadCurSector      ; then RET
; ----------------------------------;


; sequentially load in SD_BUF bytsPerSec bytes of a file opened as read or load
; HDLL_CurSize leaves the not yet read size
; All used registers must be initialized.
; ==================================;
Read_File                           ; <== SD_ACCEPT|READ
; ==================================;
    MOV     &CurrentHdl,T           ;
    MOV     #0,&BufferPtr           ; reset BufferPtr (the buffer is already read)
; ----------------------------------;
    CMP     #bytsPerSec,&BufferLen  ;
    JNZ     TokenToCloseTest        ; because already read
    SUB #bytsPerSec,HDLL_CurSize(T) ; HDLL_CurSize is decremented of one sector lenght
    SUBC    #0,HDLH_CurSize(T)      ;
    ADD.B   #1,HDLB_ClustOfst(T)    ; increment current cluster offset
    CMP.B &SecPerClus,HDLB_ClustOfst(T) ; Cluster Bound reached ?
    JNC     SetBufLenLoadCurSector  ; no
; ----------------------------------;
;SearchNextClusterInFAT1            ;
; ----------------------------------;
    MOV.B   #0,HDLB_ClustOfst(T)    ; reset Current_Cluster sectors offset
    CALL    #CurClust2FATsectWofsY  ;WXY  Output: FATsector W=FATsector, Y=FAToffset
    CALL    #ReadFAT1SectorW        ;SWX
    MOV SD_BUF(Y),HDLL_CurClust(T)  ;
    MOV SD_BUF+2(Y),HDLH_CurClust(T);
; ==================================;
SetBufLenLoadCurSector              ;WXY <== previous handle reLOADed with BufferPtr<>0
; ==================================;                        --
    MOV     #bytsPerSec,&BufferLen  ; preset BufferLen
    CMP     #0,HDLH_CurSize(T)      ; CurSize > 65535 ?
    JNZ     LoadCurSectorHL         ; yes
; ----------------------------------;
    CMP     #0,HDLL_CurSize(T)      ; CurSize = 0 ?
    JZ      TokenToCloseTest        ;
; ----------------------------------;
    CMP #bytsPerSec,HDLL_CurSize(T) ; CurSize >= 512 ?
    JC      LoadCurSectorHL         ; yes
    MOV HDLL_CurSize(T),&BufferLen  ; no: adjust BufferLen
; ==================================;
LoadCurSectorHL                     ;
; ==================================;
    CALL    #CurClustPlsOfst2SectHL ;SWX
; ==================================;
ReadSectorHL                        ;
; ==================================;
    MOV     &SectorL,W              ; Low
    MOV     &SectorH,X              ; High
    JMP     ReadSectorWX            ; SWX then RET with W = 0, SR(Z) = 1
; ----------------------------------;


; ==================================;
CloseHandle                         ; <== CLOSE|TERM2SD"|OPEN_DEL
; ==================================;
    MOV &CurrentHdl,T               ;
    CMP #0,T                        ; no handle?
    JZ HandleRet               		; RET
; ----------------------------------;
    .IFDEF SD_CARD_READ_WRITE
; ----------------------------------;
    CMP.B #4,HDLB_Token(T)          ; WRITE file (4,8) ?
    JL TokenToCloseTest             ; no, case of DEL READ LOAD file
; ----------------------------------;
;UpdateWriteSector                  ; case of any WRITE file
; ----------------------------------;
    CALL #WriteSD_Buf               ;SWX
; ----------------------------------;
;Load Update DirEntry               ;SWXY
; ----------------------------------;
    MOV     HDLL_DIRsect(T),W       ;
    MOV     HDLH_DIRsect(T),X       ;
    CALL    #ReadSectorWX           ;SWX SD_buffer = DIRsector
    MOV     HDLW_DIRofst(T),Y       ; Y = DirEntryOffset
    CALL    #GetYMDHMSforDIR        ; X=DATE,  W=TIME
    MOV     X,SD_BUF+18(Y)          ; access date
    MOV     W,SD_BUF+22(Y)          ; modified time
    MOV     X,SD_BUF+24(Y)          ; modified date
    MOV HDLL_CurSize(T),SD_BUF+28(Y); save new filesize
    MOV HDLH_CurSize(T),SD_BUF+30(Y);
    MOV     HDLL_DIRsect(T),W       ;
    MOV     HDLH_DIRsect(T),X       ;
    CALL    #WriteSectorWX          ;SWX
; ----------------------------------;
    .ENDIF
; ==================================;
TokenToCloseTest                    ; <== Read_File
; ==================================;
    CMP.B #-1,HDLB_Token(T)         ;
    JZ RestoreDefaultACCEPT         ;
; ----------------------------------;
;CaseOfAnyReadWriteDelFileIsClosed  ; token > 0 (token 0 is used to change current DIR, without handle...)
; ----------------------------------;
    JGE CloseHandleRightNow         ;
; ----------------------------------;
    MOV #4D30h,&YEMIT               ; set NOECHO
    JL CloseLoadFile                ; 
; ----------------------------------;
RestoreDefaultACCEPT                ; if token = -1 (first LOAD")
; ----------------------------------;
    MOV #0B3A2h,&YEMIT              ; set ECHO
    MOV #TIB_ORG,&CIB_ORG           ; restore TIB as Current Input Buffer and..
    MOV #BODYACCEPT,&PFAACCEPT      ; restore default ACCEPT for next return to QUIT loop
; ----------------------------------;
CloseLoadFile                       ; -- DstOrg MaxEnd DstEnd   R-- SDA_InitSrcAddr   <=== CloseHandle <=== ReadFile <=== SD_ACCEPT 
; ----------------------------------;
    MOV #SDA_EndOfFile,0(RSP)       ;                           R-- SDA_EndOfFile
; ----------------------------------;
;RestorePreviousContext             ;          
; ----------------------------------;
    ADD #2,PSP                      ; -- DstOrg DstEnd
    MOV HDLW_PrevLEN(T),TOS         ;
    MOV HDLW_PrevORG(T),0(PSP)      ; -- PrvOrg PrvLen
; ----------------------------------;
CloseHandleRightNow                 ;
; ----------------------------------;
    MOV.B #0,HDLB_Token(T)          ; release the handle
    MOV @T,T                        ; T = previous handle
    MOV T,&CurrentHdl               ; becomes current handle
    CMP #0,T                        ; no more handle ?
    JZ HandleRet               		; with SR(Z) = 1
; ----------------------------------;
RestorePreviousLoadedBuffer         ;
; ----------------------------------;
    MOV HDLW_BUFofst(T),&BufferPtr  ; restore previous BufferPtr
    CALL #SetBufLenLoadCurSector    ; then reload previous buffer
; ----------------------------------;
HandleRet                      		;
; ----------------------------------;
    MOV @RSP+,PC                    ; SR(Z) state is used by SD_ACCEPT(SDA_RetOfCloseHandle)
; ----------------------------------;

   .IFDEF SD_CARD_READ_WRITE
;-----------------------------------------------------------------------
; SD_READ_WRITE FORTH words
;-----------------------------------------------------------------------

;Z READ"         --
; parse string until " is encountered, convert counted string in String
; then parse string until char '0'.
; media identifiers "A:", "B:" ... are ignored (only one SD_Card),
; char "\" as first one initializes rootDir as SearchDir.
; if file found, if not already open and if free handle...
; ...open the file as read and return the handle in CurrentHdl.
; then load first sector in buffer, bufferLen and bufferPtr are ready for read
; currentHdl keep handle that is flagged as "read".

; to read sequentially next sectors use READ word. A flag is returned : true if file is closed.
; the last sector so is in buffer.

; if pathname is a directory, change current directory.
; if an error is encountered, no handle is set, error message is displayed.

; READ" acts also as CD dos command :
;     - READ" a:\misc\" set a:\misc as current directory
;     - READ" a:\" reset current directory to root
;     - READ" ..\" change to parent directory

; to close all files type : WARM (or COLD, RESET)

; ==================================;
    FORTHWORDIMM "READ\34"          ; immediate
; ==================================;
READDQ
    MOV.B   #1,W                    ; W = READ request
    JMP     Open_File               ;
; ----------------------------------;

;Z DEL" pathame"   --       immediate
; ==================================;
    FORTHWORDIMM "DEL\34"           ; immediate
; ==================================;
    MOV.B   #2,W                    ; W = DEL request
    JMP     Open_File               ;
; ----------------------------------;

;Z WRITE" pathame"   --       immediate
; if file exist, free all clusters then switch handle to WRITE
; if "no such file", open a write handle
; ==================================;
    FORTHWORDIMM "WRITE\34"         ; immediate
; ==================================;
WRITEDQ
    MOV.B   #4,W                    ; W = WRITE request
    JMP     Open_File               ;
; ----------------------------------;

;Z APPEND" pathame"   --       immediate
; open the file designed by pathname.
; the last sector of the file is loaded in buffer, and bufferPtr leave the address of the first free byte.
; ==================================;
    FORTHWORDIMM "APPEND\34"        ; immediate
; ==================================;
    MOV.B   #8,W                    ; W = APPEND request
    JMP     Open_File               ;
; ----------------------------------;

;Z CLOSE      --
; close current handle
; ==================================;
    FORTHWORD "CLOSE"               ;
; ==================================;
    CALL    #CloseHandle            ;
    MOV @IP+,PC                     ;
; ----------------------------------;

    .ENDIF ; SD_CARD_READ_WRITE
;-----------------------------------------------------------------------
; SD_CARD_LOADER FORTH word
;-----------------------------------------------------------------------

;Z LOAD" pathame"   --       immediate
; compile state : compile LOAD" pathname"
; exec state : open a file from SD card via its pathname
; see Open_File primitive for pathname conventions
; the opened file becomes the new input stream for INTERPRET
; this command is recursive, limited only by the count of free handles (up to 8)
; LOAD" acts also as dos command "CD" :
;     - LOAD" \misc\" set a:\misc as current directory
;     - LOAD" \" reset current directory to root
;     - LOAD" ..\" change to parent directory

; ==================================;
    FORTHWORDIMM "LOAD\34"          ; immediate
; ==================================;
SD_LOAD MOV.B   #-1,W               ; W = LOAD request
; ======================================================================
; OPEN FILE primitive
; ======================================================================
; Open_File               --
; primitive for LOAD" READ" CREATE" WRITE" DEL"
; store OpenType on TOS,
; compile state : compile OpenType, compile SQUOTE and the string of provided pathname
; exec state :  open a file from SD card via its pathname
;               convert counted string found at HERE in a String then parse it
;                   char "\" as first one initializes rootDir as SearchDir.
;               if file found, if not already open and if free handle...
;                   ...open the file as read and return the handle in CurrentHdl.
;               if the pathname is a directory, change current directory, no handle is set.
;               if an error is encountered, no handle is set, an error message is displayed.
; ==================================;
Open_File                           ; --
; ==================================;
    SUB     #2,PSP                  ;
    MOV     TOS,0(PSP)              ;
    MOV     W,TOS                   ; -- Open_type (-1=LOAD", 1=READ", 2=DEL", 4=WRITE", 8=APPEND")
    CMP     #0,&STATE               ;
    JZ      OPEN_EXEC               ;
; ----------------------------------;
;OPEN_COMP                          ;
    mCOLON                          ; if compile state                              R-- LOAD"_return
    .word   LITERAL                 ; compile open_type as literal
    .word   SQUOTE                  ; compile string_exec + string
    .word   lit,ParenOpen,COMMA     ; compile (OPEN)
    .word   EXIT                    ;
; ----------------------------------;
OPEN_EXEC                           ;
    mCOLON                          ; if exec state
    .word   lit,'"',WORDD,COUNT     ; -- open_type addr cnt
    mHI2LO                          ;
    MOV     @RSP+,IP                ;
; ----------------------------------;
ParenOpen                           ; -- open_type addr cnt         execution of OPEN_COMP: IP points to OPEN_COMP(EXIT),
; ----------------------------------;                               case of OPEN_EXEC: IP points to INTERPRET(INTLOOP).
    MOV #0,S                        ;
    BIT.B #CD_SD,&SD_CDIN           ;                               SD_memory in SD_Card module ?
    JZ Q_SD_not_init                ;                               yes
    BIC #BUS_SD,&SD_SEL             ;                               no, hide SIMO, SOMI & SCK pins (SD not initialized memory)
Q_SD_not_init                       ;          
    BIT #BUS_SD,&SD_SEL             ;                               is SD init by SYS ? 
    JNZ OPEN_LetUsGo                ;                               no --> with TOS = -1 does abort
    MOV #NO_SD_CARD,PC              ;                               S = 0 --> error 0
; ----------------------------------;
OPEN_LetUsGo                        ; -- open_type addr cnt
; ----------------------------------;
    MOV     #1,S                    ;                       preset error 1
    CMP     #0,TOS                  ;                       cnt = 0 ?
    JZ      OPEN_Error              ;                       yes: error 1 ===> PathNameNotFound
    MOV     @PSP+,rDOCON            ; -- open_type cnt      rDOCON = addr = pathname PTR
    ADD     rDOCON,TOS              ; -- open_type EOS      TOS = EOS (End Of String) = pathname end
    .IFDEF SD_CARD_READ_WRITE       ;
    MOV     TOS,&PathName_END       ; for WRITE CREATE part
    .ENDIF
    MOV     &DIRClusterL,&ClusterL  ; set DIR cluster
    MOV     &DIRClusterH,&ClusterH  ;
; ----------------------------------;
;OPN_AntiSlashFirstTest             ;
; ----------------------------------;
    CMP.B   #'\\',0(rDOCON)         ; "\" as first char ?
    JNZ     OPN_SearchInDIR         ; no
    ADD     #1,rDOCON               ; yes : skip '\' char
    MOV     #0,&ClusterH            ;       
    JMP     OPN_AntiSlashFirstNext  ;
; ----------------------------------;
OPN_SearchInDIR                     ; <=== dir found in path
; ----------------------------------;
    MOV     rDOCON,&PathName_PTR    ; save Pathname ptr
; ----------------------------------;
OPN_LoadDIRcluster                  ; <=== next DIR cluster loopback
; ----------------------------------;
    CALL    #ClusterHLtoFrstSectorHL; output: first Sector of this cluster
    MOV     &SecPerClus,rDODOES     ; set sectors count down
; ----------------------------------;
OPN_LoadDIRsector                   ; <=== next DIR Sector loopback
; ----------------------------------;
    CALL    #ReadSectorHL           ;SWX,
    MOV     #2,S                    ; prepare error 2
; ----------------------------------; W = 0 = DIREntryOfst
OPN_SearchDIRentry                  ; <=== next DIR_Entry loopback
; ----------------------------------;
    MOV     W,&DIREntryOfst         ; update DIREntryOfst
    CMP.B   #0,SD_BUF(W)            ; free entry ?
    JZ      OPN_NoSuchFile          ; NoSuchFile error = 2 ===>
    MOV     W,Y                     ; 1         W = DIREntryOfst, Y = Entry_name pointer
    MOV     #8,X                    ; count of chars in entry name
; ----------------------------------;
OPN_CompareName                     ;
; ----------------------------------;
    MOV.B   @rDOCON+,T              ;
    CMP.B   T,SD_BUF(Y)             ; compare Pathname with DirEntry1to8, char by char
    JNZ     OPN_CompareNameNext     ;
    ADD     #1,Y                    ;
    SUB     #1,X                    ;
    JNZ     OPN_CompareName         ;
    MOV.B   @rDOCON+,T              ; 9th char of Pathname should be '.'
    JZ      OPN_CompareNameDone     ; if X = 0
; ----------------------------------;
OPN_CompareNameNext                 ; remainder of 8 chars of DIR_entry name must be spaces
; ----------------------------------;
    CMP.B   #32,SD_BUF(Y)           ; parse DIR entry up to 8th chars
    JNZ     OPN_DIRentryMismatch    ; if a char of DIR entry name <> space
    ADD     #1,Y                    ;
    SUB     #1,X                    ;
    JNZ     OPN_CompareNameNext     ;
; ----------------------------------;
OPN_CompareNameDone                 ; T = "." or FirstNotEqualChar
; ----------------------------------;
    CMP.B   #'\\',T                 ; FirstNotEqualChar of Pathname = "\" ?
    JZ      OPN_EntryFound          ;
; ----------------------------------;
    MOV     #3,X                    ; to compare 3 char extension
    CMP.B   #'.',T                  ; FirstNotEqualChar of Pathname = dot ?
    JNZ     OPN_CompExtensionNext   ; if not
; ----------------------------------;
OPN_CompareExtension                ;
; ----------------------------------;
    CMP.B   @rDOCON+,SD_BUF(Y)      ; compare Pathname_ext(char) with DirEntry9to11(char)
    JNZ     OPN_CompExtensionNext   ;
    ADD     #1,Y                    ;
    SUB     #1,X                    ;
    JNZ     OPN_CompareExtension    ;
    JZ      OPN_CompExtensionDone   ;
; ----------------------------------;
OPN_CompExtensionNext               ; remainder of 8 chars of DIR_entry extension must be spaces
; ----------------------------------;
    CMP.B   #32,SD_BUF(Y)           ; parse DIR entry up to 11th chars
    JNZ     OPN_DIRentryMismatch    ; if a char of DIR entry extension <> space
    ADD     #1,Y                    ;
    SUB     #1,X                    ;
    JNZ     OPN_CompExtensionNext   ;
; ----------------------------------;
OPN_CompExtensionDone               ;
; ----------------------------------;
    CMP.B   #'.',-2(rDOCON)         ; LastCharEqual = dot ? (case of Pathname = "..\" which matches with first DIR entry = ".")
    JZ      OPN_DIRentryMismatch    ; to compare with 2th DIR entry, the good one.
    CMP     TOS,rDOCON              ; EOS reached ?
    JC      OPN_EntryFound          ; yes
; ----------------------------------;
OPN_DIRentryMismatch                ;
; ----------------------------------;
    MOV     &PathName_PTR,rDOCON    ; reload PathName_PTR as it was at last OPN_SearchInDIR
    ADD     #32,W                   ; W = DIREntryOfst + DIRentrySize
    CMP     #512,W                  ; out of sector bound ?
    JNZ     OPN_SearchDIRentry      ; no, loopback for search next DIR entry in same sector
; ----------------------------------;
    ADD     #1,&SectorL             ;
    ADDC    #0,&SectorH             ;
    SUB     #1,rDODOES              ; count of Dir sectors reached ?
    JNZ     OPN_LoadDIRsector       ; no, loopback to load next DIR sector in same cluster
; ----------------------------------;
    CALL    #Cluster2FAT1secWofsY   ; load FATsector in SD_Buffer, set Y = FAToffset
    CMP     #-1,0(Y)                ; last DIR cluster ?
    JNZ     OPN_SetNextDIRcluster   ;
    CMP     #0FFFh,2(Y)             ;
    .IFNDEF SD_CARD_READ_WRITE      ;
    JZ      OPN_NoSuchFile          ; yes, NoSuchFile error = 2 ===>
    .ELSE                           ;
; ==================================;
    JNZ     OPN_SetNextDIRcluster   ; no
;OPN_QcreateDIRentry                 ; -- open_type EOS
    CMP     #4,0(PSP)               ;               open type = WRITE" or APPEND" ?
    JNC     OPN_NoSuchFile          ; no: NoSuchFile error = 2 ===>
;OPN_AddDIRcluster                   ; yes
    PUSH    #OPN_LoadDIRcluster     ; as RETurn of GetNewCluster: ===> loopback to load this new DIR cluster
; ==================================;
GetNewCluster                       ; called by Write_File
; ==================================;
    PUSH    Y                       ; push previous FAToffset
    PUSH    W                       ; push previous FATsector
    CALL    #SearchMarkNewClusterHL ;SWXY input: W = FATsector Y = FAToffset, output: ClusterHL, W = FATsector of New cluster
    CMP     @RSP,W                  ; previous and new clusters are in same FATsector?
    JZ      LinkClusters            ;     yes
; ----------------------------------;
;UpdateNewClusterFATs               ;
; ----------------------------------;
    MOV     @RSP,W                  ; W = previous FATsector
    CALL    #ReadFAT1SectorW        ;SWX  reload previous FATsector in buffer to link clusters
; ----------------------------------;
LinkClusters                        ;
; ----------------------------------;
    MOV     @RSP+,W                 ; W = previous FATsector
    MOV     @RSP+,Y                 ; Y = previous FAToffset
    MOV     &ClusterL,SD_BUF(Y)     ; store new cluster to current cluster address in previous FATsector buffer
    MOV     &ClusterH,SD_BUF+2(Y)   ;
    JMP     SaveSectorWtoFATs       ;SWXY update FATs from SD_BUF to W = previous FATsector, then RET
; ==================================;
    .ENDIF ; SD_CARD_READ_WRITE     ;
; ----------------------------------;
OPN_SetNextDIRcluster               ;
; ----------------------------------;
    MOV     @Y+,&ClusterL           ;
    MOV     @Y,&ClusterH            ;
    JMP     OPN_LoadDIRcluster      ; ===> loop back to load this new DIR cluster
; ----------------------------------;
OPN_EntryFound                      ; Y points on the file attribute (11th byte of entry)
; ----------------------------------;
;    MOV     W,&DIREntryOfst         ;
    MOV     SD_BUF+14H(W),&ClusterH ; first clusterH of file
    MOV     SD_BUF+1Ah(W),&ClusterL ; first clusterL of file
    BIT.B   #10h,SD_BUF+0Bh(W)      ; test if Directory or File
    JZ      OPN_FileFound           ; is a file
; ----------------------------------;
;OPN_DIRfound                       ; entry is a DIRECTORY
; ----------------------------------;
    CMP     #0,&ClusterH            ; case of ".." entry, when parent directory is root
    JNZ     OPN_DIRfoundNext        ;
    CMP     #0,&ClusterL            ; case of ".." entry, when parent directory is root
    JNZ     OPN_DIRfoundNext        ;
OPN_AntiSlashFirstNext
    MOV     #2,&ClusterL            ; set clusterL as RootDIR cluster
OPN_DIRfoundNext                    ;
    CMP     TOS,rDOCON              ; EOS reached ?
    JNC     OPN_SearchInDIR         ; no: (rDOCON points after "\") ==> loop back
; ----------------------------------;
;OPN_SetCurrentDIR                  ; -- open_type ptr  PathName_PTR is set on name of this DIR
; ----------------------------------;
    MOV     &ClusterL,&DIRClusterL  ;
    MOV     &ClusterH,&DIRclusterH  ;
    MOV     #0,0(PSP)               ; -- open_type ptr      set open_type = 0 = DIR
    JMP     OPN_NEXT                ;
; ----------------------------------;
OPN_FileFound                       ; -- open_type ptr  PathName_PTR is set on name of file
; ----------------------------------;
    MOV     @PSP,W                  ;
    CALL    #GetFreeHandle          ;STWXY init handle(HDLL_DIRsect,HDLW_DIRofst,HDLL_FirstClus = HDLL_CurClust,HDLL_CurSize)
; ----------------------------------; output : T = CurrentHdl*, S = ReturnError, Y = DIRentry offset
OPN_NEXT                            ;
OPN_NoSuchFile                      ; S = error 2
    MOV     #xdodoes,rDODOES        ;                   restore rDODOES
    MOV     #xdocon,rDOCON          ;                   restore rDODOES
    MOV     @PSP+,W                 ; -- ptr            W = open_type
    MOV     @PSP+,TOS               ; --
; ----------------------------------; then go to selected OpenType subroutine (OpenType = W register)
    CMP     #0,W                    ;
    JNZ     OPEN_QLOAD              ; for OPEN DIR nothing else to do
    MOV @IP+,PC                     ;
; ----------------------------------;


; ======================================================================
; LOAD" primitive as part of Open_File
; input from open:  S = OpenError, W = open_type, SectorHL = DIRsectorHL,
;                   Buffer = [DIRsector], ClusterHL = FirstClusterHL
;       from open(GetFreeHandle): Y = DIRentry, T = CurrentHdl
; output: nothing else abort on error
; ======================================================================

; ----------------------------------;
OPEN_QLOAD                          ;
; ----------------------------------;
    .IFDEF SD_CARD_READ_WRITE       ;
    CMP.B   #0,W                    ; open_type > LOAD"
    JGE     OPEN_1W                 ; next step in forthMSP430FR_SD_RW.asm
    .ENDIF                          ;
; ----------------------------------; here W is free
;OPEN_LOAD                          ;
; ----------------------------------;
    CMP     #0,S                    ; open file happy end ?
    JNZ     OPEN_Error              ; no
    MOV #NOECHO,PC                  ; return to QUIT then SD_ACCEPT
;    MOV @IP+,PC                     ;
; ----------------------------------;

; ----------------------------------;
OPEN_Error                          ; S= error
; ----------------------------------;
; Error 1  : PathNameNotFound       ; S = error 1
; Error 2  : NoSuchFile             ; S = error 2
; Error 4  : alreadyOpen            ; S = error 4
; Error 8  : NomoreHandle           ; S = error 8
; ----------------------------------;
    PUSH #SD_OPEN_FILE_ERROR        ;
    JMP TokenToCloseTest            ;
; ----------------------------------;

; to enable bootstrap: BOOT
; to disable bootstrap: NOBOOT

; XBOOT          [SYSRSTIV|USERSTIV] --
; here we are after INIT_FORTH
; performs bootstrap from SD_CARD\BOOT.4th file, ready to test SYSRSTIV|USERSYS value
XBOOT       CALL &HARD_APP          ; WARM first calls HARD_APP (which includes INIT_HARD_SD)
            MOV #PSTACK-2,PSP       ; preserve SYSRSTIV|USERSYS in TOS for BOOT.4TH tests
            MOV #0,0(PSP)           ; set TOS = 0 for the next of XBOOT
            mLO2HI                  ;
            .word XSQUOTE           ; -- SYSRSTIV|USERSYS addr u
            .byte 15,"LOAD\34 BOOT.4TH\34"  ; LOAD" BOOT.4TH" issues error 2 if no such file...
            .word BRAN,BTSTRP_QUIT  ; to interpret this string, then loop back to QUIT_XS
; ----------------------------------;

; ==================================;
            FORTHWORD "BOOT"        ; to enable BOOT
; ==================================;
            MOV #XBOOT,&PUCNEXT     ; inserts XBOOT in PUC chain.
            MOV @IP+,PC

; ==================================;
            FORTHWORD "NOBOOT"      ; to disable BOOT
; ==================================;
NOBOOT      MOV #WARM,&PUCNEXT      ; removes XBOOT from PUC chain.
            MOV @IP+,PC             ;
